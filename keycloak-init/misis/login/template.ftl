<#import "field.ftl" as field>
<#import "footer.ftl" as loginFooter>
<#macro username>
<#assign label>
    <#if !realm.loginWithEmailAllowed>${msg("username")}<#elseif !realm.registrationEmailAsUsername>${msg("usernameOrEmail")}<#else>${msg("email")}</#if>
</#assign>
<@field.group name="username" label=label>
<div class="${properties.kcInputGroup}">
    <div class="${properties.kcInputGroupItemClass} ${properties.kcFill}">
        <span class="${properties.kcInputClass} ${properties.kcFormReadOnlyClass}">
          <input id="kc-attempted-username" value="${auth.attemptedUsername}" readonly>
        </span>
    </div>
    <div class="${properties.kcInputGroupItemClass}">
        <button id="reset-login" class="${properties.kcFormPasswordVisibilityButtonClass} kc-login-tooltip" type="button"
                aria-label="${msg('restartLoginTooltip')}" onclick="location.href='${url.loginRestartFlowUrl}'">
            <i class="fa-sync-alt fas" aria-hidden="true"></i>
            <span class="kc-tooltip-text">${msg("restartLoginTooltip")}</span>
        </button>
    </div>
    </@field.group>
    </#macro>

    <#macro registrationLayout bodyClass="" displayInfo=false displayMessage=true displayRequiredFields=false>
        <!DOCTYPE html>
        <html class="${properties.kcHtmlClass!}"<#if realm.internationalizationEnabled> lang="${locale.currentLanguageTag}" dir="${(locale.rtl)?then('rtl','ltr')}"</#if>>

        <head>
            <meta charset="utf-8">
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
            <meta name="robots" content="noindex, nofollow">

            <#if properties.meta?has_content>
                <#list properties.meta?split(' ') as meta>
                    <meta name="${meta?split('==')[0]}" content="${meta?split('==')[1]}"/>
                </#list>
            </#if>
            <title>${msg("loginTitle",(realm.displayName!''))}</title>
            <link rel="icon" href="${url.resourcesPath}/img/favicon.ico"/>
            <#if properties.stylesCommon?has_content>
                <#list properties.stylesCommon?split(' ') as style>
                    <link href="${url.resourcesCommonPath}/${style}" rel="stylesheet"/>
                </#list>
            </#if>
            <#if properties.styles?has_content>
                <#list properties.styles?split(' ') as style>
                    <link href="${url.resourcesPath}/${style}" rel="stylesheet"/>
                </#list>
            </#if>
            <script type="importmap">
                {
                    "imports": {
                        "rfc4648": "${url.resourcesCommonPath}/vendor/rfc4648/rfc4648.js"
            }
        }
            </script>
            <#if properties.scripts?has_content>
                <#list properties.scripts?split(' ') as script>
                    <script src="${url.resourcesPath}/${script}" type="text/javascript"></script>
                </#list>
            </#if>
            <#if scripts??>
                <#list scripts as script>
                    <script src="${script}" type="text/javascript"></script>
                </#list>
            </#if>
            <script type="module" src="${url.resourcesPath}/js/passwordVisibility.js"></script>
            <script type="module">
                import {checkCookiesAndSetTimer} from "${url.resourcesPath}/js/authChecker.js";

                checkCookiesAndSetTimer(
                    "${url.ssoLoginInOtherTabsUrl?no_esc}"
                );

                const DARK_MODE_CLASS = "pf-v5-theme-dark";
                const mediaQuery = window.matchMedia("(prefers-color-scheme: dark)");
                updateDarkMode(mediaQuery.matches);
                mediaQuery.addEventListener("change", (event) =>
                    updateDarkMode(event.matches),
                );

                function updateDarkMode(isEnabled) {
                    const {classList} = document.documentElement;
                    if (isEnabled) {
                        classList.add(DARK_MODE_CLASS);
                    } else {
                        classList.remove(DARK_MODE_CLASS);
                    }
                }
            </script>
        </head>

        <body id="keycloak-bg" class="${properties.kcBodyClass!}">

        <div class="${properties.kcLogin!}">
            <div class="${properties.kcLoginContainer!}">
                <header role="banner" class="fr-header">
                    <div class="fr-header__body">
                        <div class="fr-container">
                            <div class="fr-header__body-row">
                                <div class="fr-header__brand fr-enlarge-link">
                                    <div class="fr-header__brand-top">
                                        <div class="fr-header__logo">
                                            <p class="fr-logo">
                                                Ministères
                                                <br/>
                                                Territoires
                                                <br/>
                                                Écologie
                                                <br/>
                                                Logement
                                            </p>
                                        </div>
                                    </div>
                                    <div class="fr-header__service">
                                        <a onclick="history.back()" href="#" title="Accueil - MISIS - Ministères des Territoires, de l'Écologie et du Logement" style="font-weight:700;font-size:1.25rem;line-height:1.75rem">
                                            <p class="fr-header__service-title">
                                                MISIS
                                            </p>
                                        </a>
                                        <p class="fr-header__service-tagline">Outil de Mesure d'Impact des Sites Internet et leur Suivi</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </header>
                <main class="fr-pt-md-14v" role="main">
                    <div class="${properties.kcLoginMain!}">
                        <div class="${properties.kcLoginMainBody!}">
                            <div class="fr-col-12 fr-col-md-8 fr-col-lg-6" style="background: var(--background-alt-grey);">
                                <div class="fr-container fr-background-alt--grey fr-px-md-0 fr-py-10v fr-py-md-14v">
                                    <div class="fr-grid-row fr-grid-row-gutters fr-grid-row--center">
                                        <#-- App-initiated actions should not see warning messages about the need to complete the action -->
                                        <#-- during login.                                                                               -->
                                        <#if displayMessage && message?has_content && (message.type != 'warning' || !isAppInitiatedAction??)>
                                            <div class="fr-col-12 fr-px-md-10v fr-pb-10v">
                                                <div class="fr-alert fr-alert--error">
                                                    <p kc-feedback-text">${kcSanitize(message.summary)?no_esc}</p>
                                                </div>
                                            </div>
                                        </#if>
                                        <div class="fr-col-12 fr-col-md-9 fr-col-lg-8">
                                            <h1>Connexion à MISIS</h1>
                                            <h2>Se connecter avec son compte</h2>
                                            <#if !(auth?has_content && auth.showUsername() && !auth.showResetCredentials())>
                                                <#if displayRequiredFields>
                                                    <div class="${properties.kcContentWrapperClass!}">
                                                        <div class="${properties.kcLabelWrapperClass!} subtitle">
                                                            <span class="${properties.kcInputHelperTextItemTextClass!}">
                                                              <span class="${properties.kcInputRequiredClass!}">*</span> ${msg("requiredFields")}
                                                            </span>
                                                        </div>
                                                    </div>
                                                </#if>
                                                <#else>
                                                    <#if displayRequiredFields>
                                                        <div class="${properties.kcContentWrapperClass!}">
                                                            <div class="${properties.kcLabelWrapperClass!} subtitle">
                                                                <span class="${properties.kcInputHelperTextItemTextClass!}">
                                                                  <span class="${properties.kcInputRequiredClass!}">*</span> ${msg("requiredFields")}
                                                                </span>
                                                            </div>
                                                            <div class="${properties.kcFormClass} ${properties.kcContentWrapperClass}">
                                                                <#nested "show-username">
                                                                <@username />
                                                            </div>
                                                        </div>
                                                    <#else>
                                                        <div class="${properties.kcFormClass} ${properties.kcContentWrapperClass}">
                                                            <#nested "show-username">
                                                            <@username />
                                                        </div>
                                                    </#if>
                                                </#if>
                                                <div>
                                                    <#nested "form">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <#if auth?has_content && auth.showTryAnotherWayLink()>
                            <form id="kc-select-try-another-way-form" action="${url.loginAction}" method="post" novalidate="novalidate">
                                <input type="hidden" name="tryAnotherWay" value="on"/>
                                <a id="try-another-way" href="javascript:document.forms['kc-select-try-another-way-form'].submit()"
                                   class="${properties.kcButtonSecondaryClass} ${properties.kcButtonBlockClass} ${properties.kcMarginTopClass}">
                                    ${kcSanitize(msg("doTryAnotherWay"))?no_esc}
                                </a>
                            </form>
                        </#if>

                        <#if displayInfo>
                            <div id="kc-info" class="${properties.kcSignUpClass!}">
                                <div id="kc-info-wrapper" class="${properties.kcInfoAreaWrapperClass!}">
                                    <#nested "info">
                                </div>
                            </div>
                        </#if>
                    </div>
                    <div class="pf-v5-c-login__main-footer">
                        <#nested "socialProviders">
                    </div>
                </main>

                <@loginFooter.content/>
                <footer role="contentinfo" id="footer" class="fr-footer fr-mt-1v">
                    <div class="fr-container">
                        <div class="fr-footer__body">
                            <div class="fr-footer__brand fr-enlarge-link">
                                <a onclick="history.back()" href="#" title="Retour à l’accueil du site - MISIS - Ministères des Territoires, de l'Écologie et du Logement">
                                    <p class="fr-logo"> Ministères <br> Territoires <br> Écologie <br> Logement </p>
                                </a>
                            </div>
                            <div class="fr-footer__content">
                                <ul class="fr-footer__content-list">
                                    <li class="fr-footer__content-item">
                                        <a target="_blank" rel="noopener external" title="legifrance.gouv.fr - nouvelle fenêtre" href="https://legifrance.gouv.fr" class="fr-footer__content-link">legifrance.gouv.fr</a>
                                    </li>
                                    <li class="fr-footer__content-item">
                                        <a target="_blank" rel="noopener external" title="info.gouv.fr - nouvelle fenêtre" href="https://www.info.gouv.fr/" class="fr-footer__content-link"> info.gouv.fr </a>
                                    </li><li class="fr-footer__content-item">
                                        <a target="_blank" rel="noopener external" title="service-public.fr - nouvelle fenêtre" href="https://service-public.fr" class="fr-footer__content-link">service-public.fr</a>
                                    </li>
                                    <li class="fr-footer__content-item">
                                        <a target="_blank" rel="noopener external" title="data.gouv.fr - nouvelle fenêtre" href="https://data.gouv.fr" class="fr-footer__content-link">data.gouv.fr</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="fr-footer__bottom">
                            <ul class="fr-footer__bottom-list">
                                <li class="fr-footer__bottom-item">
                                    <a onclick="history.back()" class="fr-footer__bottom-link" href="#"> Plan du site </a>
                                </li>
                                <li class="fr-footer__bottom-item">
                                    <a onclick="history.back()" href="#" class="fr-footer__bottom-link"> Accessibilité : non conforme </a>
                                </li>
                                <li class="fr-footer__bottom-item">
                                    <a onclick="history.back()" class="fr-footer__bottom-link" href="#"> Mentions légales </a>
                                </li>
                                <li class="fr-footer__bottom-item">
                                    <a onclick="history.back()" class="fr-footer__bottom-link" href="#"> Données personnelles </a>
                                </li>
                                <li class="fr-footer__bottom-item">
                                    <a onclick="history.back()" class="fr-footer__bottom-link" href="#"> Gestion des cookies </a>
                                </li>
                                <li class="fr-footer__bottom-item">
                                    <button class="fr-footer__bottom-link disabled"> Paramètres d'affichage </button>
                                </li>
                            </ul>
                            <div class="fr-footer__bottom-copy">
                                <p> Sauf mention explicite de propriété intellectuelle détenue par des tiers, les contenus de ce site sont proposés sous <a href="https://github.com/etalab/licence-ouverte/blob/master/LO.md" target="_blank" rel="noopener external" title="licence etalab-2.0 - nouvelle fenêtre"> licence etalab-2.0 </a></p>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        </body>
        </html>
    </#macro>
