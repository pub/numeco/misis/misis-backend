# misis-backend

## Installation du projet
L'arborescence de l'installation final ressemblera à :
```
├── backend
│   ├── src
│   ├── doc
│   ├── gitlab
│   ├── keycloak-init
│   ├── postgres-init
│   ├── deploy
│   └── test_api
```
### Prérequis
- Java (v21)
- Git
- Docker

Depuis votre `<workspace>`\
(un dossier dans lequel sera installer le projet. Son emplacement n'a que peu d'importance)
### 1. Téléchargement du code sources
```
git clone https://gitlab-forge.din.developpement-durable.gouv.fr/pub/numeco/misis/misis-backend.git backend && cd backend
```
### 2. Lancement des images Docker
```
docker compose up -d
```
### 3. Lancement du projet
```
./mvnw quarkus:dev
```
## Prochaine étape
[Découverte du projet](doc/QUICK_TOUR.md)