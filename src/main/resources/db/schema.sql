-- liquibase formatted sql

-- changeset laurent-martins:1
CREATE TABLE "misis_user" ("created_date" date NOT NULL, "id" BIGINT NOT NULL, "email" VARCHAR(255) NOT NULL, "username" VARCHAR(255) NOT NULL, CONSTRAINT "misis_user_pkey" PRIMARY KEY ("id"));
CREATE TABLE "page" ("dom_elements_count" INTEGER, "http_requests_count" INTEGER, "analysed_at" TIMESTAMP WITHOUT TIME ZONE, "data_decoded_in_bytes" BIGINT, "data_transfered_in_bytes" BIGINT, "groupe_de_pages_id" BIGINT, "id" BIGINT NOT NULL, "analyse_statut" VARCHAR(255), "url" VARCHAR(255) NOT NULL, CONSTRAINT "page_pkey" PRIMARY KEY ("id"));
CREATE TABLE "permission_type" ("id" BIGINT NOT NULL, "misis_permission" VARCHAR(255), "name" VARCHAR(255), CONSTRAINT "permission_type_pkey" PRIMARY KEY ("id"));
CREATE TABLE "ressource" ("id" BIGINT NOT NULL, "page_id" BIGINT, "size_in_bytes" BIGINT, "url" VARCHAR(10000) NOT NULL, "loading_type" VARCHAR(255), "type" VARCHAR(255), CONSTRAINT "ressource_pkey" PRIMARY KEY ("id"));
CREATE TABLE "statistique" ("timestamp" date NOT NULL, "groupe_de_pages_id" BIGINT, "id" BIGINT NOT NULL, "statistic_type" VARCHAR(255) NOT NULL, CONSTRAINT "statistique_pkey" PRIMARY KEY ("id"));
CREATE TABLE "groupe_de_pages" ("created_date" date NOT NULL, "modified_date" date NOT NULL, "created_by" BIGINT NOT NULL, "id" BIGINT NOT NULL, "modified_by" BIGINT NOT NULL, "suivi_de_site_id" BIGINT, "methode_de_creation_de_groupe" VARCHAR(255) NOT NULL, "name" VARCHAR(255) NOT NULL, "periodicite_du_suivi" VARCHAR(255) NOT NULL, CONSTRAINT "groupe_de_pages_pkey" PRIMARY KEY ("id"));
CREATE TABLE "misis_user_suivi_de_site" ("misis_user_id" BIGINT NOT NULL, "suivis_de_site_id" BIGINT NOT NULL);
CREATE TABLE "permission" ("discriminator" INTEGER NOT NULL, "domain_id" BIGINT, "id" BIGINT NOT NULL, "user_id" BIGINT, CONSTRAINT "permission_pkey" PRIMARY KEY ("id"));
CREATE TABLE "permission_permission_type" ("allowed_id" BIGINT NOT NULL, "permission_id" BIGINT NOT NULL);
CREATE TABLE "suivi_de_site" ("created_date" date NOT NULL, "modified_date" date NOT NULL, "created_by" BIGINT NOT NULL, "id" BIGINT NOT NULL, "modified_by" BIGINT NOT NULL, "name" VARCHAR(255) NOT NULL, "url" VARCHAR(255) NOT NULL, CONSTRAINT "suivi_de_site_pkey" PRIMARY KEY ("id"));
CREATE TABLE "valeurs_statistiques" ("id" BIGINT NOT NULL, "value" BIGINT, "key" VARCHAR(255) NOT NULL, CONSTRAINT "valeurs_statistiques_pkey" PRIMARY KEY ("id", "key"));

CREATE INDEX "analyse_statut_index" ON "page"("analyse_statut");
CREATE INDEX "analysed_at_index" ON "page"("analysed_at");
CREATE INDEX "misis_permission_index" ON "permission_type"("misis_permission");
CREATE INDEX "type_index" ON "ressource"("type");
CREATE INDEX "loading_type_index" ON "ressource"("loading_type");
CREATE INDEX "statistic_type_index" ON "statistique"("statistic_type");

ALTER TABLE "misis_user" ADD CONSTRAINT "misis_user_email_key" UNIQUE ("email");
ALTER TABLE "misis_user" ADD CONSTRAINT "misis_user_username_key" UNIQUE ("username");
ALTER TABLE "page" ADD CONSTRAINT "unique_url_groupe_de_pages_id" UNIQUE ("groupe_de_pages_id", "url");
ALTER TABLE "misis_user_suivi_de_site" ADD CONSTRAINT "fk4texldr7gp2yl50p0s29h4pff" FOREIGN KEY ("suivis_de_site_id") REFERENCES "suivi_de_site" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE "permission_permission_type" ADD CONSTRAINT "fk9gx8cywyy3os8e4ul5uya3vj8" FOREIGN KEY ("permission_id") REFERENCES "permission" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE "groupe_de_pages" ADD CONSTRAINT "fk9hoeuh2uyx7d3dpmy8xndojyq" FOREIGN KEY ("created_by") REFERENCES "misis_user" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE "suivi_de_site" ADD CONSTRAINT "fkaehsc88r8pwk4re9ba401dyus" FOREIGN KEY ("modified_by") REFERENCES "misis_user" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE "suivi_de_site" ADD CONSTRAINT "fkamdsjd16bvx9666vxq6q86cx9" FOREIGN KEY ("created_by") REFERENCES "misis_user" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE "permission" ADD CONSTRAINT "fkbfrx21tp1nyu0s2qysrv2i3mk" FOREIGN KEY ("user_id") REFERENCES "misis_user" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE "groupe_de_pages" ADD CONSTRAINT "fkbok9v0qkcfyk8m92vlsq50h6i" FOREIGN KEY ("modified_by") REFERENCES "misis_user" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE "misis_user_suivi_de_site" ADD CONSTRAINT "fkcf6bnsqc8hbtd3gjgppy6dkcw" FOREIGN KEY ("misis_user_id") REFERENCES "misis_user" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE "permission" ADD CONSTRAINT "fkds5af7tmxg49hauyphk70om75" FOREIGN KEY ("domain_id") REFERENCES "groupe_de_pages" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE "valeurs_statistiques" ADD CONSTRAINT "fkefko269rym4klgdl3cvgx931a" FOREIGN KEY ("id") REFERENCES "statistique" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE "permission_permission_type" ADD CONSTRAINT "fkjxmv273j3rd4mla4wumdu969t" FOREIGN KEY ("allowed_id") REFERENCES "permission_type" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE "page" ADD CONSTRAINT "fkjxwpysanb4xqycbkrg4j6hw8v" FOREIGN KEY ("groupe_de_pages_id") REFERENCES "groupe_de_pages" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE "ressource" ADD CONSTRAINT "fkmsfbbkm717tlnghv53u7ub1e3" FOREIGN KEY ("page_id") REFERENCES "page" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE "statistique" ADD CONSTRAINT "fkr132jgdjj3c5bb3d3dhaol54x" FOREIGN KEY ("groupe_de_pages_id") REFERENCES "groupe_de_pages" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE "groupe_de_pages" ADD CONSTRAINT "fkrsc6whcqvnc6sfotm37f81lto" FOREIGN KEY ("suivi_de_site_id") REFERENCES "suivi_de_site" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION;

CREATE SEQUENCE  IF NOT EXISTS "groupe_de_pages_seq" AS bigint START WITH 1 INCREMENT BY 50 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;
CREATE SEQUENCE  IF NOT EXISTS "misis_user_seq" AS bigint START WITH 1 INCREMENT BY 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;
CREATE SEQUENCE  IF NOT EXISTS "page_seq" AS bigint START WITH 1 INCREMENT BY 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;
CREATE SEQUENCE  IF NOT EXISTS "permission_seq" AS bigint START WITH 1 INCREMENT BY 50 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;
CREATE SEQUENCE  IF NOT EXISTS "permission_type_seq" AS bigint START WITH 1 INCREMENT BY 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;
CREATE SEQUENCE  IF NOT EXISTS "ressource_seq" AS bigint START WITH 1 INCREMENT BY 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;
CREATE SEQUENCE  IF NOT EXISTS "statistique_seq" AS bigint START WITH 1 INCREMENT BY 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;
CREATE SEQUENCE  IF NOT EXISTS "suivi_de_site_seq" AS bigint START WITH 1 INCREMENT BY 50 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

-- changeset laurent-martins:2
ALTER TABLE "misis_user" ADD "delete_at" date;

-- changeset laurent-martins:3
CREATE TABLE "notification" ("visible" BOOLEAN NOT NULL, "created_at" TIMESTAMP WITHOUT TIME ZONE NOT NULL, "id" BIGINT NOT NULL, "user_id" BIGINT, "message" VARCHAR(255) NOT NULL, CONSTRAINT "notification_pkey" PRIMARY KEY ("id"));
ALTER TABLE "notification" ADD CONSTRAINT "fk2txklyjkp8ayy79e0pt0nkgr9" FOREIGN KEY ("user_id") REFERENCES "misis_user" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION;

-- changeset laurent-martins:4
CREATE SEQUENCE  IF NOT EXISTS "notification_seq" AS bigint START WITH 1 INCREMENT BY 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

-- changeset laurent-martins:5
CREATE SEQUENCE  IF NOT EXISTS "legal_documents_seq" AS bigint START WITH 1 INCREMENT BY 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;
CREATE TABLE "legal_documents" ("effective_date" TIMESTAMP WITHOUT TIME ZONE, "id" BIGINT NOT NULL, "version" VARCHAR(255), "content" OID, CONSTRAINT "legal_documents_pkey" PRIMARY KEY ("id"));
ALTER TABLE "misis_user" ADD "accepted_terms_version" VARCHAR(255);

-- changeset laurent-martins:6
ALTER TABLE "legal_documents" ALTER COLUMN "content" TYPE TEXT USING ("content"::TEXT);

-- changeset laurent-martins:7
ALTER TABLE "suivi_de_site" ALTER COLUMN "url" TYPE TEXT USING ("url"::TEXT);
ALTER TABLE "page" ALTER COLUMN "url" TYPE TEXT USING ("url"::TEXT);
ALTER TABLE "ressource" ALTER COLUMN "url" TYPE TEXT USING ("url"::TEXT);

-- changeset laurent-martins:8
ALTER TABLE "page" ADD COLUMN "title" TEXT;

-- changeset laurent-martins:9
ALTER TABLE "page" RENAME COLUMN "title" TO "titre";

-- changeset laurent-martins:10
ALTER TABLE "misis_user" DROP CONSTRAINT misis_user_username_key;

-- changeset laurent-martins:11
ALTER TABLE "page" ADD COLUMN "modified_at" TIMESTAMP WITHOUT TIME ZONE;
UPDATE "page" SET modified_at='2025-01-01 00:00:00';
ALTER TABLE "page" ALTER COLUMN "modified_at" SET NOT NULL;
