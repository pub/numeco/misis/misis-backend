--liquibase formatted sql

-- changeset laurent-martins:9999-1
INSERT INTO misis_user(id, username, email, created_date)
VALUES (nextval('misis_user_SEQ'), 'admin', 'admin@test.fr', '2000-01-01T00:00:0.000000');
INSERT INTO misis_user(id, username, email, created_date)
VALUES (nextval('misis_user_SEQ'), 'John Doe', 'un@test.fr', '2000-01-01T00:00:0.000000');
INSERT INTO misis_user(id, username, email, created_date)
VALUES (nextval('misis_user_SEQ'), 'Jane Doe', 'deux@test.fr', '2000-01-01T00:00:0.000000');
INSERT INTO misis_user(id, username, email, created_date)
VALUES (nextval('misis_user_SEQ'), 'Utilisateur Exemple', 'exemple@test.fr', '2000-01-01T00:00:0.000000');

INSERT INTO suivi_de_site (id, name, url, created_date, modified_date, created_by, modified_by)
VALUES (nextval('suivi_de_site_seq'), 'Site d''exemple', 'https://www.site-web-d-exemple-inexistant.fr/', '2000-01-01T00:00:0.000000', '2000-01-01T00:00:0.000000', 1, 1),
       (nextval('suivi_de_site_seq'), 'Site partagé par John Doe (un@test.fr)', 'https://www.john-doe-site-web.fr/', '2000-01-01T00:00:0.000000', '2000-01-01T00:00:0.000000', 2, 2);

INSERT INTO groupe_de_pages(id, name, periodicite_du_suivi, methode_de_creation_de_groupe, suivi_de_site_id, created_date, modified_date, created_by, modified_by)
VALUES (nextval('groupe_de_pages_seq'), 'Groupe n°1', 'QUOTIDIEN', 'MANUELLE', 1, '2000-01-01T00:00:0.000000', '2000-01-01T00:00:0.000000', 1, 1),
       (nextval('groupe_de_pages_seq'), 'Groupe n°2', 'QUOTIDIEN', 'MANUELLE', 1, '2000-01-01T00:00:0.000000', '2000-01-01T00:00:0.000000', 1, 1),
       (nextval('groupe_de_pages_seq'), 'Groupe n°3', 'QUOTIDIEN', 'MANUELLE', 1, '2000-01-01T00:00:0.000000', '2000-01-01T00:00:0.000000', 1, 1),
       (nextval('groupe_de_pages_seq'), 'Default', 'QUOTIDIEN', 'MANUELLE', 51, '2000-01-01T00:00:0.000000', '2000-01-01T00:00:0.000000', 2, 2);


INSERT INTO PAGE (id, url, dom_elements_count, http_requests_count, data_decoded_in_bytes, data_transfered_in_bytes, groupe_de_pages_id, analyse_statut, analysed_at, modified_at)
SELECT nextval('page_seq'),
       'https://www.site-web-d-exemple-inexistant.fr/page' || page_num || '_' || id,
       FLOOR(10 * (1000 - 100 + 1)) + 100          AS random_dom_elements_count,
       FLOOR(15 * (20 - 5 + 1)) + 5                AS random_http_requests_count,
       FLOOR(20 * (5000000 - 500000 + 1)) + 500000 AS random_data_decoded_in_bytes,
       FLOOR(30 * (5000000 - 500000 + 1)) + 500000 AS random_data_transfered_in_bytes,
       id * 50 + 1 - 50,
       'TERMINEE',
       '2000-01-02T00:00:0.000000',
       '2000-01-02T00:00:0.000000'
FROM generate_series(1, 3) AS id
         CROSS JOIN LATERAL generate_series(1, 20) AS page_num;

INSERT INTO PAGE (id, url, dom_elements_count, http_requests_count, data_decoded_in_bytes, data_transfered_in_bytes, groupe_de_pages_id, analyse_statut, analysed_at, modified_at)
VALUES (nextval('page_seq'),
        'https://www.john-doe-site-web.fr/page_1',
        FLOOR(10 * (1000 - 100 + 1)) + 100,
        FLOOR(25 * (20 - 5 + 1)) + 5,
        FLOOR(20 * (5000000 - 500000 + 1)) + 500000,
        FLOOR(30 * (5000000 - 500000 + 1)) + 500000,
        (SELECT last_value FROM groupe_de_pages_seq),
        'TERMINEE',
        '2000-01-02T00:00:0.000000',
        '2000-01-02T00:00:0.000000');

INSERT INTO ressource (id, page_id, size_in_bytes, url, type, loading_type)
SELECT nextval('ressource_seq'),
       id,
       FLOOR(10 * (500000 - 50000 + 1)) + 50000 AS random_size_in_bytes,
       'https://www.site-web-d-exemple-inexistant.fr/ressource/images/image_' || id || '_' || ressource_num || '.jpg?q=atterrissage&p=param%C3%A9tre&source=d%C3%A9veloppeur',
       'IMAGE',
       'LANDING'
FROM generate_series(1, 2) AS id,
     generate_series(1, 5) AS ressource_num;

INSERT INTO ressource (id, page_id, size_in_bytes, url, loading_type, type)
VALUES (nextval('ressource_seq'), 1, 500,
        'data:image/svg+xml;charset=utf8,%3Csvg xmlns=''http://www.w3.org/2000/svg'' viewBox=''0 0 44 18''%3E%3Cpath fill=''%23000'' d=''M11.3 10.2c-.9.6-1.7 1.3-2.3 2.1v-.1c.4-.5.7-1 1-1.5.4-.2.7-.5 1-.8.5-.5 1-1 1.7-1.3.3-.1.5-.1.8 0-.1.1-.2.1-.4.2H13v-.1c-.3.3-.7.5-1 .9-.1.2-.2.6-.7.6 0 .1.1 0 0 0zm1.6 4.6c0-.1-.1 0-.2 0l-.1.1-.1.1-.2.2s.1.1.2 0l.1-.1c.1 0 .2-.1.2-.2.1 0 .1 0 .1-.1 0 .1 0 0 0 0zm-1.6-4.3c.1 0 .2 0 .2-.1s.1-.1.1-.1v-.1c-.2.1-.3.2-.3.3zm2.4 1.9s0-.1 0 0c.1-.1.2-.1.3-.1.7-.1 1.4-.3 2.1-.6-.8-.5-1.7-.9-2.6-1h.1c-.1-.1-.3-.1-.5-.2h.1c-.2-.1-.5-.1-.7-.2.1 0 .2-.2.2-.3h-.1c-.4.2-.6.5-.8.9.2.1.5 0 .7.1h-.3c-.1 0-.2.1-.2.2h.1c-.1 0-.1.1-.2.1.1.1.2 0 .4 0 0 .1.1.1.1.1-.1 0-.2.1-.3.3-.1.2-.2.2-.3.3v.1c-.3.2-.6.5-.9.8v.1c-.1.1-.2.1-.2.2v.1c.4-.1.6-.4 1-.5l.6-.3c.2 0 .3-.1.5-.1v.1h.2c0 .1-.2 0-.1.1s.3.1.4 0c.2-.2.3-.2.4-.2zM12.4 14c-.4.2-.9.2-1.2.4 0 0 0 .1-.1.1 0 0-.1 0-.1.1-.1 0-.1.1-.2.2l-.1.1s0 .1.1 0l.1-.1s-.1.1-.1.2V15.3l-.1.1s0 .1-.1.1l-.1.1.2-.2.1-.1h.2s0-.1.1-.1c.1-.1.2-.2.3-.2h.1c.1-.1.3-.1.4-.2.1-.1.2-.2.3-.2.2-.2.5-.3.8-.5-.1 0-.2-.1-.3-.1 0 .1-.2 0-.3 0zM30 9.7c-.1.2-.4.2-.6.3-.2.2 0 .4.1.5.1.3-.2.5-.4.5.1.1.2.1.2.1 0 .2.2.2.1.4s-.5.3-.3.5c.1.2.1.5 0 .7-.1.2-.3.4-.5.5-.2.1-.4.1-.6 0-.1 0-.1-.1-.2-.1-.5-.1-1-.2-1.5-.2-.1 0-.3.1-.4.1-.1.1-.3.2-.4.3l-.1.1c-.1.1-.2.2-.2.3-.1.2-.2.4-.2.6-.2.5-.2 1 0 1.4 0 0 1 .3 1.7.6.2.1.5.2.7.4l1.7 1H13.2l1.6-1c.6-.4 1.3-.7 2-1 .5-.2 1.1-.5 1.5-.9.2-.2.3-.4.5-.5.3-.4.6-.7 1-1l.3-.3s0-.1.1-.1c-.2.1-.2.2-.4.2 0 0-.1 0 0-.1s.2-.2.3-.2v-.1c-.4 0-.7.2-1 .5h-.2c-.5.2-.8.5-1.2.7v-.1c-.2.1-.4.2-.5.2-.2 0-.5.1-.8 0-.4 0-.7.1-1.1.2-.2.1-.4.1-.6.2v.1l-.2.2c-.2.1-.3.2-.5.4l-.5.5h-.1l.1-.1.1-.1c0-.1.1-.1.1-.2.2-.1.3-.3.5-.4 0 0-.1 0 0 0 0 0 0-.1.1-.1l-.1.1c-.1.1-.1.2-.2.2v-.1-.1l.2-.2c.1-.1.2-.1.3-.2h.1c-.2.1-.3.1-.5.2H14h-.1c0-.1.1-.1.2-.2h.1c1-.8 2.3-.6 3.4-1 .1-.1.2-.1.3-.2.1-.1.3-.2.5-.3.2-.2.4-.4.5-.7v-.1c-.4.4-.8.7-1.3 1-.6.2-1.3.4-2 .4 0-.1.1-.1.1-.1 0-.1.1-.1.1-.2h.1s0-.1.1-.1h.1c-.1-.1-.3.1-.4 0 .1-.1 0-.2.1-.2h.1s0-.1.1-.1c.5-.3.9-.5 1.3-.7-.1 0-.1.1-.2 0 .1 0 0-.1.1-.1.3-.1.6-.3.9-.4-.1 0-.2.1-.3 0 .1 0 .1-.1.2-.1v-.1h0c0-.1.1 0 .2-.1h-.1c.1-.1.2-.2.4-.2 0-.1-.1 0-.1-.1h.1-.5c-.1 0 0-.1 0-.1.1-.2.2-.5.3-.7h-.1c-.3.3-.8.5-1.2.6h-.2c-.2.1-.4.1-.5 0-.1-.1-.2-.2-.3-.2-.2-.1-.5-.3-.8-.4-.7-.2-1.5-.4-2.3-.3.3-.1.7-.2 1.1-.3.5-.2 1-.3 1.5-.3h-.3c-.4 0-.9.1-1.3.2-.3.1-.6.2-.9.2-.2.1-.3.2-.5.2v-.1c.3-.4.7-.7 1.1-.8.5-.1 1.1 0 1.6.1.4 0 .8.1 1.1.2.1 0 .2.2.3.3.2.1.4 0 .5.1v-.2c.1-.1.3 0 .4 0 .2-.2-.2-.4-.3-.6v-.1c.2.2.5.4.7.6.1.1.5.2.5 0-.2-.3-.4-.6-.7-.9v-.2c-.1 0-.1 0-.1-.1-.1-.1-.1-.2-.1-.3-.1-.2 0-.4-.1-.5-.1-.2-.1-.3-.1-.5-.1-.5-.2-1-.3-1.4-.1-.6.3-1 .6-1.5.2-.4.5-.7.8-1 .1-.4.3-.7.6-1 .3-.3.6-.5.9-.6.3-.1.5-.2.8-.3l2.5-.4H25l1.8.3c.1 0 .2 0 .2.1.1.1.3.2.4.2.2.1.4.3.6.5.1.1.2.3.1.4-.1.1-.1.4-.2.4-.2.1-.4.1-.6.1-.1 0-.2 0-.4-.1.5.2.9.4 1.2.8 0 .1.2.1.3.1v.1c-.1.1-.1.1-.1.2h.1c.1-.1.1-.4.3-.3.2.1.2.3.1.4-.1.1-.2.2-.4.3v.2c.1.1.1.2.2.4s.1.5.2.7c.1.5.2.9.2 1.4 0 .2-.1.5 0 .7l.3.6c.1.2.2.3.3.5.2.3.6.6.4 1zm-15.6 5.2c-.1 0-.1.1-.1.1s.1 0 .1-.1zm5.8-1.8c-.1.1 0 0 0 0zm-6.7-.2c0 .1.1 0 .1 0 .2-.1.5 0 .6-.2-.1-.1-.2 0-.2-.1-.1 0-.2 0-.2.1-.1.1-.3.1-.3.2z''/%3E%3Cpath fill=''gray'' d=''M27.9 6.8c.1 0 .3 0 .3.1-.1.2-.4.3-.6.5h-.1c-.1.1-.1.2-.1.2h-.3c.1.1.3.2.5.2l.1.1h.2V8c-.1.1-.2.1-.4.1.2.1.5.1.7 0 .2-.1 0-.4.1-.5-.1 0 0-.1-.1-.1.1-.1.1-.2.2-.2s.1 0 .2-.1c0-.1-.1-.1-.1-.2.2-.1.3-.3.3-.5 0-.1-.3-.1-.4-.2h-.5c-.2 0-.3.1-.5.1l-.6.3c.2-.1.4-.1.7-.2 0 .3.2.3.4.3''/%3E%3C/svg%3E',
        'LANDING', 'IMAGE');

INSERT INTO ressource (id, page_id, size_in_bytes, url, type, loading_type)
SELECT nextval('ressource_seq'),
       id,
       FLOOR(20 * (500000 - 50000 + 1)) + 50000 AS random_size_in_bytes,
       'https://www.site-web-d-exemple-inexistant.fr/ressource/images/image_' || id || '_' || ressource_num || '.jpg?q=d%C3%A9filement&p=param%C3%A9tre&source=d%C3%A9veloppeur',
       'IMAGE',
       'SCROLLING'
FROM generate_series(10, 12) AS id,
     generate_series(1, 2) AS ressource_num;

INSERT INTO ressource (id, page_id, size_in_bytes, url, type, loading_type)
SELECT nextval('ressource_seq'),
       id,
       FLOOR(20 * (5000000 - 500000 + 1)) + 500000 AS random_size_in_bytes,
       'https://www.site-web-d-exemple-inexistant.fr/ressource/document/pdf_' || id || '_' || ressource_num || '.pdf?parentview=1&id=inexistant&source=d%C3%A9veloppeur',
       'PDF',
       'DOWNLOADING'
FROM generate_series(1, 2) AS id,
     generate_series(1, 2) AS ressource_num;

INSERT INTO permission_type (id, misis_permission, name)
VALUES (nextval('permission_type_seq'), 'WRITE', 'Écriture'),
       (nextval('permission_type_seq'), 'READ', 'Lecture'),
       (nextval('permission_type_seq'), 'DELETE', 'Suppression')
;

INSERT INTO permission (discriminator, domain_id, id, user_id)
VALUES (1, 1, nextval('permission_seq'), 1),
       (1, 51, nextval('permission_seq'), 2),
       (1, 51, nextval('permission_seq'), 1)
;

INSERT INTO permission_permission_type (permission_id, allowed_id)
VALUES (1, 1),
       (1, 2),
       (1, 3),
       (51, 1),
       (51, 2),
       (51, 3),
       (101, 1),
       (101, 2),
       (101, 3)
;

INSERT INTO statistique (id, timestamp, statistic_type, groupe_de_pages_id)
VALUES (nextval('statistique_seq'), '2019-06-01', 'TRANSFERRED_DATA', 151),
       (nextval('statistique_seq'), '2019-06-01', 'REQUESTS', 151),
       (nextval('statistique_seq'), '2019-06-01', 'DOM_ELEMENTS', 151),
       (nextval('statistique_seq'), '2019-06-01', 'TRANSFERRED_DATA', 101),
       (nextval('statistique_seq'), '2019-06-01', 'REQUESTS', 101),
       (nextval('statistique_seq'), '2019-06-01', 'DOM_ELEMENTS', 101),
       (nextval('statistique_seq'), '2019-06-01', 'TRANSFERRED_DATA', 51),
       (nextval('statistique_seq'), '2019-06-01', 'REQUESTS', 51),
       (nextval('statistique_seq'), '2019-06-01', 'DOM_ELEMENTS', 51),
       (nextval('statistique_seq'), '2019-06-01', 'TRANSFERRED_DATA', 1),
       (nextval('statistique_seq'), '2019-06-01', 'REQUESTS', 1),
       (nextval('statistique_seq'), '2019-06-01', 'DOM_ELEMENTS', 1),
       (nextval('statistique_seq'), '2019-06-01', 'IMAGE', 1),
       (nextval('statistique_seq'), '2019-06-01', 'PDF', 1),

       (nextval('statistique_seq'), '2020-07-02', 'TRANSFERRED_DATA', 151),
       (nextval('statistique_seq'), '2020-07-02', 'REQUESTS', 151),
       (nextval('statistique_seq'), '2020-07-02', 'DOM_ELEMENTS', 151),
       (nextval('statistique_seq'), '2020-07-02', 'TRANSFERRED_DATA', 101),
       (nextval('statistique_seq'), '2020-07-02', 'REQUESTS', 101),
       (nextval('statistique_seq'), '2020-07-02', 'DOM_ELEMENTS', 101),
       (nextval('statistique_seq'), '2020-07-02', 'TRANSFERRED_DATA', 51),
       (nextval('statistique_seq'), '2020-07-02', 'REQUESTS', 51),
       (nextval('statistique_seq'), '2020-07-02', 'DOM_ELEMENTS', 51),
       (nextval('statistique_seq'), '2020-07-02', 'TRANSFERRED_DATA', 1),
       (nextval('statistique_seq'), '2020-07-02', 'REQUESTS', 1),
       (nextval('statistique_seq'), '2020-07-02', 'DOM_ELEMENTS', 1),
       (nextval('statistique_seq'), '2020-07-02', 'IMAGE', 1),
       (nextval('statistique_seq'), '2020-07-02', 'PDF', 1),

       (nextval('statistique_seq'), '2021-08-03', 'TRANSFERRED_DATA', 151),
       (nextval('statistique_seq'), '2021-08-03', 'REQUESTS', 151),
       (nextval('statistique_seq'), '2021-08-03', 'DOM_ELEMENTS', 151),
       (nextval('statistique_seq'), '2021-08-03', 'TRANSFERRED_DATA', 101),
       (nextval('statistique_seq'), '2021-08-03', 'REQUESTS', 101),
       (nextval('statistique_seq'), '2021-08-03', 'DOM_ELEMENTS', 101),
       (nextval('statistique_seq'), '2021-08-03', 'TRANSFERRED_DATA', 51),
       (nextval('statistique_seq'), '2021-08-03', 'REQUESTS', 51),
       (nextval('statistique_seq'), '2021-08-03', 'DOM_ELEMENTS', 51),
       (nextval('statistique_seq'), '2021-08-03', 'TRANSFERRED_DATA', 1),
       (nextval('statistique_seq'), '2021-08-03', 'REQUESTS', 1),
       (nextval('statistique_seq'), '2021-08-03', 'DOM_ELEMENTS', 1),
       (nextval('statistique_seq'), '2021-08-03', 'IMAGE', 1),
       (nextval('statistique_seq'), '2021-08-03', 'PDF', 1),

       (nextval('statistique_seq'), '2022-09-04', 'TRANSFERRED_DATA', 151),
       (nextval('statistique_seq'), '2022-09-04', 'REQUESTS', 151),
       (nextval('statistique_seq'), '2022-09-04', 'DOM_ELEMENTS', 151),
       (nextval('statistique_seq'), '2022-09-04', 'TRANSFERRED_DATA', 101),
       (nextval('statistique_seq'), '2022-09-04', 'REQUESTS', 101),
       (nextval('statistique_seq'), '2022-09-04', 'DOM_ELEMENTS', 101),
       (nextval('statistique_seq'), '2022-09-04', 'TRANSFERRED_DATA', 51),
       (nextval('statistique_seq'), '2022-09-04', 'REQUESTS', 51),
       (nextval('statistique_seq'), '2022-09-04', 'DOM_ELEMENTS', 51),
       (nextval('statistique_seq'), '2022-09-04', 'TRANSFERRED_DATA', 1),
       (nextval('statistique_seq'), '2022-09-04', 'REQUESTS', 1),
       (nextval('statistique_seq'), '2022-09-04', 'DOM_ELEMENTS', 1),
       (nextval('statistique_seq'), '2022-09-04', 'IMAGE', 1),
       (nextval('statistique_seq'), '2022-09-04', 'PDF', 1),

       (nextval('statistique_seq'), '2023-10-05', 'TRANSFERRED_DATA', 151),
       (nextval('statistique_seq'), '2023-10-05', 'REQUESTS', 151),
       (nextval('statistique_seq'), '2023-10-05', 'DOM_ELEMENTS', 151),
       (nextval('statistique_seq'), '2023-10-05', 'TRANSFERRED_DATA', 101),
       (nextval('statistique_seq'), '2023-10-05', 'REQUESTS', 101),
       (nextval('statistique_seq'), '2023-10-05', 'DOM_ELEMENTS', 101),
       (nextval('statistique_seq'), '2023-10-05', 'TRANSFERRED_DATA', 51),
       (nextval('statistique_seq'), '2023-10-05', 'REQUESTS', 51),
       (nextval('statistique_seq'), '2023-10-05', 'DOM_ELEMENTS', 51),
       (nextval('statistique_seq'), '2023-10-05', 'TRANSFERRED_DATA', 1),
       (nextval('statistique_seq'), '2023-10-05', 'REQUESTS', 1),
       (nextval('statistique_seq'), '2023-10-05', 'DOM_ELEMENTS', 1),
       (nextval('statistique_seq'), '2023-10-05', 'IMAGE', 1),
       (nextval('statistique_seq'), '2023-10-05', 'PDF', 1);

INSERT INTO valeurs_statistiques (id, key, value)
VALUES (1, 'Minimum', 1234), --'2020-06-01'
       (1, 'Maximum', 9012),
       (1, 'Moyenne', 5454),
       (1, 'Médiane', 5678),
       (2, 'Minimum', 12),
       (2, 'Maximum', 30),
       (2, 'Moyenne', 22.8),
       (2, 'Médiane', 25),
       (3, 'Minimum', 210),
       (3, 'Maximum', 290),
       (3, 'Moyenne', 251),
       (3, 'Médiane', 250),
       (4, 'Minimum', 1234),
       (4, 'Maximum', 4567),
       (4, 'Moyenne', 3011.6),
       (4, 'Médiane', 3456),
       (5, 'Minimum', 11),
       (5, 'Maximum', 20),
       (5, 'Moyenne', 15.2),
       (5, 'Médiane', 15),
       (6, 'Minimum', 123),
       (6, 'Maximum', 789),
       (6, 'Moyenne', 433.8),
       (6, 'Médiane', 456),
       (7, 'Minimum', 12345678),
       (7, 'Maximum', 45678901),
       (7, 'Moyenne', 29234942.8),
       (7, 'Médiane', 30123456),
       (8, 'Minimum', 6),
       (8, 'Maximum', 19),
       (8, 'Moyenne', 11.8),
       (8, 'Médiane', 12),
       (9, 'Minimum', 75),
       (9, 'Maximum', 200),
       (9, 'Moyenne', 130),
       (9, 'Médiane', 125),
       (10, 'Minimum', 12345678),
       (10, 'Maximum', 45678901),
       (10, 'Moyenne', 29234942.8),
       (10, 'Médiane', 30123456),
       (11, 'Minimum', 7),
       (11, 'Maximum', 42),
       (11, 'Moyenne', 23.4),
       (11, 'Médiane', 25),
       (12, 'Minimum', 60000),
       (12, 'Maximum', 95000),
       (12, 'Moyenne', 77000),
       (12, 'Médiane', 75000),
       (13, 'Nombre d''images au-dessus du seuil', 13),
       (13, 'Seuil', 100000),
       (14, 'Minimum', 52000),
       (14, 'Maximum', 97000),
       (14, 'Moyenne', 75000),
       (14, 'Médiane', 75000),
--'2021-07-02
       (1 + 14, 'Minimum', 1234),
       (1 + 14, 'Maximum', 8901),
       (1 + 14, 'Moyenne', 4767),
       (1 + 14, 'Médiane', 4567),
       (2 + 14, 'Minimum', 15),
       (2 + 14, 'Maximum', 30),
       (2 + 14, 'Moyenne', 22.8),
       (2 + 14, 'Médiane', 22),
       (3 + 14, 'Minimum', 220),
       (3 + 14, 'Maximum', 300),
       (3 + 14, 'Moyenne', 260),
       (3 + 14, 'Médiane', 260),
       (4 + 14, 'Minimum', 1234),
       (4 + 14, 'Maximum', 4567),
       (4 + 14, 'Moyenne', 3184.6),
       (4 + 14, 'Médiane', 3456),
       (5 + 14, 'Minimum', 10),
       (5 + 14, 'Maximum', 19),
       (5 + 14, 'Moyenne', 14.6),
       (5 + 14, 'Médiane', 14),
       (6 + 14, 'Minimum', 234),
       (6 + 14, 'Maximum', 789),
       (6 + 14, 'Moyenne', 522.6),
       (6 + 14, 'Médiane', 567),
       (7 + 14, 'Minimum', 15234567),
       (7 + 14, 'Maximum', 48765432),
       (7 + 14, 'Moyenne', 32602046.4),
       (7 + 14, 'Médiane', 32345678),
       (8 + 14, 'Minimum', 5),
       (8 + 14, 'Maximum', 17),
       (8 + 14, 'Moyenne', 10.8),
       (8 + 14, 'Médiane', 10),
       (9 + 14, 'Minimum', 60),
       (9 + 14, 'Maximum', 180),
       (9 + 14, 'Moyenne', 116),
       (9 + 14, 'Médiane', 110),
       (10 + 14, 'Minimum', 15234567),
       (10 + 14, 'Maximum', 48765432),
       (10 + 14, 'Moyenne', 32602046.4),
       (10 + 14, 'Médiane', 32345678),
       (11 + 14, 'Minimum', 8),
       (11 + 14, 'Maximum', 47),
       (11 + 14, 'Moyenne', 24.2),
       (11 + 14, 'Médiane', 22),
       (12 + 14, 'Minimum', 55000),
       (12 + 14, 'Maximum', 100000),
       (12 + 14, 'Moyenne', 78000),
       (12 + 14, 'Médiane', 80000),
       (13 + 14, 'Nombre d''images au-dessus du seuil', 9),
       (13 + 14, 'Seuil', 100000),
       (14 + 14, 'Minimum', 54000),
       (14 + 14, 'Maximum', 92000),
       (14 + 14, 'Moyenne', 73600),
       (14 + 14, 'Médiane', 73000),
--'2022-08-03
       (1 + 14 * 2, 'Minimum', 2345),
       (1 + 14 * 2, 'Maximum', 9012),
       (1 + 14 * 2, 'Moyenne', 5676),
       (1 + 14 * 2, 'Médiane', 5678),
       (2 + 14 * 2, 'Minimum', 11),
       (2 + 14 * 2, 'Maximum', 28),
       (2 + 14 * 2, 'Moyenne', 19),
       (2 + 14 * 2, 'Médiane', 19),
       (3 + 14 * 2, 'Minimum', 205),
       (3 + 14 * 2, 'Maximum', 285),
       (3 + 14 * 2, 'Moyenne', 245),
       (3 + 14 * 2, 'Médiane', 245),
       (4 + 14 * 2, 'Minimum', 1234),
       (4 + 14 * 2, 'Maximum', 4567),
       (4 + 14 * 2, 'Moyenne', 3184.6),
       (4 + 14 * 2, 'Médiane', 3456),
       (5 + 14 * 2, 'Minimum', 11),
       (5 + 14 * 2, 'Maximum', 20),
       (5 + 14 * 2, 'Moyenne', 15.4),
       (5 + 14 * 2, 'Médiane', 16),
       (6 + 14 * 2, 'Minimum', 123),
       (6 + 14 * 2, 'Maximum', 789),
       (6 + 14 * 2, 'Moyenne', 478.2),
       (6 + 14 * 2, 'Médiane', 456),
       (7 + 14 * 2, 'Minimum', 10987654),
       (7 + 14 * 2, 'Maximum', 50456789),
       (7 + 14 * 2, 'Moyenne', 30429228.8),
       (7 + 14 * 2, 'Médiane', 30234567),
       (8 + 14 * 2, 'Minimum', 7),
       (8 + 14 * 2, 'Maximum', 20),
       (8 + 14 * 2, 'Moyenne', 13),
       (8 + 14 * 2, 'Médiane', 13),
       (9 + 14 * 2, 'Minimum', 55),
       (9 + 14 * 2, 'Maximum', 195),
       (9 + 14 * 2, 'Moyenne', 127),
       (9 + 14 * 2, 'Médiane', 130),
       (10 + 14 * 2, 'Minimum', 10987654),
       (10 + 14 * 2, 'Maximum', 50456789),
       (10 + 14 * 2, 'Moyenne', 30429228.8),
       (10 + 14 * 2, 'Médiane', 30234567),
       (11 + 14 * 2, 'Minimum', 3),
       (11 + 14 * 2, 'Maximum', 40),
       (11 + 14 * 2, 'Moyenne', 21.6),
       (11 + 14 * 2, 'Médiane', 18),
       (12 + 14 * 2, 'Minimum', 50000),
       (12 + 14 * 2, 'Maximum', 95000),
       (12 + 14 * 2, 'Moyenne', 72000),
       (12 + 14 * 2, 'Médiane', 70000),
       (13 + 14 * 2, 'Nombre d''images au-dessus du seuil', 8),
       (13 + 14 * 2, 'Seuil', 100000),
       (14 + 14 * 2, 'Minimum', 55000),
       (14 + 14 * 2, 'Maximum', 90000),
       (14 + 14 * 2, 'Moyenne', 73400),
       (14 + 14 * 2, 'Médiane', 77000),
--'2023-09-04
       (1 + 14 * 3, 'Minimum', 1234),
       (1 + 14 * 3, 'Maximum', 8901),
       (1 + 14 * 3, 'Moyenne', 4989),
       (1 + 14 * 3, 'Médiane', 4567),
       (2 + 14 * 3, 'Minimum', 13),
       (2 + 14 * 3, 'Maximum', 30),
       (2 + 14 * 3, 'Moyenne', 21.4),
       (2 + 14 * 3, 'Médiane', 21),
       (3 + 14 * 3, 'Minimum', 215),
       (3 + 14 * 3, 'Maximum', 295),
       (3 + 14 * 3, 'Moyenne', 255),
       (3 + 14 * 3, 'Médiane', 255),
       (4 + 14 * 3, 'Minimum', 1234),
       (4 + 14 * 3, 'Maximum', 4567),
       (4 + 14 * 3, 'Moyenne', 3184.6),
       (4 + 14 * 3, 'Médiane', 3456),
       (5 + 14 * 3, 'Minimum', 12),
       (5 + 14 * 3, 'Maximum', 19),
       (5 + 14 * 3, 'Moyenne', 15.2),
       (5 + 14 * 3, 'Médiane', 15),
       (6 + 14 * 3, 'Minimum', 123),
       (6 + 14 * 3, 'Maximum', 678),
       (6 + 14 * 3, 'Moyenne', 411.6),
       (6 + 14 * 3, 'Médiane', 456),
       (7 + 14 * 3, 'Minimum', 15567890),
       (7 + 14 * 3, 'Maximum', 50678901),
       (7 + 14 * 3, 'Moyenne', 34678721),
       (7 + 14 * 3, 'Médiane', 35678901),
       (8 + 14 * 3, 'Minimum', 6),
       (8 + 14 * 3, 'Maximum', 18),
       (8 + 14 * 3, 'Moyenne', 11.6),
       (8 + 14 * 3, 'Médiane', 11),
       (9 + 14 * 3, 'Minimum', 70),
       (9 + 14 * 3, 'Maximum', 185),
       (9 + 14 * 3, 'Moyenne', 126),
       (9 + 14 * 3, 'Médiane', 120),
       (10 + 14 * 3, 'Minimum', 15567890),
       (10 + 14 * 3, 'Maximum', 50678901),
       (10 + 14 * 3, 'Moyenne', 34678721),
       (10 + 14 * 3, 'Médiane', 35678901),
       (11 + 14 * 3, 'Minimum', 5),
       (11 + 14 * 3, 'Maximum', 45),
       (11 + 14 * 3, 'Moyenne', 22.6),
       (11 + 14 * 3, 'Médiane', 20),
       (12 + 14 * 3, 'Minimum', 55000),
       (12 + 14 * 3, 'Maximum', 95000),
       (12 + 14 * 3, 'Moyenne', 75000),
       (12 + 14 * 3, 'Médiane', 75000),
       (13 + 14 * 3, 'Nombre d''images au-dessus du seuil', 6),
       (13 + 14 * 3, 'Seuil', 100000),
       (14 + 14 * 3, 'Minimum', 58000),
       (14 + 14 * 3, 'Maximum', 95000),
       (14 + 14 * 3, 'Moyenne', 74000),
       (14 + 14 * 3, 'Médiane', 70000),
--'2024-10-05
       (1 + 14 * 4, 'Minimum', 9012),
       (1 + 14 * 4, 'Maximum', 2345),
       (1 + 14 * 4, 'Moyenne', 5898),
       (1 + 14 * 4, 'Médiane', 5678),
       (2 + 14 * 4, 'Minimum', 10),
       (2 + 14 * 4, 'Maximum', 29),
       (2 + 14 * 4, 'Moyenne', 19.8),
       (2 + 14 * 4, 'Médiane', 20),
       (3 + 14 * 4, 'Minimum', 200),
       (3 + 14 * 4, 'Maximum', 280),
       (3 + 14 * 4, 'Moyenne', 246),
       (3 + 14 * 4, 'Médiane', 250),
       (4 + 14 * 4, 'Minimum', 1234),
       (4 + 14 * 4, 'Maximum', 4567),
       (4 + 14 * 4, 'Moyenne', 3184.6),
       (4 + 14 * 4, 'Médiane', 3456),
       (5 + 14 * 4, 'Minimum', 11),
       (5 + 14 * 4, 'Maximum', 20),
       (5 + 14 * 4, 'Moyenne', 15.8),
       (5 + 14 * 4, 'Médiane', 16),
       (6 + 14 * 4, 'Minimum', 234),
       (6 + 14 * 4, 'Maximum', 789),
       (6 + 14 * 4, 'Moyenne', 478.2),
       (6 + 14 * 4, 'Médiane', 456),
       (7 + 14 * 4, 'Minimum', 10987654),
       (7 + 14 * 4, 'Maximum', 50456789),
       (7 + 14 * 4, 'Moyenne', 30429228.8),
       (7 + 14 * 4, 'Médiane', 30234567),
       (8 + 14 * 4, 'Minimum', 7),
       (8 + 14 * 4, 'Maximum', 19),
       (8 + 14 * 4, 'Moyenne', 12.2),
       (8 + 14 * 4, 'Médiane', 12),
       (9 + 14 * 4, 'Minimum', 65),
       (9 + 14 * 4, 'Maximum', 175),
       (9 + 14 * 4, 'Moyenne', 121),
       (9 + 14 * 4, 'Médiane', 135),
       (10 + 14 * 4, 'Minimum', 10987654),
       (10 + 14 * 4, 'Maximum', 50456789),
       (10 + 14 * 4, 'Moyenne', 30429228.8),
       (10 + 14 * 4, 'Médiane', 30234567),
       (11 + 14 * 4, 'Minimum', 6),
       (11 + 14 * 4, 'Maximum', 38),
       (11 + 14 * 4, 'Moyenne', 20.2),
       (11 + 14 * 4, 'Médiane', 19),
       (12 + 14 * 4, 'Minimum', 60000),
       (12 + 14 * 4, 'Maximum', 100000),
       (12 + 14 * 4, 'Moyenne', 80000),
       (12 + 14 * 4, 'Médiane', 80000),
       (13 + 14 * 4, 'Nombre d''images au-dessus du seuil', 4),
       (13 + 14 * 4, 'Seuil', 100000),
       (14 + 14 * 4, 'Minimum', 54000),
       (14 + 14 * 4, 'Maximum', 91000),
       (14 + 14 * 4, 'Moyenne', 74600),
       (14 + 14 * 4, 'Médiane', 78000);

INSERT INTO legal_documents VALUES ('2024-12-01 00:00:00.0', nextval('legal_documents_seq'), 'v1.0.0', '
<h2>Article 1 - Champ d''application</h2>

<p>Les présentes conditions générales d''utilisation (ci-après "CGU")
précisent le cadre juridique de "MISIS" (ci-après la
"Plateforme") et définissent les conditions d''accès et d''utilisation des
Services par l''Utilisateur. Ce dernier est nécessairement un membre du
personnel d''une administration publique française.</p>

<h2>Article 2 - Objet de la Plateforme</h2>

<ul>
<li>la création et la gestion de comptes d''utilisateurs ;</li>
<li>l''aide, pas à pas, des entités publiques à suivre et minimiser l''impact environnemental de leur site web.</li>
</ul>

<h2>Article 3 - Définitions</h2>

<p>"L''Utilisateur" est toute personne physique, membre du
personnel d''une administration publique française, ayant un accès via ProConnect.</p>

<p>Les "Services" sont les fonctionnalités offertes par la Plateforme pour
répondre à ses finalités.</p>

<p>La "Plateforme" est la solution "MISIS"
conçue par la Direction du Numérique des Ministères Territoires Ecologie Logement, qui initie et accompagne la transformation numérique du ministère.</p>

<p>"L''Éditeur" est la personne morale qui met à la disposition du public la
Plateforme, à savoir la DIRECTION DU NUMERIQUE - SITE AIX-EN-PROVENCE.</p>

<h2>Article 4 - Fonctionnalités</h2>

<h3>4.1 Compte Utilisateur</h3>

<p>L''Utilisateur est titulaire d''un compte. Il s''engage à choisir un mot
de passe sécurisé, protéger la sécurité et la confidentialité de son mot
de passe, et est responsable de toute utilisation de son compte. Toute
utilisation abusive doit être signalée à l''Éditeur aux coordonnées
indiquées dans les Mentions
légales. Tout
utilisateur peut procéder à la modification des données liées à son
compte et à sa suppression.</p>

<h3>4.2 Contributeur</h3>

<p>L''Utilisateur peut ajouter un contributeur à un service numérique
référencé sur MISIS. Dans ce cas, il renseigne l''adresse
de messagerie électronique professionnelle du contributeur.</p>

<p>Lorsque le contributeur a déjà un compte personnel, l''acceptation de
l''invitation à contribuer suffit. Lorsque le contributeur n''a pas de
compte, il est invité à s''inscrire et se voit alors appliquer les
droits et obligations d''un Utilisateur.</p>

<p>Les actions du collaborateur et de l''Utilisateur modifient les
informations renseignées concernant le service numérique référencé, ils
sont chacun responsables de l''exactitude de leurs déclarations
respectives.</p>

<h3>4.3 Notifications et Messages</h3>

<p>L''Utilisateur comprend que des notifications et des messages peuvent
lui être communiqués par les moyens suivants : (i) via la Plateforme
(compte Utilisateur) ou (ii) envoyés aux coordonnées fournies par
l''Utilisateur (adresse e-mail). Si ses coordonnées ne sont pas à jour,
l''Utilisateur est conscient que des notifications importantes sont
susceptibles de ne pas lui parvenir. Par conséquent, l''Utilisateur
s''engage à tenir à jour ses coordonnées.</p>

<h2>Article 5 - Responsabilités</h2>

<h3>5.1 Responsabilités de l''Éditeur</h3>

<p>Les sources des informations diffusées sur la Plateforme sont réputées
fiables mais elle ne garantit pas être exempte de défauts, d''erreurs ou
d''omissions.</p>

<p>L''Éditeur s''engage à la sécurisation de la Plateforme, notamment en
prenant toutes les mesures nécessaires permettant de garantir la
sécurité et la confidentialité des informations fournies.</p>

<p>L''Éditeur fournit les moyens nécessaires et raisonnables pour assurer un
accès continu à la Plateforme. Il se réserve la liberté de faire
évoluer, de modifier ou de suspendre, sans préavis, la Plateforme pour
des raisons de maintenance ou pour tout autre motif jugé nécessaire.</p>

<p>En cas de manquement à une ou plusieurs des stipulations des présentes
CGU, l''Éditeur se réserve le droit de supprimer le compte de
l''Utilisateur responsable.</p>

<h3>5.2 Responsabilités de l''Utilisateur</h3>

<p>Dans le cadre de l''utilisation de la Plateforme, l''Utilisateur
s''engage à :</p>

<ul>
<li>ne créer qu''un seul compte utilisateur et ne communiquer que des
    informations, fichiers et autres contenus conformes à la réalité,
    honnêtes et loyaux ;</li>

<li>n''effectuer de suivi de site web que s''il en est propriétaire, membre actuel de l''équipe projet ou s''il possède l''autorisation explicite de celle-ci ;</li>

<li>ne pas divulguer via la Plateforme des propos ou des contenus
    illicites, et notamment tous contenus contrefaits, diffamatoires,
    injurieux, insultants, obscènes, offensants, discriminatoires,
    violents, xénophobes, incitant à la haine raciale ou faisant
    l''apologie du terrorisme, ou tout autre contenu contraire à la
    législation et réglementation applicable ainsi qu''aux bonnes mœurs
    et aux règles de bienséance ;</li>

<li>ne pas intégrer et diffuser via la Plateforme du contenu qui serait
    contraire à la finalité de celui-ci ;</li>

<li>ne pas communiquer ou envoyer, par l''intermédiaire de la Plateforme
    , du contenu, quel qu''il soit, qui comprendrait des liens pointant
    vers des sites internet illicites, offensants ou incompatibles avec
    la finalité de la Plateforme ;</li>

<li>ne pas utiliser la Plateforme pour envoyer massivement des messages
    non sollicités (publicitaires ou autres).</li>
</ul>

<p>Il est rappelé que toute personne procédant à une fausse déclaration
pour elle-même ou pour autrui s''expose, notamment, aux sanctions prévues
à l''article 441-1 du code pénal, prévoyant des peines pouvant aller
jusqu''à trois ans d''emprisonnement et 45 000 euros d''amende.</p>

<h2>Article 6 - Exclusions de certains services</h2>

<p>Les services numériques suivants ne peuvent pas être référencés sur
MISIS :</p>

<ul>
<li>Les services essentiels au fonctionnement de la société ou de
    l''économie, des opérateurs désignés opérateurs de services
    essentiels (loi n° 2018-133 du 26 février 2018 qui présente les
    mesures de transposition de la direction européenne sur la sécurité
    des réseaux et des systèmes d''information, décret n° 2018-384 du 23
    mai 2018 en détaillant les modalités d''application).</li>

<li>Les services pour lesquels l''atteinte à la sécurité ou au bon
    fonctionnement engendrerait des conséquences importantes sur le
    potentiel scientifique et technique (décret n° 2011-1425 du 2
    novembre 2011 portant application de l''article 413-7 du code pénal
    relatif à la protection du potentiel scientifique et technique de la
    nation).</li>

<li>Les services pour lesquels l''atteinte à la sécurité ou au
    fonctionnement engendrerait des conséquences importantes sur le
    potentiel de guerre ou économique, la sécurité ou la capacité de
    survie de la Nation (articles L. 1332-1 à L. 1332-7 du Code de la
    Défense).</li>

<li>Les services traitant d''informations protégées par le secret de la
    défense nationale ou équivalent, ou de niveau diffusion restreinte
    (IGI 1300 relative au secret de la défense nationale, l''IGI 2102
    relatives aux informations ou supports classifiés de l''Union
    européenne, l''IGI 2100 relative à la protection des supports
    classifiés dans le cadre de l''OTAN, l''IGI 901 définissant les
    règles relatives à la protection des informations sensibles et à
    diffusion restreinte).</li>
</ul>

<h2>Article 7 - Mise à jour des conditions générales d''utilisation</h2>

<p>Les termes des présentes CGU peuvent être amendés à tout moment, en
fonction des modifications apportées à la Plateforme, de l''évolution de
la législation ou pour tout autre motif jugé nécessaire. Chaque
modification donne lieu à une nouvelle version qui est acceptée par les
parties</p>');

INSERT INTO suivi_de_site (id, name, url, created_date, modified_date, created_by, modified_by)
VALUES (nextval('suivi_de_site_seq'), 'Gitlab Page', 'https://misis-test-website-pub-numeco-misis-319bda392a27d3f07057d9125aa.gitlab-pages.din.developpement-durable.gouv.fr/', '2000-01-01T00:00:0.000000', '2000-01-01T00:00:0.000000', 1, 1);

INSERT INTO permission (discriminator, domain_id, id, user_id)
VALUES (1, (SELECT last_value FROM suivi_de_site_seq), nextval('permission_seq'), 1);

INSERT INTO permission_permission_type (permission_id, allowed_id)
VALUES ((SELECT last_value FROM permission_seq), 1),
       ((SELECT last_value FROM permission_seq), 2),
       ((SELECT last_value FROM permission_seq), 3);

INSERT INTO groupe_de_pages(id, name, periodicite_du_suivi, methode_de_creation_de_groupe, suivi_de_site_id, created_date, modified_date, created_by, modified_by)
VALUES (nextval('groupe_de_pages_seq'), 'Mes Gitlab Pages', 'QUOTIDIEN', 'MANUELLE', (SELECT last_value FROM suivi_de_site_seq), '2000-01-01T00:00:0.000000', '2000-01-01T00:00:0.000000', 1, 1);

INSERT INTO page (id, url, dom_elements_count, http_requests_count, data_decoded_in_bytes, data_transfered_in_bytes, groupe_de_pages_id, analyse_statut, analysed_at, modified_at)
VALUES (nextval('page_seq'),
        'https://misis-test-website-pub-numeco-misis-319bda392a27d3f07057d9125aa.gitlab-pages.din.developpement-durable.gouv.fr/Un-roman-Copie-Colle-et-c-fait',
        null,
        null,
        null,
        null,
        (SELECT last_value FROM groupe_de_pages_seq),
        'NOUVEAU',
        null,
        '2025-01-01T00:00:0.000000'),
       (nextval('page_seq'),
        'https://misis-test-website-pub-numeco-misis-319bda392a27d3f07057d9125aa.gitlab-pages.din.developpement-durable.gouv.fr/la-flemme/',
        null,
        null,
        null,
        null,
        (SELECT last_value FROM groupe_de_pages_seq),
        'NOUVEAU',
        null,
        '2025-01-01T00:00:0.000000'),
       (nextval('page_seq'),
        'https://misis-test-website-pub-numeco-misis-319bda392a27d3f07057d9125aa.gitlab-pages.din.developpement-durable.gouv.fr/L%27Importance-des-Lecteurs-Vid%C3%A9o-%C3%89co-Responsables-sur-un-Site-Web/',
        null,
        null,
        null,
        null,
        (SELECT last_value FROM groupe_de_pages_seq),
        'NOUVEAU',
        null,
        '2025-01-01T00:00:0.000000'),
       (nextval('page_seq'),
        'https://misis-test-website-pub-numeco-misis-319bda392a27d3f07057d9125aa.gitlab-pages.din.developpement-durable.gouv.fr/Le-Travail-d%27%C3%89quipe-Une-Aventure-Compl%C3%A8tement-Folle/',
        null,
        null,
        null,
        null,
        (SELECT last_value FROM groupe_de_pages_seq),
        'NOUVEAU',
        null,
        '2025-01-01T00:00:0.000000');

-- changeset cindy-canevet:9999-2
INSERT INTO legal_documents VALUES (NOW(), nextval('legal_documents_seq'), 'v0.0.1', '
<h2>Article 1 - Champ d''application</h2>

<p>Les présentes conditions générales d''utilisation (ci-après "CGU")
précisent le cadre juridique de "MISIS" (ci-après la
"Plateforme") et définissent les conditions d''accès et d''utilisation des
Services par l''Utilisateur. Ce dernier est nécessairement un membre du
personnel d''une administration publique française.</p>

<h2>Article 2 - Objet de la Plateforme</h2>

<ul>
<li>la création et la gestion de comptes d''utilisateurs ;</li>
<li>l''aide, pas à pas, des entités publiques à suivre et minimiser l''impact environnemental de leur site web.</li>
</ul>

<h2>Article 3 - Définitions</h2>

<p>L''"Utilisateur" est toute personne physique, membre du
personnel d''une administration publique française, ayant un accès via ProConnect.</p>

<p>Les "Services" sont les fonctionnalités offertes par la Plateforme pour
répondre à ses finalités.</p>

<p>La "Plateforme" est la solution "MISIS"
conçue par la Direction du Numérique des Ministères Territoires Écologie Logement, qui initie et accompagne la transformation numérique du ministère.</p>

<p>L''"Éditeur" est la personne morale qui met à la disposition du public la
Plateforme, à savoir la DIRECTION DU NUMÉRIQUE - SITE AIX-EN-PROVENCE.</p>

<h2>Article 4 - Fonctionnalités</h2>

<h3>4.1 Compte Utilisateur</h3>

<p>L''Utilisateur est titulaire d''un compte. Il s''engage à choisir un mot
de passe sécurisé, protéger la sécurité et la confidentialité de son mot
de passe, et est responsable de toute utilisation de son compte. Toute
utilisation abusive doit être signalée à l''Éditeur aux coordonnées
indiquées dans les Mentions
légales. Tout
utilisateur peut procéder à la modification des données liées à son
compte et à sa suppression.</p>

<h3>4.2 Contributeur</h3>

<p>L''Utilisateur peut ajouter un contributeur à un service numérique
référencé sur MISIS. Dans ce cas, il renseigne l''adresse
de messagerie électronique professionnelle du contributeur.</p>

<p>Lorsque le contributeur a déjà un compte personnel, l''acceptation de
l''invitation à contribuer suffit. Lorsque le contributeur n''a pas de
compte, il est invité à s''inscrire et se voit alors appliquer les
droits et obligations d''un Utilisateur.</p>

<p>Les actions du collaborateur et de l''Utilisateur modifient les
informations renseignées concernant le service numérique référencé, ils
sont chacun responsables de l''exactitude de leurs déclarations
respectives.</p>

<h3>4.3 Notifications et Messages</h3>

<p>L''Utilisateur comprend que des notifications et des messages peuvent
lui être communiqués par les moyens suivants : (i) via la Plateforme
(compte Utilisateur) ou (ii) envoyés aux coordonnées fournies par
l''Utilisateur (adresse e-mail). Si ses coordonnées ne sont pas à jour,
l''Utilisateur est conscient que des notifications importantes sont
susceptibles de ne pas lui parvenir. Par conséquent, l''Utilisateur
s''engage à tenir à jour ses coordonnées.</p>

<h2>Article 5 - Responsabilités</h2>

<h3>5.1 Responsabilités de l''Éditeur</h3>

<p>Les sources des informations diffusées sur la Plateforme sont réputées
fiables mais elle ne garantit pas être exempte de défauts, d''erreurs ou
d''omissions.</p>

<p>L''Éditeur s''engage à la sécurisation de la Plateforme, notamment en
prenant toutes les mesures nécessaires permettant de garantir la
sécurité et la confidentialité des informations fournies.</p>

<p>L''Éditeur fournit les moyens nécessaires et raisonnables pour assurer un
accès continu à la Plateforme. Il se réserve la liberté de faire
évoluer, de modifier ou de suspendre, sans préavis, la Plateforme pour
des raisons de maintenance ou pour tout autre motif jugé nécessaire.</p>

<p>En cas de manquement à une ou plusieurs des stipulations des présentes
CGU, l''Éditeur se réserve le droit de supprimer le compte de
l''Utilisateur responsable.</p>

<h3>5.2 Responsabilités de l''Utilisateur</h3>

<p>Dans le cadre de l''utilisation de la Plateforme, l''Utilisateur
s''engage à :</p>

<ul>
<li>ne créer qu''un seul compte utilisateur et ne communiquer que des
    informations, fichiers et autres contenus conformes à la réalité,
    honnêtes et loyaux ;</li>

<li>n''effectuer de suivi de site web que s''il en est propriétaire, membre actuel de l''équipe projet ou s''il possède l''autorisation explicite de celle-ci ;</li>

<li>ne pas divulguer via la Plateforme des propos ou des contenus
    illicites, et notamment tous contenus contrefaits, diffamatoires,
    injurieux, insultants, obscènes, offensants, discriminatoires,
    violents, xénophobes, incitant à la haine raciale ou faisant
    l''apologie du terrorisme, ou tout autre contenu contraire à la
    législation et réglementation applicable ainsi qu''aux bonnes mœurs
    et aux règles de bienséance ;</li>

<li>ne pas intégrer et diffuser via la Plateforme du contenu qui serait
    contraire à la finalité de celui-ci ;</li>

<li>ne pas communiquer ou envoyer, par l''intermédiaire de la Plateforme
    , du contenu, quel qu''il soit, qui comprendrait des liens pointant
    vers des sites internet illicites, offensants ou incompatibles avec
    la finalité de la Plateforme ;</li>

<li>ne pas utiliser la Plateforme pour envoyer massivement des messages
    non sollicités (publicitaires ou autres).</li>
</ul>

<p>Il est rappelé que toute personne procédant à une fausse déclaration
pour elle-même ou pour autrui s''expose, notamment, aux sanctions prévues
à l''article 441-1 du code pénal, prévoyant des peines pouvant aller
jusqu''à trois ans d''emprisonnement et 45 000 euros d''amende.</p>

<h2>Article 6 - Exclusions de certains services</h2>

<p>Les services numériques suivants ne peuvent pas être référencés sur
MISIS :</p>

<ul>
<li>Les services essentiels au fonctionnement de la société ou de
    l''économie, des opérateurs désignés opérateurs de services
    essentiels (loi n° 2018-133 du 26 février 2018 qui présente les
    mesures de transposition de la direction européenne sur la sécurité
    des réseaux et des systèmes d''information, décret n° 2018-384 du 23
    mai 2018 en détaillant les modalités d''application).</li>

<li>Les services pour lesquels l''atteinte à la sécurité ou au bon
    fonctionnement engendrerait des conséquences importantes sur le
    potentiel scientifique et technique (décret n° 2011-1425 du 2
    novembre 2011 portant application de l''article 413-7 du code pénal
    relatif à la protection du potentiel scientifique et technique de la
    nation).</li>

<li>Les services pour lesquels l''atteinte à la sécurité ou au
    fonctionnement engendrerait des conséquences importantes sur le
    potentiel de guerre ou économique, la sécurité ou la capacité de
    survie de la Nation (articles L. 1332-1 à L. 1332-7 du Code de la
    Défense).</li>

<li>Les services traitant d''informations protégées par le secret de la
    défense nationale ou équivalent, ou de niveau diffusion restreinte
    (IGI 1300 relative au secret de la défense nationale, l''IGI 2102
    relatives aux informations ou supports classifiés de l''Union
    européenne, l''IGI 2100 relative à la protection des supports
    classifiés dans le cadre de l''OTAN, l''IGI 901 définissant les
    règles relatives à la protection des informations sensibles et à
    diffusion restreinte).</li>
</ul>

<h2>Article 7 - Mise à jour des conditions générales d''utilisation</h2>

<p>Les termes des présentes CGU peuvent être amendés à tout moment, en
fonction des modifications apportées à la Plateforme, de l''évolution de
la législation ou pour tout autre motif jugé nécessaire. Chaque
modification donne lieu à une nouvelle version qui est acceptée par les
parties.</p>');