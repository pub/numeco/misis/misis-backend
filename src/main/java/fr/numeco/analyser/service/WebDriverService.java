package fr.numeco.analyser.service;

import org.openqa.selenium.remote.RemoteWebDriver;

import fr.numeco.analyser.bean.ExtractedPageInfo;
import fr.numeco.misis.analyzer.dto.PageDto;
import io.smallrye.mutiny.tuples.Tuple2;

public interface WebDriverService {

    Tuple2<RemoteWebDriver, PageDto> createNewTuple(final PageDto pageDto);

    Tuple2<PageDto, ExtractedPageInfo> extractInformation(final Tuple2<RemoteWebDriver, PageDto> tuple);
}
