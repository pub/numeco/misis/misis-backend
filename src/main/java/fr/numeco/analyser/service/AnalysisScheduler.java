package fr.numeco.analyser.service;

import fr.numeco.analyser.AnalyzerConfig;
import fr.numeco.misis.analyzer.service.AnalyserService;
import io.quarkus.scheduler.Scheduled;
import static io.quarkus.scheduler.Scheduled.ConcurrentExecution.SKIP;
import io.quarkus.scheduler.ScheduledExecution;
import jakarta.enterprise.context.ApplicationScoped;
import lombok.RequiredArgsConstructor;

@ApplicationScoped
@RequiredArgsConstructor
public class AnalysisScheduler {

    private final AnalyserService analyserService;
    private final AnalyzerConfig analyzerConfig;

    @Scheduled(identity = "AnalysisScheduler", cron = "{analyser.refresh.schedule.cron.expression}", concurrentExecution = SKIP, skipExecutionIf = AnalyserService.class)
    void scheduleGroupesDePagesAnalysis(final ScheduledExecution execution) {
        analyserService.process(0, analyzerConfig.paginationSize()-1);
    }

}
