package fr.numeco.analyser.service;

import fr.numeco.misis.analyzer.dto.PageDto;
import io.quarkus.spring.data.runtime.FunctionalityNotImplemented;
import io.smallrye.mutiny.Multi;

import java.util.List;
import java.util.stream.Stream;

public interface NetworkAnalyser {
    default Stream<PageDto> analyse(List<PageDto> pages) {
        return analyse(pages.stream()).subscribe().asStream();
    }

    default Multi<PageDto> analyse(Stream<PageDto> pages) {
        throw new FunctionalityNotImplemented(this.getClass().getSimpleName(), Thread.currentThread().getStackTrace()[1].getMethodName());
    }
}
