package fr.numeco.analyser.service.impl;

import java.net.MalformedURLException;
import java.net.URI;
import java.util.Optional;

import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;

import fr.numeco.analyser.AnalyzerConfig;
import fr.numeco.analyser.service.WebDriverProvider;
import jakarta.enterprise.context.ApplicationScoped;
import lombok.AllArgsConstructor;

@ApplicationScoped
@AllArgsConstructor
public class DynamicWebDriverProvider implements WebDriverProvider {

    private static final String HEADLESS_CHROME_ARGUMENT = "--headless=new";
    private static final String BI_DIRECTIONAL_CAPABILITY = "webSocketUrl";

    private final AnalyzerConfig analyzerConfig;

    @Override
    public RemoteWebDriver createDriverInstance() throws MalformedURLException {
        final ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments(HEADLESS_CHROME_ARGUMENT);
        chromeOptions.setCapability(BI_DIRECTIONAL_CAPABILITY, Boolean.TRUE);

        final Optional<String> remoteUrl = analyzerConfig.remoteUrl();
        if (remoteUrl.isPresent()) {
            return new RemoteWebDriver(URI.create(remoteUrl.get()).toURL(), chromeOptions, analyzerConfig.enableTracing());
        } else {
            throw new MalformedURLException(String.format("'%s' is not a valid URL", remoteUrl));
        }
    }

}
