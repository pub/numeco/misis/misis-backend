package fr.numeco.analyser.service.impl;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.stream.Stream;

import org.openqa.selenium.SessionNotCreatedException;
import org.openqa.selenium.devtools.v131.network.model.ResourceType;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.numeco.analyser.AnalyzerConfig;
import fr.numeco.analyser.bean.ExtractedPageInfo;
import fr.numeco.analyser.bean.ExtractedRessourceInfo;
import fr.numeco.analyser.errors.MisisPageAnalyserException;
import fr.numeco.analyser.service.NetworkAnalyser;
import fr.numeco.analyser.service.WebDriverService;
import fr.numeco.misis.analyzer.dto.PageDto;
import fr.numeco.misis.analyzer.dto.RessourceDto;
import fr.numeco.misis.enums.AnalyseStatut;
import fr.numeco.misis.enums.RessourceType;
import io.quarkus.logging.Log;
import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;
import io.smallrye.mutiny.infrastructure.Infrastructure;
import io.smallrye.mutiny.tuples.Tuple2;
import jakarta.enterprise.context.ApplicationScoped;
import lombok.AllArgsConstructor;

@ApplicationScoped
@AllArgsConstructor
public class MisisNetworkAnalyser implements NetworkAnalyser {

    private final WebDriverService webDriverService;
    private final AnalyzerConfig analyzerConfig;

    @Override
    public Multi<PageDto> analyse(final Stream<PageDto> stream) {
        if (Objects.isNull(stream)) {
            return Multi.createFrom().empty();
        } else {
            return Multi.createFrom().items(stream)
                    .emitOn(Infrastructure.getDefaultExecutor())
                    .runSubscriptionOn(Infrastructure.getDefaultExecutor())
                    .onItem().transformToUni(this::pageProcessing)
                    .merge(analyzerConfig.concurrency())
                    ;
        }
    }

    protected Uni<PageDto> pageProcessing(final PageDto pageDto) {
        if (Log.isTraceEnabled()) {
            final String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
            Log.tracef(analyzerConfig.trace().format(), methodName, pageDto.getId());
        }

        return Uni.createFrom().item(pageDto)
                .emitOn(Infrastructure.getDefaultExecutor())
                .runSubscriptionOn(Infrastructure.getDefaultExecutor())
                .onItem().delayIt().by(Duration.ofMillis(200))
                .onItem().transform(webDriverService::createNewTuple)
                .onItem().transform(webDriverService::extractInformation)
                .onItem().transform(this::mergeDataIntoDto)
                .onFailure(MisisPageAnalyserException.class).recoverWithItem(this::invalidPage)
                .onFailure(SessionNotCreatedException.class).recoverWithItem(() -> {
                    pageDto.setAnalyseStatut(AnalyseStatut.TERMINEE);
                    Log.error(String.format("Session not created with page id %s with url %s", pageDto.getId(), pageDto.getUrl()));
                    return pageDto;
                })
                ;
    }

    private PageDto invalidPage(final Throwable throwable) {
        if (throwable instanceof MisisPageAnalyserException misisPageAnalyserException) {
            final PageDto pageDto = misisPageAnalyserException.getPageDto();
            if (Objects.nonNull(pageDto)) {
                pageDto.setAnalyseStatut(misisPageAnalyserException.getAnalyseStatut());
                Log.warn(String.format("unable to open page id %s with url %s", pageDto.getId(), pageDto.getUrl()), throwable);
            }
            return pageDto;
        }
        throw new MisisPageAnalyserException("unexpected exception", throwable);
    }

    protected PageDto mergeDataIntoDto(final Tuple2<PageDto, ExtractedPageInfo> tuple) {
        final PageDto pageDto = tuple.getItem1();
        final ExtractedPageInfo extractedPageInfo = tuple.getItem2();

        if (Log.isTraceEnabled()) {
            final String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
            Log.debugf(analyzerConfig.trace().format(), methodName, pageDto.getId());
        } else if (Log.isDebugEnabled()) {
            final String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
            try {
                Log.debugf(analyzerConfig.debug().format(), methodName, pageDto.getId(), new ObjectMapper().writeValueAsString(extractedPageInfo));
            } catch (JsonProcessingException e) {
                Log.debugf("unable to log [%s]", methodName, e);
            }
        }

        final PageDto toPersist = new PageDto(
                pageDto.getId(),
                pageDto.getUrl(),
                extractedPageInfo.getTitle(),
                extractedPageInfo.getDataTransferedInBytes(),
                extractedPageInfo.getDataDecodedInBytes(),
                extractedPageInfo.getHttpRequestsCount(),
                extractedPageInfo.getDomElementsCount(),
                LocalDateTime.now(),
                AnalyseStatut.TERMINEE
        );

        extractedPageInfo.getRessources().values().forEach(extractedRessourceInfo -> addRessource(extractedRessourceInfo, toPersist));

        return toPersist;
    }

    protected void addRessource(final ExtractedRessourceInfo extractedRessourceInfo, final PageDto toPersist) {
        if (extractedRessourceInfo.isValid()) {
            final RessourceDto ressourceDto = new RessourceDto();

            ressourceDto.setUrl(extractedRessourceInfo.getUrl());
            ressourceDto.setSizeInBytes(extractedRessourceInfo.getSizeInBytes());
            ressourceDto.setLoadingType(extractedRessourceInfo.getLoadingType());

            switch (extractedRessourceInfo.getResourceType()) {
                case ResourceType.IMAGE :
                    ressourceDto.setType(RessourceType.IMAGE);
                    break;
                case MEDIA:
                    ressourceDto.setType(RessourceType.STREAMING);
                    break;
                case ResourceType.DOCUMENT :
                    if (Objects.nonNull(extractedRessourceInfo.getExtension())) {
                        analyzerConfig.documents().extensions().entrySet().stream()
                                .filter(entry -> entry.getValue().contains(extractedRessourceInfo.getExtension()))
                                .findFirst()
                                .ifPresentOrElse(
                                        entry -> ressourceDto.setType(entry.getKey()),
                                        () -> ressourceDto.setType(RessourceType.UNDEFINED));
                    }
                    break;
                default : Log.debugf("unmanaged type [%s] with mimetype [%s] for url [%s] in page id [%s] ... ignored!", extractedRessourceInfo.getResourceType(), extractedRessourceInfo.getMimeType(), extractedRessourceInfo.getUrl(), toPersist.getId());
            }

            toPersist.getRessources().add(ressourceDto);
        }
    }
}
