package fr.numeco.analyser.service.impl;

import fr.numeco.analyser.AnalyzerConfig;
import fr.numeco.analyser.service.NetworkAnalyser;
import fr.numeco.misis.analyzer.dto.PageDto;
import io.quarkus.runtime.StartupEvent;
import io.smallrye.mutiny.Multi;
import jakarta.enterprise.event.Observes;
import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import org.jboss.logging.Logger;

import java.util.List;
import java.util.stream.Stream;

@Singleton
public class DynamicNetworkAnalyser implements NetworkAnalyser {

    private final AnalyzerConfig analyzerConfig;
    private final NetworkAnalyser networkAnalyser;

    private static final Logger log = Logger.getLogger(DynamicNetworkAnalyser.class);

    @Inject
    public DynamicNetworkAnalyser(FakeNetworkAnalyser fakeNetworkAnalyser, MisisNetworkAnalyser misisNetworkAnalyser,
            AnalyzerConfig analyzerConfig) {
        this.analyzerConfig = analyzerConfig;
        if (analyzerConfig.remoteUrl().isEmpty()) {
            this.networkAnalyser = fakeNetworkAnalyser;
            log.info("Using fake network analyser");
        } else {
            this.networkAnalyser = misisNetworkAnalyser;
            log.info("Using real network analyser");
        }
    }

    void startup(@Observes StartupEvent event) {
        // Force this bean to be instantiated on startup
        // to display which analyzer is used
    }

    @Override
    public Multi<PageDto> analyse(Stream<PageDto> pages) {
        return networkAnalyser.analyse(pages);
    }

    @Override
    public Stream<PageDto> analyse(List<PageDto> pages) {
        return networkAnalyser.analyse(pages);
    }

    public boolean isFake() {
        return analyzerConfig.remoteUrl().isEmpty();
    }
}
