package fr.numeco.analyser.service.impl;

import fr.numeco.analyser.AnalyzerConfig;
import fr.numeco.analyser.Constants;
import fr.numeco.analyser.bean.ExtractedPageInfo;
import fr.numeco.analyser.errors.MisisPageAnalyserException;
import fr.numeco.analyser.errors.MisisRemoteMalformedURLException;
import fr.numeco.analyser.errors.MisisWebDriverResponseStatus;
import fr.numeco.analyser.service.WebDriverProvider;
import fr.numeco.analyser.service.WebDriverService;
import fr.numeco.misis.analyzer.dto.PageDto;
import fr.numeco.misis.enums.AnalyseStatut;
import fr.numeco.misis.enums.LoadingType;
import fr.numeco.misis.enums.RessourceType;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.quarkus.logging.Log;
import io.smallrye.mutiny.tuples.Tuple2;
import jakarta.enterprise.context.ApplicationScoped;
import lombok.AllArgsConstructor;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.bidi.network.ResponseDetails;
import org.openqa.selenium.devtools.DevTools;
import org.openqa.selenium.devtools.HasDevTools;
import org.openqa.selenium.devtools.v131.dom.DOM;
import org.openqa.selenium.devtools.v131.network.Network;
import org.openqa.selenium.devtools.v131.network.model.DataReceived;
import org.openqa.selenium.devtools.v131.network.model.ResourceType;
import org.openqa.selenium.devtools.v131.network.model.ResponseReceived;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.time.Duration;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;

@ApplicationScoped
@AllArgsConstructor
public class MisisWebDriverService implements WebDriverService {

    private static final String ANY_WILDCARD = "*";
    private static final String HTML_TAG = "html";
    private static final String ATTRIBUTE_CSS_SELECTOR = "[%s$='%s']";
    private static final String CSS_SELECTOR_DELIMITER = ", ";

    private final WebDriverProvider webDriverProvider;
    private final AnalyzerConfig analyzerConfig;

    @Override
    public Tuple2<RemoteWebDriver, PageDto> createNewTuple(final PageDto pageDto) {
        printTrace(pageDto.getId());

        try {
            return Tuple2.of(webDriverProvider.createDriverInstance(), pageDto);
        } catch (MalformedURLException e) {
            throw new MisisRemoteMalformedURLException(e);
        }
    }

    @Override
    public Tuple2<PageDto, ExtractedPageInfo> extractInformation(final Tuple2<RemoteWebDriver, PageDto> tuple) {
        final RemoteWebDriver webDriver = (RemoteWebDriver) new Augmenter().augment(tuple.getItem1());
        final PageDto pageDto = tuple.getItem2();
        final ExtractedPageInfo extractedPageInfo = new ExtractedPageInfo();

        DevTools devTools = ((HasDevTools) webDriver).getDevTools();

        printTrace(pageDto.getId());

        try (org.openqa.selenium.bidi.module.Network network = new org.openqa.selenium.bidi.module.Network(webDriver)) {
            CompletableFuture<ResponseDetails> completableFuture = new CompletableFuture<>();

            network.onResponseCompleted(responseDetails -> {
                final String requestUrl = responseDetails.getRequest().getUrl();

                if (pageDto.getUrl().equals(requestUrl)) {
                    completableFuture.complete(responseDetails);
                }
            });

            setupDevTools(devTools, pageDto.getId());
            addLandingListener(devTools, extractedPageInfo, pageDto.getId());
            openPage(pageDto, webDriver);

            final ResponseDetails responseDetails = completableFuture.get(2000, TimeUnit.MILLISECONDS);
            final long responseStatus = responseDetails.getResponseData().getStatus();
            if (responseStatus != HttpResponseStatus.OK.code()) {
                throw new WebDriverException(new MisisWebDriverResponseStatus(responseDetails.getResponseData().getStatus()));
            }
            addScrollingListener(devTools, extractedPageInfo, pageDto.getId());
            scrollToEndPage(webDriver, pageDto.getId());

            extractedPageInfo.setTitle(webDriver.getTitle());
            extractedPageInfo.setDomElementsCount(requestDomElementsCount(devTools));

            final Map<String, String> ressourceSelector = analyzerConfig.documents().extensions().entrySet().stream()
                    .collect(Collectors.toMap(
                            entry -> analyzerConfig.documents().cssSelectorAttribute().get(entry.getKey()),
                            this::joinCssSelector,
                            (s, s2) -> String.join(CSS_SELECTOR_DELIMITER, s, s2)
                    ));

            for (Map.Entry<String, String> entry : ressourceSelector.entrySet()) {
                webDriver.findElements(By.cssSelector(entry.getValue()))
                        .forEach(webElement -> extractDocumentInfo(webElement, extractedPageInfo, pageDto, entry.getKey()));
            }
        } catch (WebDriverException e) {
            webDriver.quit();
            final AnalyseStatut analyseStatut = getAnalyseStatutFromException(e);
            throw new MisisPageAnalyserException(pageDto, analyseStatut, e);
        } catch (RuntimeException e) {
            webDriver.quit();
            throw new MisisPageAnalyserException(pageDto, AnalyseStatut.INVALIDE, e);
        } catch (ExecutionException | InterruptedException | TimeoutException e) {
            webDriver.quit();
            Thread.currentThread().interrupt();
            throw new MisisPageAnalyserException(pageDto, AnalyseStatut.INVALIDE, e);
        } finally {
            Log.tracef("closing session for page id [%s]", pageDto.getId());
            webDriver.quit();
        }

        return Tuple2.of(pageDto, extractedPageInfo);
    }

    private AnalyseStatut getAnalyseStatutFromException(WebDriverException e) {
        AnalyseStatut analyseStatut = AnalyseStatut.INVALIDE;
        if (e.getCause() instanceof MalformedURLException) {
            analyseStatut = AnalyseStatut.ERREUR_URL_MALFORMEE;
        } else if (e.getCause() instanceof MisisWebDriverResponseStatus webDriverResponseStatuts) {
            analyseStatut = AnalyseStatut.getAnalyseStatut(webDriverResponseStatuts.getResponseStatus());
        } else if (e.getMessage().contains("ERR_NAME_NOT_RESOLVED")) {
            analyseStatut = AnalyseStatut.ERREUR_URL_NON_RESOLUE;
        }
        return analyseStatut;
    }

    private String joinCssSelector(final Map.Entry<RessourceType, List<String>> entry) {
        final String selectorAttribute = analyzerConfig.documents().cssSelectorAttribute().get(entry.getKey());
        return entry.getValue().stream()
                .map(extension -> String.format(ATTRIBUTE_CSS_SELECTOR, selectorAttribute, extension))
                .collect(Collectors.joining(CSS_SELECTOR_DELIMITER));
    }

    protected void extractDocumentInfo(final WebElement webElement, final ExtractedPageInfo extractedPageInfo, final PageDto pageDto, final String attribute) {
        final String link = webElement.getDomAttribute(attribute);
        try {
            final URL url = URI.create(link).toURL();
            requestAndPopulateInfo(extractedPageInfo, url);
        } catch (MalformedURLException | IllegalArgumentException e) {
            Log.warnf("invalid url found %s in page %s", link, pageDto.getUrl());
        }
    }

    protected void requestAndPopulateInfo(final ExtractedPageInfo extractedPageInfo, final URL url) {
        HttpURLConnection connection = null;
        try {
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("HEAD");
            connection.connect();

            final String mimeType = connection.getContentType();
            final long contentLength = connection.getContentLengthLong();

            extractedPageInfo.addExtractedRessourceInfo(url.getPath(), url.toString(), mimeType, LoadingType.DOWNLOADING, ResourceType.DOCUMENT);
            extractedPageInfo.addExtractedRessourceDataLength(url.getPath(), contentLength);
        } catch (IOException e) {
            Log.warnf("unable to open connection for %s", url.toString());
        } finally {
            if (connection instanceof HttpURLConnection httpURLConnection) {
                httpURLConnection.disconnect();
            }
        }
    }

    protected void setupDevTools(final DevTools devTools, final Long pageId) {
        printTrace(pageId);
        devTools.createSession();
        devTools.send(DOM.enable(Optional.of(DOM.EnableIncludeWhitespace.ALL)));
        devTools.send(Network.enable(Optional.empty(), Optional.empty(), Optional.of(Integer.MAX_VALUE)));
    }

    protected void addLandingListener(final DevTools devTools, final ExtractedPageInfo extractedPageInfo,
                                      final Long pageId) {
        printTrace(pageId);
        devTools.clearListeners();
        devTools.addListener(Network.dataReceived(),
                (aLong, dataReceived) -> processDataReceived(dataReceived, extractedPageInfo));
        devTools.addListener(Network.responseReceived(), (aLong,
                                                          responseReceived) -> processResponseReceived(responseReceived, extractedPageInfo, LoadingType.LANDING));
    }

    protected void openPage(final PageDto pageDto, final RemoteWebDriver webDriver) {
        printTrace(pageDto.getId());
        final String urlWithProtocol = StringUtils.startsWithIgnoreCase(pageDto.getUrl(), Constants.HTTP)
                ? pageDto.getUrl()
                : Constants.HTTPS + pageDto.getUrl();

        webDriver.get(urlWithProtocol);
    }

    protected void addScrollingListener(final DevTools devTools, final ExtractedPageInfo extractedPageInfo,
                                        final Long pageId) {
        printTrace(pageId);
        devTools.clearListeners();
        devTools.addListener(Network.dataReceived(),
                (aLong, dataReceived) -> processDataReceived(dataReceived, extractedPageInfo));
        devTools.addListener(Network.responseReceived(),
                (aLong, responseReceived) -> processResponseReceived(responseReceived, extractedPageInfo,
                        LoadingType.SCROLLING));
    }

    private void printTrace(final Long pageId) {
        if (Log.isTraceEnabled()) {
            final String methodName = Thread.currentThread().getStackTrace()[2].getMethodName();
            Log.tracef(analyzerConfig.trace().format(), methodName, pageId);
        }
    }

    protected void scrollToEndPage(final RemoteWebDriver webDriver, final Long pageId) {
        printTrace(pageId);

        final WebElement webElement = webDriver.findElement(By.tagName(HTML_TAG));
        final int scrollingHeight = analyzerConfig.scrolling().heightInPixel()
                .orElse(webDriver.manage().window().getSize().getHeight());

        final int scrollCount = Math.divideExact(webElement.getSize().getHeight(), scrollingHeight);

        for (int i = 0; i < scrollCount; i++) {
            new Actions(webDriver)
                    .scrollByAmount(0, scrollingHeight)
                    .pause(Duration.ofMillis(analyzerConfig.scrolling().pauseInMilliseconds()))
                    .perform();
        }
    }

    protected Integer requestDomElementsCount(final DevTools devTools) {
        return devTools.send(DOM.performSearch(ANY_WILDCARD, Optional.of(Boolean.TRUE))).getResultCount();
    }

    private static void processDataReceived(final DataReceived dataReceived,
                                            final ExtractedPageInfo extractedPageInfo) {
        final String requestId = dataReceived.getRequestId().toString();
        final Integer dataLength = dataReceived.getDataLength();
        final Integer encodedDataLength = dataReceived.getEncodedDataLength();

        extractedPageInfo.setDataDecodedInBytes(Math.addExact(dataLength, extractedPageInfo.getDataDecodedInBytes()));
        extractedPageInfo.setDataTransferedInBytes(
                Math.addExact(encodedDataLength, extractedPageInfo.getDataTransferedInBytes()));
        extractedPageInfo.addExtractedRessourceDataLength(requestId, Long.valueOf(dataLength));
    }

    private static void processResponseReceived(final ResponseReceived responseReceived,
                                                final ExtractedPageInfo extractedPageInfo, final LoadingType loadingType) {
        final String requestId = responseReceived.getRequestId().toString();
        final String url = responseReceived.getResponse().getUrl();
        final String typeMime = responseReceived.getResponse().getMimeType();
        final Integer httpRequestsCount = Math.addExact(1, extractedPageInfo.getHttpRequestsCount());
        final ResourceType resourceType = responseReceived.getType();

        extractedPageInfo.setHttpRequestsCount(httpRequestsCount);
        extractedPageInfo.addExtractedRessourceInfo(requestId, url, typeMime, loadingType, resourceType);
    }
}
