package fr.numeco.analyser.service.impl;

import fr.numeco.analyser.errors.MisisPageAnalyserException;
import fr.numeco.analyser.service.NetworkAnalyser;
import fr.numeco.misis.analyzer.dto.PageDto;
import fr.numeco.misis.enums.AnalyseStatut;
import io.quarkus.logging.Log;
import io.quarkus.runtime.util.StringUtil;
import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;
import jakarta.enterprise.context.ApplicationScoped;

import java.security.SecureRandom;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Random;
import java.util.stream.Stream;

/**
 * Permet de simuler l'analyse des données réseaux d'une URL
 */
@ApplicationScoped
public class FakeNetworkAnalyser implements NetworkAnalyser {

    private final Random random;

    public FakeNetworkAnalyser() {
        this.random = new SecureRandom();
    }

    @Override
    public Multi<PageDto> analyse(final Stream<PageDto> pages) {
        return this.updatePageAsync(pages);
    }

    private Multi<PageDto> updatePageAsync(final Stream<PageDto> pages) {
        return Multi.createFrom().items(pages)
                .onItem().transformToUniAndMerge(this::recreateWithGeneratedData);
    }

    private Uni<PageDto> recreateWithGeneratedData(final PageDto page) {
        if (StringUtil.isNullOrEmpty(page.getUrl())) {
            throw new MisisPageAnalyserException(page, AnalyseStatut.ERREUR_URL_VIDE);
        }
        final Integer duration = random.nextInt(100, 500);
        page.setDataDecodedInBytes(getRandomBytes());
        page.setDataTransferedInBytes(getRandomBytes());
        page.setDomElementsCount(getRandomDomElementsNumber());
        page.setHttpRequestsCount(getRandomHttpRequestsNumber());
        page.setAnalysedAt(LocalDateTime.now());
        page.setAnalyseStatut(AnalyseStatut.TERMINEE);
        return Uni.createFrom().item(page)
                .onItem().delayIt().by(Duration.ofMillis(duration))
                .onItem().invoke(pageDto -> Log.debugf("End of page processing [id=%d] with duration [millisecond=%d]", pageDto.getId(), duration));
    }

    public Long getRandomBytes() {
        return random.nextLong(2_000_000);
    }

    public Integer getRandomDomElementsNumber() {
        return random.nextInt(100);
    }

    public Integer getRandomHttpRequestsNumber() {
        return random.nextInt(10);
    }
}
