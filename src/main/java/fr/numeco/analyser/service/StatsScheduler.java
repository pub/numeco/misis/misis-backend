package fr.numeco.analyser.service;

import fr.numeco.misis.statistic.service.StatisticService;
import fr.numeco.misis.suividesite.domain.GroupeDePages;
import fr.numeco.misis.suividesite.repository.GroupeDePagesRepository;
import io.quarkus.logging.Log;
import io.quarkus.scheduler.Scheduled;
import io.quarkus.scheduler.ScheduledExecution;
import jakarta.enterprise.context.ApplicationScoped;
import lombok.RequiredArgsConstructor;

import java.util.List;

@ApplicationScoped
@RequiredArgsConstructor
public class StatsScheduler {
    private static final String SCHEDULER_START = "STATS SCHEDULER::START | Vérification des groupes...";
    private static final String SCHEDULER_END = "STATS SCHEDULER::END | Statistiques mises à jour. ";
    private static final String PROCESSING_LOG_FORMAT = "[%s] %s (%s) %s";
    private static final String STATS_UPDATED = "| Statistiques du groupe mises à jour !";

    private final GroupeDePagesRepository groupeDePagesRepository;
    private final StatisticService statisticService;

    @Scheduled(cron = "{stats.refresh.schedule.cron.expression}")
    void scheduleCreationOfPageGroupStatistics(ScheduledExecution execution) {
        Log.info(SCHEDULER_START);

        final List<GroupeDePages> groupesDePages = groupeDePagesRepository.findGroupsAnalysedForHistorization();

        for (GroupeDePages groupeDePages : groupesDePages) {
            statisticService.createStatistics(groupeDePages);

            Log.infof(
                    PROCESSING_LOG_FORMAT,
                    groupeDePages.getSuiviDeSite().getName(),
                    groupeDePages.getName(),
                    groupeDePages.getPeriodiciteDuSuivi(),
                    STATS_UPDATED
            );
        }

        Log.info(SCHEDULER_END);
    }

}
