package fr.numeco.analyser.service;

import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;

public interface WebDriverProvider {

    RemoteWebDriver createDriverInstance() throws MalformedURLException;

}
