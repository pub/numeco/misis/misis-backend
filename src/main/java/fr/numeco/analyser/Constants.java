package fr.numeco.analyser;

import fr.numeco.misis.enums.AnalyseStatut;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

public final class Constants {
    public static final String HTTPS = "https://";
    public static final String HTTP = "http";
    public static final Pattern FILENAME_PATTERN = Pattern.compile("^(?>https?://[\\p{L}\\p{N}\\p{P}\\p{S}.-]+/|data:)([\\p{L}\\p{N}\\p{P}\\p{S}_]+(?:\\.[\\p{L}\\p{N}]+)?)(?>[;?].*)?$");

    public static final List<AnalyseStatut> STATUSES_IN_ERROR = Arrays.asList(
            AnalyseStatut.INVALIDE,
            AnalyseStatut.ERREUR_URL_MALFORMEE,
            AnalyseStatut.ERREUR_URL_NON_RESOLUE,
            AnalyseStatut.ERREUR_URL_VIDE,
            AnalyseStatut.ERREUR_PAGE_302,
            AnalyseStatut.ERREUR_PAGE_404,
            AnalyseStatut.ERREUR_PAGE_500
    );
    private Constants() {}
}
