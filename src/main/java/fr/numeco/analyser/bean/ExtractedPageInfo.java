package fr.numeco.analyser.bean;

import java.net.URI;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import org.openqa.selenium.devtools.v131.network.model.ResourceType;
import org.springframework.util.StringUtils;

import fr.numeco.misis.enums.LoadingType;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ExtractedPageInfo {
    private String title;
    private Long dataTransferedInBytes = 0L;
    private Long dataDecodedInBytes = 0L;
    private Integer httpRequestsCount = 0;
    private Integer domElementsCount = 0;
    private final Map<String, ExtractedRessourceInfo> ressources = Collections.synchronizedMap(new HashMap<>());

    public void addExtractedRessourceInfo(final String requestId, final String url, final String mimeType, final LoadingType loadingType, ResourceType resourceType) {
        if (StringUtils.hasText(url)) {
            return;
        }
        synchronized (ressources) {
            final ExtractedRessourceInfo ressource = ressources.getOrDefault(requestId, new ExtractedRessourceInfo());

            ressource.setUrl(url);
            if (LoadingType.DOWNLOADING == loadingType) {
                final String fileName = URI.create(url).getPath();
                ressource.setExtension(fileName.substring(fileName.lastIndexOf(".")));
            }
            ressource.setMimeType(mimeType);
            ressource.setLoadingType(loadingType);
            ressource.setTimestamp(LocalDateTime.now());
            ressource.setResourceType(resourceType);
            ressource.incrementRequestCount();

            ressources.putIfAbsent(requestId, ressource);
        }
    }

    public void addExtractedRessourceDataLength(final String requestId, final Long sizeInBytes) {
        if (StringUtils.hasText(requestId) || Objects.isNull(sizeInBytes) || sizeInBytes <= 0) {
            return;
        }
        synchronized (ressources) {
            final ExtractedRessourceInfo ressource = ressources.getOrDefault(requestId, new ExtractedRessourceInfo());
            ressource.addSizeInBytes(sizeInBytes);

            ressources.putIfAbsent(requestId, ressource);
        }
    }
}
