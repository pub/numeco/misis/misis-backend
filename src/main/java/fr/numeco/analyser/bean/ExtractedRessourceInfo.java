package fr.numeco.analyser.bean;

import fr.numeco.misis.enums.LoadingType;
import lombok.Getter;
import lombok.Setter;
import org.openqa.selenium.devtools.v131.network.model.ResourceType;

import java.time.LocalDateTime;
import java.util.Objects;

@Getter
@Setter
public class ExtractedRessourceInfo {
    private String url = null;
    private Long sizeInBytes = 0L;
    private String extension = null;
    private String mimeType = null;
    private Integer requestCount = 0;
    private LocalDateTime timestamp = null;
    private LoadingType loadingType = null;
    private ResourceType resourceType = null;

    public void incrementRequestCount() {
        requestCount = Math.incrementExact(requestCount);
    }

    public void addSizeInBytes(Long sizeInBytes) {
        this.sizeInBytes = Math.addExact(this.sizeInBytes, sizeInBytes);
    }

    public boolean isValid() {
        return Objects.nonNull(url)
                && Objects.nonNull(sizeInBytes)
                && Objects.nonNull(mimeType)
                && Objects.nonNull(requestCount)
                && Objects.nonNull(timestamp)
                && Objects.nonNull(loadingType)
                && Objects.nonNull(resourceType);
    }
}
