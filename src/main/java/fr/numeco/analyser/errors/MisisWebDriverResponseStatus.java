package fr.numeco.analyser.errors;

public class MisisWebDriverResponseStatus extends Exception {
    private final long responseStatus;
    public MisisWebDriverResponseStatus(long responseStatus) {
        super("Response status " + responseStatus + " is invalid");
        this.responseStatus = responseStatus;
    }

    public long getResponseStatus() {
        return responseStatus;
    }
}
