package fr.numeco.analyser.errors;

import fr.numeco.misis.analyzer.dto.PageDto;
import fr.numeco.misis.enums.AnalyseStatut;
import lombok.Getter;

@Getter
public class MisisPageAnalyserException extends RuntimeException {

    private final PageDto pageDto;
    private final AnalyseStatut analyseStatut;

    public MisisPageAnalyserException(final PageDto pageDto, final AnalyseStatut analyseStatut) {
        super(String.format("unable to analyse page id %s", pageDto.getId()));
        this.pageDto = pageDto;
        this.analyseStatut = analyseStatut;
    }

    public MisisPageAnalyserException(final PageDto pageDto, final AnalyseStatut analyseStatut, final Throwable throwable) {
        super(String.format("unable to analyse page id %s", pageDto.getId()), throwable);
        this.pageDto = pageDto;
        this.analyseStatut = analyseStatut;
    }

    public MisisPageAnalyserException(Throwable cause) {
        super(cause);
        this.pageDto = null;
        this.analyseStatut = null;
    }

    public MisisPageAnalyserException(String message) {
        super(message);
        this.pageDto = null;
        this.analyseStatut = null;
    }

    public MisisPageAnalyserException(String message, Throwable cause) {
        super(message, cause);
        this.pageDto = null;
        this.analyseStatut = null;
    }

    public MisisPageAnalyserException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.pageDto = null;
        this.analyseStatut = null;
    }
}
