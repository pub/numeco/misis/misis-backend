package fr.numeco.analyser.errors;

public class AnalyserEmptyListException extends RuntimeException {

        public AnalyserEmptyListException() {
            super("The list of pages is empty");
        }
}
