package fr.numeco.analyser.errors;

public class MisisRemoteMalformedURLException extends RuntimeException{
    public MisisRemoteMalformedURLException(Throwable cause) {
        super(cause);
    }
}
