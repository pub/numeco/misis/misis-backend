package fr.numeco.analyser;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import fr.numeco.misis.enums.RessourceType;
import io.quarkus.runtime.annotations.ConfigGroup;
import io.smallrye.config.ConfigMapping;
import io.smallrye.config.WithDefault;

@ConfigMapping(prefix = "analyzer")
public interface AnalyzerConfig {
    Optional<String> remoteUrl();
    @WithDefault("70")
    Integer paginationSize();
    Documents documents();
    Scrolling scrolling();
    Debug debug();
    Trace trace();
    @WithDefault("false")
    boolean enableTracing();
    @WithDefault("5")
    Integer concurrency();

    @ConfigGroup
    interface Documents {
        Map<RessourceType, List<String>> extensions();
        Map<RessourceType, String> cssSelectorAttribute();
    }

    @ConfigGroup
    interface Debug {
        @WithDefault("[%1$s] for page id [%2$s] [%3$s]")
        String format();
    }

    @ConfigGroup
    interface Trace {
        @WithDefault("[%1$s] for page id [%2$s]")
        String format();
    }

    @ConfigGroup
    interface Scrolling {
        Optional<Integer> heightInPixel();
        @WithDefault("400")
        Integer pauseInMilliseconds();
    }
}
