package fr.numeco.misis.statistic.resource;

import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;

import fr.numeco.misis.enums.StatisticType;
import fr.numeco.misis.security.Constants;
import fr.numeco.misis.statistic.dto.StatisticsDto;
import fr.numeco.misis.statistic.service.StatisticService;
import io.quarkus.security.UnauthorizedException;
import jakarta.annotation.security.RolesAllowed;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import lombok.RequiredArgsConstructor;

@Path("suivis-de-site/{suiviId}/groupe/{groupId}/statistiques")
@RequiredArgsConstructor
@RolesAllowed(Constants.SERVICE_PUBLIC_ROLE)
public class StatisticsResource {

    private final StatisticService statisticService;

    @GET
    @Path("/{statType}")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Get a website's statistics", description = "Returns the list of datasets.")
    @APIResponse(responseCode = "200", description = "Page list stats", content = @Content(mediaType = "application/json", schema = @Schema(implementation = StatisticsDto.class)))
    @APIResponse(responseCode = "404", description = "Site or Group not found")
    public StatisticsDto getStatistics(
            @Parameter(description = "ID of the site to retrieve", required = true) @PathParam("suiviId") Long suiviId,
            @Parameter(description = "ID of the group to view", required = true) @PathParam("groupId") Long groupId,
            @Parameter(description = "Type of the stat requested", required = true) @PathParam("statType") StatisticType statType) throws UnauthorizedException {

        return statisticService.getGroupStatistics(suiviId, groupId, statType);
    }

}
