package fr.numeco.misis.statistic.domain;

import fr.numeco.misis.enums.StatisticType;
import fr.numeco.misis.suividesite.domain.GroupeDePages;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(indexes = {
        @Index(name = "statistic_type_index", columnList = "statistic_type"),
})
public class Statistique {

    private static final String ENTITY_SEQ = "statistique_SEQ";

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = ENTITY_SEQ)
    @SequenceGenerator(name = ENTITY_SEQ, sequenceName = ENTITY_SEQ, allocationSize = 1)
    @Column(nullable = false)
    private Long id;

    @Column(nullable = false)
    private LocalDate timestamp = LocalDate.now();

    @ManyToOne(fetch=FetchType.LAZY)
    private GroupeDePages groupeDePages;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private StatisticType statisticType;

    @ElementCollection
    @CollectionTable(name = "valeurs_statistiques", joinColumns = @JoinColumn(name = "id"))
    @MapKeyColumn(name = "key")
    @Column(name = "value")
    private Map<String, Long> valeurs;

    public static Statistique create(GroupeDePages groupeDePages, StatisticType statisticType) {
        final Statistique statistique = new Statistique();
        statistique.setGroupeDePages(groupeDePages);
        statistique.setStatisticType(statisticType);

        return statistique;
    }

    public void addValues(String key, Long value) {
        if (Objects.isNull(this.valeurs)) {
            this.valeurs = new HashMap<>();
        }
        final Number newValue = this.valeurs.computeIfPresent(key, (existingKey, existingValue) -> Math.addExact(existingValue, value));
        if (Objects.isNull(newValue)) {
            this.valeurs.put(key, value);
        }
    }

}
