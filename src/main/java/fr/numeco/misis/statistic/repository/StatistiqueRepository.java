package fr.numeco.misis.statistic.repository;

import fr.numeco.misis.enums.StatisticType;
import fr.numeco.misis.statistic.domain.Statistique;
import io.quarkus.hibernate.orm.panache.PanacheQuery;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import jakarta.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class StatistiqueRepository implements PanacheRepository<Statistique> {

    private static final String QUERY_STATISTIQUE_BY_GROUPE_DE_PAGE_ID = """
            SELECT s FROM Statistique AS s
            JOIN s.groupeDePages AS g
            WHERE g.suiviDeSite.id = ?1 AND g.id = ?2 AND s.statisticType = ?3
            ORDER BY s.timestamp
            """;

    public PanacheQuery<Statistique> findByGroupDePageId(final Long suiviId, final Long groupId, StatisticType statType) {
        return find(QUERY_STATISTIQUE_BY_GROUPE_DE_PAGE_ID, suiviId, groupId, statType);
    }
}
