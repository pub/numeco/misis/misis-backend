package fr.numeco.misis.statistic.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
public class StatisticsDto {
    private List<LocalDate> labels; //NOSONAR
    private Set<DatasetsDto> datasets; //NOSONAR
}
