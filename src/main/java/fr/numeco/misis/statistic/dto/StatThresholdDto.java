package fr.numeco.misis.statistic.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StatThresholdDto {
    Long id; //NOSONAR
    private int threshold; //NOSONAR
    private int nbImagesAboveThreshold; //NOSONAR
}
