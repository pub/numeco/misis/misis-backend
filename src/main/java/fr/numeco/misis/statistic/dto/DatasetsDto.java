package fr.numeco.misis.statistic.dto;

import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Getter
@Setter
@RequiredArgsConstructor
public class DatasetsDto implements Comparable<DatasetsDto> {
    @NonNull
    private String label; //NOSONAR
    private List<Number> data = new ArrayList<>(); //NOSONAR

    public void addData(Number value) {
        data.add(value);
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (!(object instanceof DatasetsDto datasetsDto)) return false;
        return Objects.equals(label, datasetsDto.label);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(label);
    }

    @Override
    public int compareTo(@NotNull final DatasetsDto other) {
        return other == null
                ? 1
                : this.label.compareTo(other.getLabel());
    }
}
