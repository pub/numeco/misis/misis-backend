package fr.numeco.misis.statistic.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StatAverageDto {
    Long id; //NOSONAR
    private long minimum; //NOSONAR
    private long maximum; //NOSONAR
    private long average; //NOSONAR
    private long median; //NOSONAR
}
