package fr.numeco.misis.statistic.service.impl;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import fr.numeco.misis.enums.MisisPermission;
import fr.numeco.misis.enums.StatisticType;
import fr.numeco.misis.error.EntityNotFound;
import fr.numeco.misis.permission.service.impl.SuiviDeSitePermissionService;
import fr.numeco.misis.security.service.AuthenticatedUserService;
import fr.numeco.misis.statistic.domain.Statistique;
import fr.numeco.misis.statistic.dto.DatasetsDto;
import fr.numeco.misis.statistic.dto.StatisticsDto;
import fr.numeco.misis.statistic.repository.StatistiqueRepository;
import fr.numeco.misis.statistic.service.StatisticService;
import fr.numeco.misis.suividesite.domain.GroupeDePages;
import fr.numeco.misis.suividesite.domain.Page;
import fr.numeco.misis.suividesite.domain.Ressource;
import fr.numeco.misis.suividesite.domain.SuiviDeSite;
import fr.numeco.misis.suividesite.repository.SuiviDeSiteRepository;
import fr.numeco.misis.user.domain.MisisUser;
import io.quarkus.security.UnauthorizedException;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;

@ApplicationScoped
@RequiredArgsConstructor
@Transactional
public class DefaultStatisticService implements StatisticService {

    public static final long IMAGE_THRESHOLD_IN_BYTES = 100000;
    private static final String IMAGES_ABOVE_THRESHOLD_DATASETS_LABEL = "Nombre d'images au-dessus du seuil";
    private static final String THRESHOLD_DATASETS_LABEL = "Seuil";
    private static final String MAXIMUM_DATASETS_LABEL = "Maximum";
    private static final String MINIMUM_DATASETS_LABEL = "Minimum";
    private static final String AVERAGE_DATASETS_LABEL = "Moyenne";
    private static final String MEDIAN_DATASETS_LABEL = "Médiane";

    private final StatistiqueRepository statistiqueRepository;
    private final AuthenticatedUserService authenticatedUserService;
    private final SuiviDeSitePermissionService suiviDeSitePermissionService;
    private final SuiviDeSiteRepository suiviDeSiteRepository;

    @Override
    public void createStatistics(final GroupeDePages groupeDePages) {
        final List<Long> donneesTransfereesValues = new ArrayList<>();
        final List<Integer> nbElementsDOM = new ArrayList<>();
        final List<Integer> nbRequetesHttp = new ArrayList<>();
        final List<Ressource> imagesAboveThreshold = new ArrayList<>();
        final List<Long> pdfSizes = new ArrayList<>();
        final List<Long> otherFilesSizes = new ArrayList<>();
        final List<Long> streamingSizes = new ArrayList<>();

        for (Page page : groupeDePages.getPages()) {
            if (Objects.nonNull(page.getDataTransferedInBytes())) {
                donneesTransfereesValues.add(page.getDataTransferedInBytes());
            }
            if (Objects.nonNull(page.getDomElementsCount())) {
                nbElementsDOM.add(page.getDomElementsCount());
            }
            if (Objects.nonNull(page.getHttpRequestsCount())) {
                nbRequetesHttp.add(page.getHttpRequestsCount());
            }

            page.getRessources().stream()
                    .filter(ressource -> Objects.nonNull(ressource.getType()))
                    .filter(ressource -> Objects.nonNull(ressource.getSizeInBytes()))
                    .forEach(ressource -> {
                        switch (ressource.getType()) {
                            case IMAGE:
                                if (ressource.getSizeInBytes() > IMAGE_THRESHOLD_IN_BYTES) {
                                    imagesAboveThreshold.add(ressource);
                                }
                                break;
                            case PDF:
                                pdfSizes.add(ressource.getSizeInBytes());
                                break;
                            case OTHERS:
                                otherFilesSizes.add(ressource.getSizeInBytes());
                                break;
                            case STREAMING:
                                streamingSizes.add(ressource.getSizeInBytes());
                                break;
                            case UNDEFINED:
                            default:
                                break;
                        }
                    });
        }

        if (!donneesTransfereesValues.isEmpty()) {
            donneesTransfereesValues.sort(Long::compareTo);
        }
        if (!nbElementsDOM.isEmpty()) {
            nbElementsDOM.sort(Integer::compareTo);
        }
        if (!nbRequetesHttp.isEmpty()) {
            nbRequetesHttp.sort(Integer::compareTo);
        }
        if (!pdfSizes.isEmpty()) {
            pdfSizes.sort(Long::compareTo);
        }
        if (!otherFilesSizes.isEmpty()) {
            otherFilesSizes.sort(Long::compareTo);
        }

        Arrays.stream(StatisticType.values()).
                map(statisticType -> Statistique.create(groupeDePages, statisticType))
                .map(statistique -> switch (statistique.getStatisticType()) {
            case StatisticType.TRANSFERRED_DATA ->
                calculateStatsAverage(statistique, donneesTransfereesValues);
            case REQUESTS ->
                calculateStatsAverage(statistique, nbRequetesHttp);
            case DOM_ELEMENTS ->
                calculateStatsAverage(statistique, nbElementsDOM);
            case IMAGE ->
                calculateImageAboveThreshold(statistique, imagesAboveThreshold);
            case PDF ->
                calculateStatsAverage(statistique, pdfSizes);
            case OTHERS ->
                calculateStatsAverage(statistique, otherFilesSizes);
            case STREAMING ->
                calculateStatsAverage(statistique, streamingSizes);
        }
                )
                .filter(Objects::nonNull)
                .forEach(statistiqueRepository::persist);
    }

    private <N extends Number> Statistique calculateStatsAverage(final Statistique statistique, final List<N> data) {
        if (Objects.isNull(data) || data.isEmpty() || Objects.isNull(data.getFirst())) {
            return null;
        }
        statistique.addValues(MINIMUM_DATASETS_LABEL, data.getFirst().longValue());
        statistique.addValues(MAXIMUM_DATASETS_LABEL, data.getLast().longValue());

        data.stream()
                .mapToLong(Number::longValue)
                .average()
                .ifPresentOrElse(
                        average -> statistique.addValues(AVERAGE_DATASETS_LABEL, Math.round(average)),
                        () -> statistique.addValues(AVERAGE_DATASETS_LABEL, 0L)
                );

        final long median = data.size() > 2
                ? data.get(data.size() / 2 + 1).longValue()
                : data.getFirst().longValue();
        statistique.addValues(MEDIAN_DATASETS_LABEL, median);

        return statistique;
    }

    private Statistique calculateImageAboveThreshold(final Statistique statistique, final List<Ressource> ressources) {
        if (Objects.isNull(ressources)) {
            return null;
        }

        statistique.addValues(THRESHOLD_DATASETS_LABEL, IMAGE_THRESHOLD_IN_BYTES);
        statistique.addValues(IMAGES_ABOVE_THRESHOLD_DATASETS_LABEL, (long) ressources.size());

        return statistique;
    }

    @Override
    public StatisticsDto getGroupStatistics(final Long suiviId, final Long groupId, final StatisticType statType) throws UnauthorizedException {

        final MisisUser currentConnectedUser = authenticatedUserService.getAndUpdateCurrentAuthenticatedUser();
        final SuiviDeSite suiviDeSite = suiviDeSiteRepository.findById(suiviId);
        boolean statAccessIsAllowed = suiviDeSitePermissionService.isAllowed(currentConnectedUser, suiviDeSite, MisisPermission.READ);

        if (!statAccessIsAllowed) {
            throw new EntityNotFound(suiviId);
        }

        final List<LocalDate> labels = new ArrayList<>();
        final Map<String, DatasetsDto> datasets = new HashMap<>();

        statistiqueRepository.findByGroupDePageId(suiviId, groupId, statType).stream()
                .forEach(statistique -> populateDatasets(statistique, labels, datasets));

        return new StatisticsDto(labels, new HashSet<>(datasets.values()));
    }

    private void populateDatasets(final Statistique statistique, final List<LocalDate> labels, final Map<String, DatasetsDto> datasets) {
        labels.add(statistique.getTimestamp());
        statistique.getValeurs().entrySet().stream()
                .filter(entry -> !AVERAGE_DATASETS_LABEL.equalsIgnoreCase(entry.getKey()))
                .forEach(entry -> {
                    final DatasetsDto dataset = datasets.getOrDefault(entry.getKey(), new DatasetsDto(entry.getKey()));
                    dataset.addData(entry.getValue());
                    datasets.putIfAbsent(entry.getKey(), dataset);
                });

    }
}
