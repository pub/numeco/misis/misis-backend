package fr.numeco.misis.statistic.service;

import fr.numeco.misis.enums.StatisticType;
import fr.numeco.misis.statistic.dto.StatisticsDto;
import fr.numeco.misis.suividesite.domain.GroupeDePages;
import io.quarkus.security.UnauthorizedException;

public interface StatisticService {
    void createStatistics(GroupeDePages groupeDePages);

    StatisticsDto getGroupStatistics(Long suiviId, Long groupId, StatisticType statType) throws UnauthorizedException;
}
