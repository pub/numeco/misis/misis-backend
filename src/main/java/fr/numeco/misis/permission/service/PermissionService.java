package fr.numeco.misis.permission.service;

import fr.numeco.misis.enums.MisisPermission;
import fr.numeco.misis.permission.domain.Auditable;
import fr.numeco.misis.permission.domain.Permission;
import fr.numeco.misis.permission.domain.PermissionType;
import fr.numeco.misis.permission.repository.PermissionRepository;
import fr.numeco.misis.permission.repository.impl.PermissionTypeRepository;
import fr.numeco.misis.user.domain.MisisUser;
import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Objects;

@NoArgsConstructor
@AllArgsConstructor
public abstract class PermissionService<D extends Auditable, P extends Permission> {
    protected PermissionRepository<P> permissionRepository;
    protected PermissionTypeRepository permissionTypeRepository;

    @Transactional
    public void addOrCreatePermission(final MisisUser misisUser, final D domain, final List<MisisPermission> misisPermission) {
        permissionRepository.findByUserAndDomain(misisUser, domain)
                .ifPresentOrElse(
                        permission -> addPermission(permission, misisPermission),
                        () -> createPermission(misisUser, domain, misisPermission)
                );
    }

    private void createPermission(final MisisUser misisUser, final D domain, final List<MisisPermission> misisPermissions) {
        final List<PermissionType> permissionType = getPermissionType(misisPermissions);
        final P permission = createNewPermissionInstance(domain);
        permission.setUser(misisUser);
        permission.setAllowed(permissionType);

        permissionRepository.persist(permission);
    }

    private void addPermission(final P permission, final List<MisisPermission> misisPermissions) {
        final List<PermissionType> permissionTypes = getPermissionType(misisPermissions);

        if (Objects.isNull(permission.getAllowed())) {
            permission.setAllowed(permissionTypes);
        } else {
            permissionTypes.forEach(permissionType -> {
                if (!permission.getAllowed().contains(permissionType)) {
                    permission.getAllowed().add(permissionType);
                }
            });
        }
        permissionRepository.persist(permission);
    }

    private List<PermissionType> getPermissionType(final List<MisisPermission> misisPermission) {
        final List<PermissionType> permissionTypes = permissionTypeRepository.listPermissionType(misisPermission);
        initPermissionTypeIfMissing(misisPermission, permissionTypes);
        return permissionTypes;
    }

    private void initPermissionTypeIfMissing(final List<MisisPermission> misisPermissions, final List<PermissionType> permissionTypes) {
        if (permissionTypes.size() != misisPermissions.size()) {
            final List<MisisPermission> availablePermissionType = permissionTypes.stream()
                    .map(PermissionType::getMisisPermission)
                    .toList();

            misisPermissions.stream()
                    .filter(misisPermission -> !availablePermissionType.contains(misisPermission))
                    .forEach(misisPermission -> createPermissionTypeInstance(misisPermission, permissionTypes));
        }
    }

    private void createPermissionTypeInstance(final MisisPermission misisPermission, final List<PermissionType> permissionTypes) {
        final PermissionType permissionType = new PermissionType();
        permissionType.setMisisPermission(misisPermission);
        permissionType.setName(misisPermission.getDefaultName());

        permissionTypes.add(permissionType);
        permissionTypeRepository.persist(permissionType);
    }

    protected abstract P createNewPermissionInstance(D domain);

    public boolean isAllowed(MisisUser misisUser, D domain, MisisPermission misisPermission) {
        return permissionRepository.findPermission(misisUser, domain, misisPermission).isPresent();
    }
}
