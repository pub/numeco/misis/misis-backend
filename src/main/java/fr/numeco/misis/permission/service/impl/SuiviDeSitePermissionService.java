package fr.numeco.misis.permission.service.impl;

import fr.numeco.misis.permission.domain.PermissionSurLeSuiviDeSite;
import fr.numeco.misis.permission.repository.impl.PermissionTypeRepository;
import fr.numeco.misis.permission.repository.impl.SuiviDeSitePermissionRepository;
import fr.numeco.misis.permission.service.PermissionService;
import fr.numeco.misis.suividesite.domain.SuiviDeSite;
import fr.numeco.misis.user.domain.MisisUser;
import jakarta.enterprise.context.ApplicationScoped;

import java.util.List;

@ApplicationScoped
public class SuiviDeSitePermissionService extends PermissionService<SuiviDeSite, PermissionSurLeSuiviDeSite> {

    public SuiviDeSitePermissionService(SuiviDeSitePermissionRepository permissionRepository, PermissionTypeRepository permissionTypeRepository) {
        super(permissionRepository, permissionTypeRepository);
    }

    @Override
    protected PermissionSurLeSuiviDeSite createNewPermissionInstance(SuiviDeSite domain) {
        final PermissionSurLeSuiviDeSite permission = new PermissionSurLeSuiviDeSite();

        permission.setDomain(domain);

        return permission;
    }

    public void clearPermission(SuiviDeSite domain) {
        permissionRepository.delete("domain", domain);
    }

    public void removeIfNotPresent(final SuiviDeSite suiviDeSite, final List<MisisUser> shareToUsers) {
        permissionRepository.delete("domain=?1 AND user NOT IN (?2)  AND user != ?3", suiviDeSite, shareToUsers, suiviDeSite.getCreatedBy());
    }
}
