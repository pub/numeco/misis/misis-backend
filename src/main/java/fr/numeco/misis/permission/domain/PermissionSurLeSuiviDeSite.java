package fr.numeco.misis.permission.domain;

import fr.numeco.misis.suividesite.domain.SuiviDeSite;
import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToOne;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@DiscriminatorValue("1")
public class PermissionSurLeSuiviDeSite extends Permission {

    @ManyToOne
    public SuiviDeSite domain;
}
