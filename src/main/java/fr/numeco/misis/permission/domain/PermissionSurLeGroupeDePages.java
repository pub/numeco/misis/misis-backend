package fr.numeco.misis.permission.domain;

import fr.numeco.misis.suividesite.domain.GroupeDePages;
import jakarta.persistence.CascadeType;
import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToOne;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@DiscriminatorValue("2")
public class PermissionSurLeGroupeDePages extends Permission {

    @ManyToOne(cascade = CascadeType.MERGE)
    public GroupeDePages domain;
}
