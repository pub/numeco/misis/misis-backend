package fr.numeco.misis.permission.domain;

import fr.numeco.misis.enums.MisisPermission;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.Objects;

@Getter
@Setter
@Entity
@Table(indexes = {
        @Index(name = "misis_permission_index", columnList = "misis_permission"),
})
public class PermissionType {
    private static final String ENTITY_SEQ = "permission_type_SEQ";

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = ENTITY_SEQ)
    @SequenceGenerator(name = ENTITY_SEQ, sequenceName = ENTITY_SEQ, allocationSize = 1)
    @Column(nullable = false)
    private Long id;

    @Enumerated(EnumType.STRING)
    private MisisPermission misisPermission;
    private String name;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PermissionType that)) return false;
        return misisPermission == that.misisPermission;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(misisPermission);
    }
}
