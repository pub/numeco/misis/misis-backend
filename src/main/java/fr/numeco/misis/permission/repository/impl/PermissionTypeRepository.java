package fr.numeco.misis.permission.repository.impl;

import fr.numeco.misis.enums.MisisPermission;
import fr.numeco.misis.permission.domain.PermissionType;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import jakarta.enterprise.context.ApplicationScoped;

import java.util.List;

@ApplicationScoped
public class PermissionTypeRepository implements PanacheRepository<PermissionType> {

    public List<PermissionType> listPermissionType(List<MisisPermission> misisPermission) {
        return list("misisPermission IN (?1)", misisPermission);
    }
}
