package fr.numeco.misis.permission.repository;

import fr.numeco.misis.enums.MisisPermission;
import fr.numeco.misis.permission.domain.Auditable;
import fr.numeco.misis.permission.domain.Permission;
import fr.numeco.misis.user.domain.MisisUser;
import io.quarkus.hibernate.orm.panache.PanacheRepository;

import java.util.Optional;

public interface PermissionRepository<P extends Permission> extends PanacheRepository<P> {
    String QUERY_BY_USER_AND_DOMAIN = """
            SELECT p FROM %s AS p
            WHERE p.user = ?1 AND p.domain = ?2
            """;

    String QUERY_BY_USER_AND_DOMAIN_AND_PERMISSION = """
            SELECT p FROM %s AS p
            JOIN p.allowed AS t
            WHERE p.user = ?1 AND p.domain = ?2 AND t.misisPermission = ?3
            """;

    default Optional<P> findByUserAndDomain(MisisUser misisUser, Auditable domain) {
        return find(String.format(QUERY_BY_USER_AND_DOMAIN, getPermissionName()), misisUser, domain)
                .singleResultOptional();
    }

    default Optional<P> findPermission(MisisUser misisUser, Auditable domain, MisisPermission misisPermission) {
        return find(String.format(QUERY_BY_USER_AND_DOMAIN_AND_PERMISSION, getPermissionName()), misisUser, domain, misisPermission)
                .singleResultOptional();
    }

    String getPermissionName();
}
