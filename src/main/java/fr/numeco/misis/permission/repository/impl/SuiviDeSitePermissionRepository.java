package fr.numeco.misis.permission.repository.impl;

import fr.numeco.misis.permission.domain.PermissionSurLeSuiviDeSite;
import fr.numeco.misis.permission.repository.PermissionRepository;
import jakarta.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class SuiviDeSitePermissionRepository implements PermissionRepository<PermissionSurLeSuiviDeSite> {
    @Override
    public String getPermissionName() {
        return PermissionSurLeSuiviDeSite.class.getSimpleName();
    }
}
