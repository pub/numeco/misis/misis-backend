package fr.numeco.misis.analyzer.service.impl;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import static java.util.function.UnaryOperator.identity;
import java.util.stream.Collectors;

import fr.numeco.analyser.service.impl.DynamicNetworkAnalyser;
import fr.numeco.misis.analyzer.dto.PageDto;
import fr.numeco.misis.analyzer.dto.RessourceDto;
import fr.numeco.misis.analyzer.mapper.PageMapper;
import fr.numeco.misis.analyzer.mapper.RessourceMapper;
import fr.numeco.misis.analyzer.service.AnalyserService;
import fr.numeco.misis.enums.AnalyseStatut;
import fr.numeco.misis.error.EntityNotFound;
import fr.numeco.misis.notification.service.NotificationService;
import fr.numeco.misis.suividesite.domain.Page;
import fr.numeco.misis.suividesite.domain.Ressource;
import fr.numeco.misis.suividesite.domain.SuiviDeSite;
import fr.numeco.misis.suividesite.repository.PageRepository;
import fr.numeco.misis.suividesite.repository.RessourceRepository;
import fr.numeco.misis.suividesite.repository.SuiviDeSiteRepository;
import io.quarkus.hibernate.orm.panache.PanacheQuery;
import io.quarkus.logging.Log;
import io.quarkus.scheduler.ScheduledExecution;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;

@ApplicationScoped
@RequiredArgsConstructor
public class DefaultAnalyserService implements AnalyserService {

    private final DynamicNetworkAnalyser networkAnalyser;
    private final PageRepository pageRepository;
    private final RessourceRepository ressourceRepository;
    private final PageMapper pageMapper;
    private final RessourceMapper ressourceMapper;
    private final NotificationService notificationService;
    private final SuiviDeSiteRepository suiviDeSiteRepository;
    private volatile boolean isRunning = false;

    @Override
    public void process(final io.quarkus.panache.common.Page pagination) {
        final PanacheQuery<Page> panacheQuery = pageRepository.findAvailableByPeriodicity().page(pagination);
        do {
            Log.infof("Processing with pagination whose size per page is %s [%s/%s]", panacheQuery.page().size, panacheQuery.page().index + 1, panacheQuery.pageCount());
            processInternal(panacheQuery);
        } while (panacheQuery.hasNextPage() && Objects.nonNull(panacheQuery.nextPage()));
    }

    @Override
    public void process(final int startIndex, final int lastIndex) {
        final PanacheQuery<Page> panacheQuery = pageRepository.findAvailableByPeriodicity()
                .range(startIndex, lastIndex);
        processInternal(panacheQuery);
    }

    @Transactional
    protected void processInternal(final PanacheQuery<Page> panacheQuery) {
        final Set<Long> pagesIds = new HashSet<>();
        final List<PageDto> pageDtos = panacheQuery.stream()
                .map(page -> {
                    pagesIds.add(page.getId());
                    return pageMapper.toDto(page);
                }).toList();
        if (pageDtos.isEmpty()) {
            return;
        } else {
            Log.infof("Processing %s page(s)", pageDtos.size());
        }
        pageRepository.update("analyseStatut=?1 WHERE id IN (?2)", AnalyseStatut.EN_COURS, pagesIds);
        isRunning = Boolean.TRUE;
        networkAnalyser.analyse(pageDtos.stream())
                .onTermination().invoke(() -> isRunning = Boolean.FALSE)
                .subscribe().with(
                        this::handlePageSuccess,
                        this::handleFailure,
                        () -> handleComplete(pageDtos)
                );

    }

    @Transactional
    protected void handleComplete(final List<PageDto> pageDtos) {
        Log.infof("Fin de l'analyse de %s pages", pageDtos.size());
        final List<Long> ids = pageDtos.stream().map(PageDto::getId).toList();
        final List<SuiviDeSite> suiviDeSites = suiviDeSiteRepository.findLatestAnalysis(ids);
        suiviDeSites.forEach(notificationService::createNotificationAnalysisCompleted);
    }

    private void handleFailure(final Throwable throwable) {
        Log.error("error during analyse", throwable);
    }

    @Transactional
    @Override
    public void handlePageSuccess(final PageDto analysedPage) {
        final Page existingPage = pageRepository.findByIdOptional(analysedPage.getId())
                .orElseThrow(
                        () -> new EntityNotFound(analysedPage.getId())
                );
        existingPage.updateAfterAnalysis(analysedPage);
        if (!networkAnalyser.isFake() && AnalyseStatut.TERMINEE.equals(analysedPage.getAnalyseStatut())) {
            updateRessource(analysedPage, existingPage);
        }
        Log.debugf("Page with id %s updated", existingPage.getId());
    }

    @Override
    public void updateRessource(final PageDto analysedPage, final Page existingPage) {
        final Map<String, Ressource> existingRessources = ressourceRepository.list("page=?1", existingPage).stream()
                .collect(Collectors.toMap(Ressource::getUrl, identity()));
        final Map<String, RessourceDto> analysedRessource = analysedPage.getRessources().stream()
                .collect(Collectors.toMap(RessourceDto::getFilename, identity(), this::mergeRessources));

        updateExistingResources(existingRessources, analysedRessource);
        createNewResources(existingPage, analysedRessource, existingRessources);
        removeNonExistingResources(existingPage, analysedRessource);
    }

    private RessourceDto mergeRessources(final RessourceDto ressourceDto, final RessourceDto mergedResource) {
        ressourceDto.setSizeInBytes(Math.addExact(ressourceDto.getSizeInBytes(), mergedResource.getSizeInBytes()));
        return ressourceDto;
    }

    @Override
    public void removeNonExistingResources(final Page existingPage, final Map<String, RessourceDto> analysedRessource) {
        ressourceRepository.delete("page = :page AND url NOT IN (:url)", Map.of(
                "page", existingPage, "url", analysedRessource.keySet()));
    }

    @Override
    public void createNewResources(final Page existingPage, final Map<String, RessourceDto> analysedRessource, final Map<String, Ressource> existingRessources) {
        final List<Ressource> ressources = analysedRessource.entrySet().stream()
                .filter(entry -> !existingRessources.containsKey(entry.getKey()))
                .map(entry -> {
                    Ressource ressource = ressourceMapper.toEntity(entry.getValue());
                    ressource.setPage(existingPage);
                    return ressource;
                }).toList();
        ressourceRepository.persist(ressources);
    }

    protected static void updateExistingResources(final Map<String, Ressource> existingRessources, final Map<String, RessourceDto> analysedRessource) {
        existingRessources.entrySet().stream()
                .filter(entry -> analysedRessource.containsKey(entry.getKey()))
                .forEach(entry -> entry.getValue().updateAfterAnalysis(analysedRessource.get(entry.getKey())));
    }

    @Override
    public boolean test(ScheduledExecution execution) {
        return isRunning;
    }

}
