package fr.numeco.misis.analyzer.service;

import fr.numeco.misis.analyzer.dto.PageDto;
import fr.numeco.misis.analyzer.dto.RessourceDto;
import fr.numeco.misis.suividesite.domain.Page;
import fr.numeco.misis.suividesite.domain.Ressource;
import io.quarkus.scheduler.Scheduled.SkipPredicate;

import java.util.Map;

public interface AnalyserService extends SkipPredicate {
    void process(io.quarkus.panache.common.Page pagination);

    void process(int startIndex, int lastIndex);

    void handlePageSuccess(PageDto analysedPage);

    void updateRessource(PageDto analysedPage, Page existingPage);

    void removeNonExistingResources(Page existingPage, Map<String, RessourceDto> analysedRessource);

    void createNewResources(Page existingPage, Map<String, RessourceDto> analysedRessource, Map<String, Ressource> existingRessources);

}
