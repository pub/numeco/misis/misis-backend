package fr.numeco.misis.analyzer.dto;

import fr.numeco.misis.enums.AnalyseStatut;
import fr.numeco.misis.suividesite.domain.Page;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.PositiveOrZero;
import lombok.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * DTO for {@link Page}
 */
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class PageDto implements Serializable {
    Long id; //NOSONAR
    @NotBlank
    private final String url; //NOSONAR
    private final String title; //NOSONAR
    @PositiveOrZero
    private Long dataTransferedInBytes; //NOSONAR
    @PositiveOrZero
    private Long dataDecodedInBytes; //NOSONAR
    @PositiveOrZero
    private Integer httpRequestsCount; //NOSONAR
    @PositiveOrZero
    private Integer domElementsCount; //NOSONAR
    private final List<RessourceDto> ressources = new ArrayList<>(); //NOSONAR
    private LocalDateTime analysedAt; //NOSONAR
    private AnalyseStatut analyseStatut; //NOSONAR

}
