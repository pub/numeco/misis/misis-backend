package fr.numeco.misis.analyzer.dto;

import fr.numeco.misis.enums.LoadingType;
import fr.numeco.misis.enums.RessourceType;
import fr.numeco.misis.suividesite.domain.Ressource;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.regex.Matcher;

import static fr.numeco.analyser.Constants.FILENAME_PATTERN;

/**
 * DTO for {@link Ressource}
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RessourceDto implements Serializable {
    private static final String UNKNOWN = "unknown";

    private Long id; //NOSONAR
    private String url; //NOSONAR
    private Long sizeInBytes; //NOSONAR
    private RessourceType type; //NOSONAR
    private LoadingType loadingType; //NOSONAR

    public String getFilename() {
        final Matcher matcher = FILENAME_PATTERN.matcher(url);

        return matcher.matches()
                ? matcher.group(1)
                : UNKNOWN;
    }
}
