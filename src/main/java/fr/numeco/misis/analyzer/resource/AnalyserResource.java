package fr.numeco.misis.analyzer.resource;

import fr.numeco.misis.analyzer.service.AnalyserService;
import io.quarkus.panache.common.Page;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import lombok.RequiredArgsConstructor;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;

@Path("api/analyser")
@RequiredArgsConstructor
public class AnalyserResource {

    private final AnalyserService analyserService;

    @GET
    @Path("groupe-de-pages")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Trigger analysis by ID group", description = "Trigger the analysis of a group of pages based on the provided ID.")
    @APIResponse(responseCode = "200")
    @APIResponse(responseCode = "404", description = "Analysis not triggered")
    public Response triggerAnalysisByGroupeDePagesId(@QueryParam("size") Integer size) {
        analyserService.process(Page.ofSize(size));
        return Response.ok().build();
    }
}
