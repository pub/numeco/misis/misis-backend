package fr.numeco.misis.analyzer.mapper;

import fr.numeco.misis.analyzer.dto.RessourceDto;
import fr.numeco.misis.suividesite.domain.Ressource;
import org.mapstruct.*;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = MappingConstants.ComponentModel.JAKARTA)
public interface RessourceMapper {
    Ressource toEntity(RessourceDto ressourceDto);

    RessourceDto toDto(Ressource ressource);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    Ressource partialUpdate(RessourceDto ressourceDto, @MappingTarget Ressource ressource);
}
