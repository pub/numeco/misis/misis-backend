package fr.numeco.misis.analyzer.mapper;

import fr.numeco.misis.analyzer.dto.PageDto;
import fr.numeco.misis.suividesite.domain.Page;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

import java.util.List;

@Mapper(componentModel = MappingConstants.ComponentModel.JAKARTA)
public interface PageMapper {

    PageDto toDto(Page page);
    Page toEntity(PageDto pageDto);

    List<PageDto> toDtos(List<Page> pages);
    List<Page> toEntities(List<PageDto> pageDtos);
}
