package fr.numeco.misis.suividesite.mapper;

import fr.numeco.misis.enums.Alert;
import fr.numeco.misis.enums.RessourceType;
import fr.numeco.misis.suividesite.domain.GroupeDePages;
import fr.numeco.misis.suividesite.domain.Page;
import fr.numeco.misis.suividesite.domain.Ressource;
import fr.numeco.misis.suividesite.domain.SuiviDeSite;
import fr.numeco.misis.suividesite.dto.GroupDetailsDto;
import fr.numeco.misis.suividesite.dto.WebsiteSummaryDto;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

import static fr.numeco.analyser.Constants.STATUSES_IN_ERROR;

@Mapper(componentModel = MappingConstants.ComponentModel.JAKARTA)
public interface WebsiteSummaryMapper {
    default WebsiteSummaryDto toDto(final SuiviDeSite suiviDeSite, final LocalDateTime nextAnalysis) {
        final Long id = suiviDeSite.getId();
        final String name = suiviDeSite.getName();
        final String url = suiviDeSite.getUrl();
        List<Long> suiviInError = new ArrayList<>();
        final LocalDateTime lastAnalysis = suiviDeSite.getGroupesDePages().stream()
                .flatMap(groupeDePages -> groupeDePages.getPages().stream())
                .map(Page::getAnalysedAt)
                .filter(Objects::nonNull)
                .max(LocalDateTime::compareTo)
                .orElse(null);

        final Map<Long, GroupDetailsDto> groups = new TreeMap<>();
        long pageCount=0L;
        for (GroupeDePages groupeDePages : suiviDeSite.getGroupesDePages()) {
            List<Long> groupInError = new ArrayList<>();
            final Set<RessourceType> availableRessourceTypes = groupeDePages.getPages().stream()
                    .peek(page -> {
                        if (STATUSES_IN_ERROR.contains(page.getAnalyseStatut())) {
                            suiviInError.add(page.getId());
                            groupInError.add(page.getId());
                        }
                    })
                    .flatMap(page -> page.getRessources().stream())
                    .map(Ressource::getType)
                    .filter(Objects::nonNull)
                    .collect(Collectors.toCollection(TreeSet::new));

            Alert alert = null;
            if (!groupInError.isEmpty() && groupInError.size() < groupeDePages.getPages().size()) {
                alert = Alert.WARNING;
            } else if (!groupInError.isEmpty() && groupInError.size() == groupeDePages.getPages().size()) {
                alert = Alert.ERROR;
            }

            pageCount += groupeDePages.getPages().size();
            groups.put(groupeDePages.getId(), new GroupDetailsDto(groupeDePages.getName(), availableRessourceTypes, alert));
        }

        Alert alert = null;
        if (!suiviInError.isEmpty() && suiviInError.size() < pageCount) {
            alert = Alert.WARNING;
        } else if (!suiviInError.isEmpty() && suiviInError.size() == pageCount) {
            alert = Alert.ERROR;
        }

        return new WebsiteSummaryDto(id, name, url, lastAnalysis, nextAnalysis, groups, pageCount, alert);
    }
}
