package fr.numeco.misis.suividesite.mapper;


import fr.numeco.misis.enums.RessourceType;
import fr.numeco.misis.suividesite.domain.Page;
import fr.numeco.misis.suividesite.domain.Ressource;
import fr.numeco.misis.suividesite.domain.SuiviDeSite;
import fr.numeco.misis.suividesite.dto.*;
import fr.numeco.misis.suividesite.service.WebsiteService;
import io.smallrye.mutiny.tuples.Tuple2;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static fr.numeco.misis.statistic.service.impl.DefaultStatisticService.IMAGE_THRESHOLD_IN_BYTES;

@Mapper(componentModel = MappingConstants.ComponentModel.JAKARTA)
public interface WebsiteDetailsMapper {

    default WebsitePagesDetailsDto toPagesDetails(final SuiviDeSite suiviDeSite, final Long groupId) {
        final List<PageDetailsDto> pageDetailDtos = new ArrayList<>();
        Tuple2<Long, Map<Long, GroupDetailsDto>> groupDetails = WebsiteService.getGroupDetails(suiviDeSite, groupeDePages -> {
            if (groupeDePages.getId().equals(groupId)) {
                for (Page page : groupeDePages.getPages()) {
                    pageDetailDtos.add(new PageDetailsDto(page.getUrl(), page.getTitre(), page.getDataTransferedInBytes(), page.getDataDecodedInBytes(), page.getHttpRequestsCount(), page.getDomElementsCount(), page.getAnalyseStatut()));
                }
            }
            return null;
        });

        return new WebsitePagesDetailsDto(suiviDeSite.getName(), groupDetails.getItem1(), groupDetails.getItem2(), pageDetailDtos);
    }

    default WebsiteResourcesDetailsDto toRessourcesDetails(final SuiviDeSite suiviDeSite, final Long groupId, final RessourceType type) {
        final List<ResourceDetailsDto> resourceDetailDtos = new ArrayList<>();
        Tuple2<Long, Map<Long, GroupDetailsDto>> groupDetails = WebsiteService.getGroupDetails(suiviDeSite, groupeDePages -> {
            if (groupeDePages.getId().equals(groupId)) {
                groupeDePages.getPages().stream()
                        .flatMap(page -> page.getRessources().stream())
                        .filter(ressource -> isRequiredToDisplay(ressource, type))
                        .forEach(ressource -> resourceDetailDtos.add(new ResourceDetailsDto(ressource.getUrl(), ressource.getPage().getUrl(), ressource.getSizeInBytes())));
            }
            return null;
        });

        return new WebsiteResourcesDetailsDto(suiviDeSite.getName(), groupDetails.getItem1(), groupDetails.getItem2(), resourceDetailDtos);
    }

    private boolean isRequiredToDisplay(Ressource ressource, RessourceType requiredType) {
        return Objects.nonNull(ressource.getType()) && ressource.getType().equals(requiredType)
                && (!RessourceType.IMAGE.equals(ressource.getType()) || ressource.getSizeInBytes() >= IMAGE_THRESHOLD_IN_BYTES);
    }
}
