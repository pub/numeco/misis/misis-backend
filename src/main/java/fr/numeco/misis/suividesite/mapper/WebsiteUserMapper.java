package fr.numeco.misis.suividesite.mapper;

import fr.numeco.misis.suividesite.domain.SuiviDeSite;
import fr.numeco.misis.suividesite.dto.WebsiteUserDto;
import fr.numeco.misis.user.domain.MisisUser;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

import java.util.Objects;

@Mapper(componentModel = MappingConstants.ComponentModel.JAKARTA)
public interface WebsiteUserMapper {
    default WebsiteUserDto toDto(final MisisUser misisUser, final SuiviDeSite suiviDeSite) {
        if ( misisUser == null ) {
            return null;
        }

        WebsiteUserDto websiteUserDto = new WebsiteUserDto();

        websiteUserDto.setId( misisUser.getId() );
        websiteUserDto.setEmail( misisUser.getEmail() );
        websiteUserDto.setName( misisUser.getUsername() );
        websiteUserDto.setAdmin(Objects.equals(suiviDeSite.getCreatedBy(), misisUser));

        return websiteUserDto;
    }
}
