package fr.numeco.misis.suividesite.dto.abst;

import fr.numeco.misis.suividesite.dto.GroupDetailsDto;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.util.Map;

@Getter
@Setter
@RequiredArgsConstructor
public abstract class WebsiteDetails {
    @NonNull
    private String websiteName; //NOSONAR
    @NonNull
    private Long pageCount; //NOSONAR
    @NonNull
    private Map<Long, GroupDetailsDto> groups; //NOSONAR
}
