package fr.numeco.misis.suividesite.dto;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.numeco.misis.enums.MethodeDeCreationDeGroupe;
import fr.numeco.misis.enums.PeriodiciteDuSuivi;
import io.quarkus.logging.Log;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class GroupOfPagesFormDto {

    private Long id; //NOSONAR
    @NotBlank
    private String name; //NOSONAR
    @NotNull
    private PeriodiciteDuSuivi periodiciteDuSuivi; //NOSONAR
    @NotNull
    private MethodeDeCreationDeGroupe methodeDeCreationDeGroupe; //NOSONAR
    @Valid
    @Size
    private List<PageFormDto> pages; //NOSONAR

    public GroupOfPagesFormDto(Long id, final String name, final PeriodiciteDuSuivi periodiciteDuSuivi, final MethodeDeCreationDeGroupe methodeDeCreationDeGroupe, final Object pages) {
        this(id, name, periodiciteDuSuivi, methodeDeCreationDeGroupe, map(pages));
    }

    private static List<PageFormDto> map(Object object) {
        try {
            List<HashMap<Long, String>> list =  new ObjectMapper().readValue(String.valueOf(object), new TypeReference<>() {});
            return list.stream()
                    .flatMap(hashMap -> hashMap.entrySet().stream())
                    .map(mapEntry -> new PageFormDto(mapEntry.getKey(), mapEntry.getValue()))
                    .sorted(Comparator.comparing(PageFormDto::getId))
                    .toList();
        } catch (JsonProcessingException e) {
            Log.error(e.getMessage());
            return Collections.emptyList();
        }
    }
}
