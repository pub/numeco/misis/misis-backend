package fr.numeco.misis.suividesite.dto;

import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SettingsFormDto {

    @NotNull
    private List<Long> suivisId; //NOSONAR
    @NotNull
    private List<String> usersEmail; //NOSONAR
}
