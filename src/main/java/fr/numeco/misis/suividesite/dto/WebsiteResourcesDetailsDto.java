package fr.numeco.misis.suividesite.dto;

import fr.numeco.misis.suividesite.dto.abst.WebsiteDetails;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Map;

@Getter
@Setter
public class WebsiteResourcesDetailsDto extends WebsiteDetails {
    private List<ResourceDetailsDto> resourceDetailDtos; //NOSONAR

    public WebsiteResourcesDetailsDto(String websiteName, Long pageCount, Map<Long, GroupDetailsDto> groups, List<ResourceDetailsDto> resourceDetailDtos) {
        super(websiteName, pageCount, groups);
        this.resourceDetailDtos = resourceDetailDtos;
    }
}
