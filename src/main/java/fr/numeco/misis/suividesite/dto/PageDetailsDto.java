package fr.numeco.misis.suividesite.dto;

import fr.numeco.misis.enums.AnalyseStatut;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class PageDetailsDto {
    private String url; //NOSONAR
    private String title; //NOSONAR
    private Long dataTransferedInBytes; //NOSONAR
    private Long dataDecodedInBytes; //NOSONAR
    private Integer httpRequestsCount; //NOSONAR
    private Integer domElementsCount; //NOSONAR
    private AnalyseStatut analyseStatut;
}
