package fr.numeco.misis.suividesite.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class WebsiteUserDto {
    private Long id; //NOSONAR
    private String name; //NOSONAR
    private String email; //NOSONAR
    private boolean admin; //NOSONAR
}
