package fr.numeco.misis.suividesite.dto;

import fr.numeco.misis.enums.Alert;
import fr.numeco.misis.enums.RessourceType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class GroupDetailsDto {
    private String name; //NOSONAR
    private Set<RessourceType> availableRessourceTypes; //NOSONAR
    private Alert alert;
}