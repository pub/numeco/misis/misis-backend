package fr.numeco.misis.suividesite.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class PageFormDto {
    private Long id; //NOSONAR
    private String url; //NOSONAR
}
