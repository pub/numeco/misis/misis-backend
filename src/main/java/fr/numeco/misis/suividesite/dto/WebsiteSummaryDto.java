package fr.numeco.misis.suividesite.dto;

import java.time.LocalDateTime;
import java.util.Map;

import fr.numeco.misis.enums.Alert;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class WebsiteSummaryDto {
    private Long id; //NOSONAR
    private String name; //NOSONAR
    private String url; //NOSONAR
    private LocalDateTime lastAnalysis; //NOSONAR
    private LocalDateTime nextAnalysis; //NOSONAR
    private Map<Long, GroupDetailsDto> groups; //NOSONAR
    private Long pageCount; //NOSONAR
    private Alert alert; //NOSONAR
}
