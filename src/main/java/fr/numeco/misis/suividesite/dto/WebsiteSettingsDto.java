package fr.numeco.misis.suividesite.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@AllArgsConstructor
@Getter
public class WebsiteSettingsDto {
    private final List<WebsiteUserDto> users; //NOSONAR
    private final List<GroupOfPagesFormDto> groupOfPagesFormDtos; //NOSONAR
}
