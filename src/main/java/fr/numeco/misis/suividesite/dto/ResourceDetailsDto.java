package fr.numeco.misis.suividesite.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.regex.Matcher;

import static fr.numeco.analyser.Constants.FILENAME_PATTERN;

@AllArgsConstructor
@Getter
@Setter
public class ResourceDetailsDto {
    private static final String UNKNOWN = "unknown";

    private String url; //NOSONAR
    private String pageUrl; //NOSONAR
    private Long sizeInBytes; //NOSONAR

    public String getFilename() {
        final Matcher matcher = FILENAME_PATTERN.matcher(url);

        return matcher.matches()
                ? matcher.group(1)
                : UNKNOWN;
    }

}
