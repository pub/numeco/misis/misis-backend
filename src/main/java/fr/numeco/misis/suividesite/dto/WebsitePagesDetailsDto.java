package fr.numeco.misis.suividesite.dto;

import fr.numeco.misis.suividesite.dto.abst.WebsiteDetails;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Map;

@Getter
@Setter
public class WebsitePagesDetailsDto extends WebsiteDetails {
    private List<PageDetailsDto> pageDetailDtos; //NOSONAR

    public WebsitePagesDetailsDto(String websiteName, Long pageCount, Map<Long, GroupDetailsDto> groups, List<PageDetailsDto> pageDetailDtos) {
        super(websiteName, pageCount, groups);
        this.pageDetailDtos = pageDetailDtos;
    }
}
