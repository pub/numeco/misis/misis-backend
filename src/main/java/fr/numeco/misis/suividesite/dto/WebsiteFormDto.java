package fr.numeco.misis.suividesite.dto;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@AllArgsConstructor
@Getter
@Setter
public class WebsiteFormDto {
    @NotBlank
    private String name; //NOSONAR
    @NotBlank
    private String url; //NOSONAR
    @Valid
    @Size
    private List<GroupOfPagesFormDto> groupOfPages; //NOSONAR
}
