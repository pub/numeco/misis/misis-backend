package fr.numeco.misis.suividesite.domain;

import java.net.URI;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

import fr.numeco.misis.analyzer.dto.PageDto;
import fr.numeco.misis.enums.AnalyseStatut;
import fr.numeco.misis.suividesite.dto.PageFormDto;
import static io.quarkus.hibernate.orm.panache.Panache.getEntityManager;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Index;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(
        uniqueConstraints = {
                @UniqueConstraint(name = "unique_url_groupe_de_pages_id", columnNames = {"groupe_de_pages_id", "url"})
        },
        indexes = {
                @Index(name = "analyse_statut_index", columnList = "analyse_statut"),
                @Index(name = "analysed_at_index", columnList = "analysed_at"),
        }
)
public class Page {

    private static final String ENTITY_SEQ = "page_SEQ";

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = ENTITY_SEQ)
    @SequenceGenerator(name = ENTITY_SEQ, sequenceName = ENTITY_SEQ, allocationSize = 1)
    @Column(nullable = false)
    private Long id;

    @Column(length = Integer.MAX_VALUE)
    private String titre;

    @Column(nullable = false, length = Integer.MAX_VALUE)
    private String url;

    private Long dataTransferedInBytes;

    private Long dataDecodedInBytes;

    private Integer httpRequestsCount;

    private Integer domElementsCount;

    @Column(nullable = false)
    private LocalDateTime modifiedAt;

    private LocalDateTime analysedAt;

    @Enumerated(EnumType.STRING)
    private AnalyseStatut analyseStatut;

    @ManyToOne
    private GroupeDePages groupeDePages;

    @OneToMany(mappedBy = "page", orphanRemoval = true)
    private List<Ressource> ressources;

    public static List<Page> createOrUpdate(final String domain, final List<PageFormDto> pageFormDtos, final GroupeDePages groupeDePages) {
        return pageFormDtos.stream()
                .map(pageUrl -> Page.createOrUpdate(domain, pageUrl, groupeDePages))
                .toList();
    }

    public static Page createOrUpdate(final String domain, final PageFormDto pageFormDto, final GroupeDePages groupeDePages) {
        final Page page = findOrCreatePage(pageFormDto.getId(), groupeDePages);
        String urlWithDomain = pageFormDto.getUrl().startsWith(domain)
                ? page.getUrl()
                : URI.create(domain.trim()).resolve(pageFormDto.getUrl().trim()).toString();

        page.setUrl(Objects.isNull(urlWithDomain) ? "/" : urlWithDomain.trim());
        if (Objects.isNull(page.getGroupeDePages())) {
            page.setGroupeDePages(groupeDePages);
        }
        page.setAnalyseStatut(AnalyseStatut.NOUVEAU);
        if (Objects.nonNull(page.getTitre())) {
            page.setTitre(null);
        }
        page.setModifiedAt(LocalDateTime.now());
        return page;
    }

    private static Page findOrCreatePage(final Long id, GroupeDePages groupeDePages) {
        Page page;
        if (Objects.nonNull(id)) {
            page = getEntityManager(Page.class).find(Page.class, id);
            if (Objects.isNull(page)) {
                page = new Page();
            }
            groupeDePages.addPage(page);
        } else {
            page = new Page();
            groupeDePages.addPage(page);
        }

        return page;
    }

    public void updateAfterAnalysis(PageDto page) {
        this.url = page.getUrl();
        this.titre = page.getTitle();
        this.dataTransferedInBytes = page.getDataTransferedInBytes();
        this.dataDecodedInBytes = page.getDataDecodedInBytes();
        this.httpRequestsCount = page.getHttpRequestsCount();
        this.domElementsCount = page.getDomElementsCount();
        this.analysedAt = page.getAnalysedAt();
        this.analyseStatut = page.getAnalyseStatut();
    }
}
