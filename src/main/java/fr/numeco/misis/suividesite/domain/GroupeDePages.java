package fr.numeco.misis.suividesite.domain;

import fr.numeco.misis.enums.MethodeDeCreationDeGroupe;
import fr.numeco.misis.enums.PeriodiciteDuSuivi;
import fr.numeco.misis.permission.domain.Auditable;
import fr.numeco.misis.suividesite.dto.GroupOfPagesFormDto;
import fr.numeco.misis.suividesite.dto.PageFormDto;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import static io.quarkus.hibernate.orm.panache.Panache.getEntityManager;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class GroupeDePages extends Auditable {
    private static final String DEFAULT_GROUPE_DE_PAGE_NAME = "Groupe de page n°1";

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private PeriodiciteDuSuivi periodiciteDuSuivi;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private MethodeDeCreationDeGroupe methodeDeCreationDeGroupe;

    @OneToMany(mappedBy = "groupeDePages", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Page> pages;

    @ManyToOne(cascade = CascadeType.MERGE)
    private SuiviDeSite suiviDeSite;

    public void addPage(Page page) {
        if (Objects.isNull(pages)) {
            pages = new ArrayList<>();
        }
        page.setGroupeDePages(this);
        this.pages.add(page);
    }

    public static List<GroupeDePages> createOrUpdate(final List<GroupOfPagesFormDto> groupOfPages, final SuiviDeSite suiviDeSite) {
        return groupOfPages.stream()
                .map(groupOfPagesForm -> createOrUpdate(groupOfPagesForm, suiviDeSite))
                .toList();
    }

    public static List<GroupeDePages> createDefaultGroup(final PageFormDto pageFormDto, final SuiviDeSite suiviDeSite) {
        final GroupeDePages groupeDePages = new GroupeDePages();

        groupeDePages.setName(DEFAULT_GROUPE_DE_PAGE_NAME);
        groupeDePages.setPeriodiciteDuSuivi(PeriodiciteDuSuivi.HEBDOMADAIRE);
        groupeDePages.setMethodeDeCreationDeGroupe(MethodeDeCreationDeGroupe.MANUELLE);
        manageOrphan(Collections.singletonList(Page.createOrUpdate(suiviDeSite.getUrl(), pageFormDto, groupeDePages)), groupeDePages);
        groupeDePages.setCreatedBy(suiviDeSite.getCreatedBy());
        groupeDePages.setModifiedBy(suiviDeSite.getModifiedBy());
        groupeDePages.setCreatedDate(LocalDate.now());
        groupeDePages.setSuiviDeSite(suiviDeSite);

        return Collections.singletonList(groupeDePages);
    }

    private static void manageOrphan(List<Page> createdPage, GroupeDePages groupeDePages) {
        if (Objects.isNull(groupeDePages.getPages())) {
            groupeDePages.setPages(createdPage);
            return;
        }

        groupeDePages.getPages().stream()
                .filter(page -> !createdPage.contains(page))
                .toList()
                .forEach(groupeDePages.getPages()::remove);
    }

    public static GroupeDePages createOrUpdate(final GroupOfPagesFormDto groupOfPagesFormDto, final SuiviDeSite suiviDeSite) {
        final GroupeDePages groupeDePages = findOrCreate(groupOfPagesFormDto.getId(), suiviDeSite);


        groupeDePages.setName(groupOfPagesFormDto.getName());
        groupeDePages.setPeriodiciteDuSuivi(groupOfPagesFormDto.getPeriodiciteDuSuivi());
        groupeDePages.setMethodeDeCreationDeGroupe(groupOfPagesFormDto.getMethodeDeCreationDeGroupe());
        manageOrphan(Page.createOrUpdate(suiviDeSite.getUrl(), groupOfPagesFormDto.getPages(), groupeDePages), groupeDePages);
        if (Objects.isNull(groupeDePages.getCreatedBy())) {
            groupeDePages.setCreatedBy(suiviDeSite.getCreatedBy());
        }
        groupeDePages.setModifiedBy(suiviDeSite.getModifiedBy());
        if (Objects.isNull(groupeDePages.getCreatedDate())) {
            groupeDePages.setCreatedDate(LocalDate.now());
        }
        groupeDePages.setModifiedDate(LocalDate.now());
        groupeDePages.setSuiviDeSite(suiviDeSite);

        return groupeDePages;
    }

    private static GroupeDePages findOrCreate(final Long id, SuiviDeSite suiviDeSite) {
        GroupeDePages groupeDePages;
        if (Objects.nonNull(id)) {
            groupeDePages = getEntityManager(GroupeDePages.class).find(GroupeDePages.class, id);
            if (Objects.isNull(groupeDePages)) {
                groupeDePages = new GroupeDePages();
            }
            suiviDeSite.addGroupeDePages(groupeDePages);
        } else {
            groupeDePages = new GroupeDePages();
            suiviDeSite.addGroupeDePages(groupeDePages);
        }

        return groupeDePages;
    }
}
