package fr.numeco.misis.suividesite.domain;

import fr.numeco.analyser.Constants;
import fr.numeco.misis.permission.domain.Auditable;
import fr.numeco.misis.suividesite.dto.PageFormDto;
import fr.numeco.misis.suividesite.dto.WebsiteFormDto;
import fr.numeco.misis.user.domain.MisisUser;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.OneToMany;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.util.StringUtils;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SuiviDeSite extends Auditable {

    @Column(nullable = false)
    private String name;

    @Column(nullable = false, length = Integer.MAX_VALUE)
    private String url;

    @OneToMany(mappedBy = "suiviDeSite", cascade = CascadeType.ALL, orphanRemoval = true)
    List<GroupeDePages> groupesDePages;

    public void addGroupeDePages(GroupeDePages groupeDePages) {
        if (Objects.isNull(this.groupesDePages)) {
            this.groupesDePages = new ArrayList<>();
        }
        groupeDePages.setSuiviDeSite(this);
        this.groupesDePages.add(groupeDePages);
    }

    public static SuiviDeSite create(final WebsiteFormDto form, final MisisUser currentConnectedUser) {
        final SuiviDeSite suiviDeSite = new SuiviDeSite();
        final String urlWithProtocol = StringUtils.startsWithIgnoreCase(form.getUrl(), Constants.HTTP)
                ? form.getUrl()
                : Constants.HTTPS + form.getUrl();
        suiviDeSite.setName(Objects.isNull(form.getName()) ? null: form.getName().trim());
        suiviDeSite.setUrl(urlWithProtocol.trim());
        suiviDeSite.setCreatedBy(currentConnectedUser);
        suiviDeSite.setModifiedBy(currentConnectedUser);
        suiviDeSite.setCreatedDate(LocalDate.now());
        suiviDeSite.setModifiedDate(LocalDate.now());

        final List<GroupeDePages> groupesDePages = Objects.isNull(form.getGroupOfPages()) || form.getGroupOfPages().isEmpty()
                ? GroupeDePages.createDefaultGroup(new PageFormDto(null, urlWithProtocol), suiviDeSite)
                : GroupeDePages.createOrUpdate(form.getGroupOfPages(), suiviDeSite);
        suiviDeSite.setGroupesDePages(groupesDePages);

        return suiviDeSite;
    }
}
