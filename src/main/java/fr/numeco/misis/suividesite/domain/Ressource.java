package fr.numeco.misis.suividesite.domain;

import fr.numeco.misis.analyzer.dto.RessourceDto;
import fr.numeco.misis.enums.LoadingType;
import fr.numeco.misis.enums.RessourceType;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(indexes = {
        @Index(name = "type_index", columnList = "type"),
        @Index(name = "loading_type_index", columnList = "loading_type"),
})
public class Ressource {

    private static final String ENTITY_SEQ = "ressource_SEQ";

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = ENTITY_SEQ)
    @SequenceGenerator(name = ENTITY_SEQ, sequenceName = ENTITY_SEQ, allocationSize = 1)
    @Column(nullable = false)
    private Long id;

    @Column(nullable = false, length = Integer.MAX_VALUE)
    private String url;

    private Long sizeInBytes;

    @Enumerated(EnumType.STRING)
    private RessourceType type;

    @ManyToOne
    private Page page;

    @Enumerated(EnumType.STRING)
    private LoadingType loadingType;

    public void updateAfterAnalysis(RessourceDto ressourceDto) {
        this.url = ressourceDto.getUrl();
        this.sizeInBytes = ressourceDto.getSizeInBytes();
        this.type = ressourceDto.getType();
        this.loadingType = ressourceDto.getLoadingType();
    }
}
