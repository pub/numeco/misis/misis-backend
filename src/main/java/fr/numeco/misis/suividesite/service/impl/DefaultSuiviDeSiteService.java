package fr.numeco.misis.suividesite.service.impl;

import fr.numeco.misis.enums.MisisPermission;
import fr.numeco.misis.error.EntityNotFound;
import fr.numeco.misis.notification.service.NotificationService;
import fr.numeco.misis.permission.service.impl.SuiviDeSitePermissionService;
import fr.numeco.misis.security.service.AuthenticatedUserService;
import fr.numeco.misis.suividesite.domain.GroupeDePages;
import fr.numeco.misis.suividesite.domain.SuiviDeSite;
import fr.numeco.misis.suividesite.dto.GroupOfPagesFormDto;
import fr.numeco.misis.suividesite.dto.WebsiteUserDto;
import fr.numeco.misis.suividesite.repository.GroupeDePagesRepository;
import fr.numeco.misis.suividesite.repository.SuiviDeSiteRepository;
import fr.numeco.misis.suividesite.service.SuiviDeSiteService;
import fr.numeco.misis.user.domain.MisisUser;
import fr.numeco.misis.user.repository.MisisUserRepository;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;

import java.util.List;
import java.util.Objects;

@ApplicationScoped
@RequiredArgsConstructor
@Transactional
public class DefaultSuiviDeSiteService implements SuiviDeSiteService {
    private final SuiviDeSiteRepository suiviDeSiteRepository;
    private final MisisUserRepository misisUserRepository;
    private final SuiviDeSitePermissionService suiviDeSitePermissionService;
    private final AuthenticatedUserService authenticatedUserService;
    private final GroupeDePagesRepository groupeDePagesRepository;
    private final NotificationService notificationService;

    @Override
    public List<WebsiteUserDto> getUsersOfTrackingInReading(final Long suiviId) {
        final MisisUser currentAuthenticatedUser = authenticatedUserService.getAndUpdateCurrentAuthenticatedUser();
        final SuiviDeSite suiviDeSite = suiviDeSiteRepository.findById(suiviId);

        boolean canRead = suiviDeSitePermissionService.isAllowed(currentAuthenticatedUser, suiviDeSite,
                MisisPermission.READ);

        if (!canRead) {
            throw new EntityNotFound(suiviId);
        }

        return misisUserRepository.findUsersOfTrackingInReading(suiviDeSite);
    }

    @Override
    public void updateSharedUsersOfTracking(final List<Long> suivisId, final List<String> usersEmail) {
        final MisisUser currentAuthenticatedUser = authenticatedUserService.getAndUpdateCurrentAuthenticatedUser();
        final List<MisisUser> shareToUsers = misisUserRepository.findAllByEmailAndCurrentUser(usersEmail,
                currentAuthenticatedUser);

        for (final Long id : suivisId) {
            final SuiviDeSite suiviDeSite = suiviDeSiteRepository.findByIdOptional(id)
                    .orElseThrow(() -> new EntityNotFound(id));

            final boolean canUpdateSuiviDeSite = suiviDeSitePermissionService.isAllowed(currentAuthenticatedUser,
                    suiviDeSite, MisisPermission.WRITE);
            if (canUpdateSuiviDeSite) {
                updateSettings(suiviDeSite, shareToUsers);
                notificationService.createNotificationListOfUsersWithAccessHasBeenModified(suiviDeSite);
            } else {
                throw new EntityNotFound(id);
            }
        }
    }

    private void updateSettings(final SuiviDeSite suiviDeSite, final List<MisisUser> shareToUsers) {
        shareToUsers.forEach(misisUser -> {
            suiviDeSitePermissionService.addOrCreatePermission(misisUser, suiviDeSite, MisisPermission.getUserPermissions());
            if (Objects.nonNull(suiviDeSite.getCreatedBy()) && !Objects.equals(suiviDeSite.getCreatedBy().getId(), misisUser.getId()))  {
                notificationService.createNotificationAccessModified(suiviDeSite, misisUser);
            }
        });
        suiviDeSitePermissionService.removeIfNotPresent(suiviDeSite, shareToUsers);
    }

    @Override
    public void editGroupOfPages(final Long suiviId, final List<GroupOfPagesFormDto> groupOfPagesFormDto) {
        final MisisUser currentAuthenticatedUser = authenticatedUserService.getAndUpdateCurrentAuthenticatedUser();
        final SuiviDeSite suiviDeSite = suiviDeSiteRepository.findById(suiviId);

        boolean canUpdate = suiviDeSitePermissionService.isAllowed(currentAuthenticatedUser, suiviDeSite,
                MisisPermission.WRITE);
        if (!canUpdate) {
            throw new EntityNotFound(suiviId);
        }

        GroupeDePages.createOrUpdate(groupOfPagesFormDto, suiviDeSite)
                .forEach(suiviDeSite::addGroupeDePages);

        suiviDeSiteRepository.persist(suiviDeSite);
    }

    @Override
    public void remove(final Long suiviId) {
        final MisisUser currentAuthenticatedUser = authenticatedUserService.getAndUpdateCurrentAuthenticatedUser();
        final SuiviDeSite suiviDeSite = suiviDeSiteRepository.findById(suiviId);

        boolean canDelete = suiviDeSitePermissionService.isAllowed(currentAuthenticatedUser, suiviDeSite,
                MisisPermission.DELETE);

        if (canDelete) {
            suiviDeSitePermissionService.clearPermission(suiviDeSite);
        }
    }

    @Override
    public List<GroupOfPagesFormDto> getGroupOfPagesForms(Long suiviId) {
        final MisisUser currentAuthenticatedUser = authenticatedUserService.getAndUpdateCurrentAuthenticatedUser();
        final SuiviDeSite suiviDeSite = suiviDeSiteRepository.findById(suiviId);

        boolean canRead = suiviDeSitePermissionService.isAllowed(currentAuthenticatedUser, suiviDeSite,
                MisisPermission.READ);

        if (!canRead) {
            throw new EntityNotFound(suiviId);
        }

        return groupeDePagesRepository.findGroupsForUpdate(suiviId);
    }
}
