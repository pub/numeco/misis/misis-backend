package fr.numeco.misis.suividesite.service;

import fr.numeco.misis.suividesite.dto.GroupOfPagesFormDto;
import fr.numeco.misis.suividesite.dto.WebsiteUserDto;

import java.util.List;

public interface SuiviDeSiteService {
    List<WebsiteUserDto> getUsersOfTrackingInReading(Long suiviId);

    void updateSharedUsersOfTracking(List<Long> suivisId, List<String> usersEmail);

    void editGroupOfPages(Long suiviId, List<GroupOfPagesFormDto> groupOfPagesFormDto);

    void remove(Long suiviId);

    List<GroupOfPagesFormDto> getGroupOfPagesForms(Long suiviId);
}
