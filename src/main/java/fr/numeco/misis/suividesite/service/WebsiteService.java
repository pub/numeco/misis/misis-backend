package fr.numeco.misis.suividesite.service;

import fr.numeco.misis.enums.RessourceType;
import fr.numeco.misis.suividesite.domain.GroupeDePages;
import fr.numeco.misis.suividesite.domain.Ressource;
import fr.numeco.misis.suividesite.domain.SuiviDeSite;
import fr.numeco.misis.suividesite.dto.GroupDetailsDto;
import fr.numeco.misis.suividesite.dto.WebsiteFormDto;
import fr.numeco.misis.suividesite.dto.WebsiteSummaryDto;
import fr.numeco.misis.suividesite.dto.abst.WebsiteDetails;
import io.smallrye.mutiny.tuples.Tuple2;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static fr.numeco.misis.statistic.service.impl.DefaultStatisticService.IMAGE_THRESHOLD_IN_BYTES;

public interface WebsiteService {
    List<WebsiteSummaryDto> getAllWebsitesVisibleToCurrentUser();

    void create(WebsiteFormDto websiteFormDto);

    WebsiteDetails getPagesDetails(Long suiviId, Long groupId);

    WebsiteDetails getResourcesDetails(Long suiviId, Long groupId, RessourceType type);

    WebsiteSummaryDto getWebsiteByIdVisibleToCurrentUser(Long suiviId);

    static Tuple2<Long, Map<Long, GroupDetailsDto>> getGroupDetails(SuiviDeSite suiviDeSite) {
        return getGroupDetails(suiviDeSite, null);
    }

    static Tuple2<Long, Map<Long, GroupDetailsDto>> getGroupDetails(SuiviDeSite suiviDeSite, Function<GroupeDePages, Void> function) {
        long pageCount=0L;
        final Map<Long, GroupDetailsDto> groups = new TreeMap<>();
        for (GroupeDePages groupeDePages : suiviDeSite.getGroupesDePages()) {
            final Set<RessourceType> availableRessourceTypes = groupeDePages.getPages().stream()
                    .flatMap(page -> page.getRessources().stream())
                    .filter(ressource -> !RessourceType.IMAGE.equals(ressource.getType()) || ressource.getSizeInBytes() > IMAGE_THRESHOLD_IN_BYTES)
                    .map(Ressource::getType)
                    .filter(Objects::nonNull)
                    .collect(Collectors.toCollection(TreeSet::new));

            if (Objects.nonNull(function)) {
                function.apply(groupeDePages);
            }

            pageCount += groupeDePages.getPages().size();
            groups.put(groupeDePages.getId(), new GroupDetailsDto(groupeDePages.getName(), availableRessourceTypes, null));
        }

        return Tuple2.of(pageCount, groups);
    }
}
