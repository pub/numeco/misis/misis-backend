package fr.numeco.misis.suividesite.service.impl;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.Objects;

import fr.numeco.misis.enums.MisisPermission;
import fr.numeco.misis.enums.RessourceType;
import fr.numeco.misis.error.EntityNotFound;
import fr.numeco.misis.permission.service.impl.SuiviDeSitePermissionService;
import fr.numeco.misis.security.service.AuthenticatedUserService;
import fr.numeco.misis.suividesite.domain.SuiviDeSite;
import fr.numeco.misis.suividesite.dto.WebsiteFormDto;
import fr.numeco.misis.suividesite.dto.WebsiteSummaryDto;
import fr.numeco.misis.suividesite.dto.abst.WebsiteDetails;
import fr.numeco.misis.suividesite.mapper.WebsiteDetailsMapper;
import fr.numeco.misis.suividesite.mapper.WebsiteSummaryMapper;
import fr.numeco.misis.suividesite.repository.WebsiteRepository;
import fr.numeco.misis.suividesite.service.WebsiteService;
import fr.numeco.misis.user.domain.MisisUser;
import io.quarkus.scheduler.Scheduler;
import io.quarkus.scheduler.Trigger;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;

@ApplicationScoped
@RequiredArgsConstructor
@Transactional
public class DefaultWebsiteService implements WebsiteService {

    private final WebsiteRepository websiteRepository;
    private final AuthenticatedUserService authenticatedUserService;
    private final SuiviDeSitePermissionService suiviDeSitePermissionService;
    private final WebsiteSummaryMapper websiteSummaryMapper;
    private final WebsiteDetailsMapper websiteDetailsMapper;
    private final Scheduler scheduler;

    @Override
    public List<WebsiteSummaryDto> getAllWebsitesVisibleToCurrentUser() {
        final MisisUser currentConnectedUser = authenticatedUserService.getAndUpdateCurrentAuthenticatedUser();

        return websiteRepository.findAllVisibleByUser(currentConnectedUser)
                .stream()
                .map(suiviDeSite -> websiteSummaryMapper.toDto(suiviDeSite,
                        LocalDateTime.ofInstant(getNextFireTime(), ZoneId.systemDefault())))
                .toList();
    }

    @Override
    @Transactional
    public void create(final WebsiteFormDto websiteFormDto) {
        final MisisUser currentConnectedUser = authenticatedUserService.getAndUpdateCurrentAuthenticatedUser();

        final SuiviDeSite suiviDeSite = SuiviDeSite.create(websiteFormDto, currentConnectedUser);
        websiteRepository.persist(suiviDeSite);
        suiviDeSitePermissionService.addOrCreatePermission(currentConnectedUser, suiviDeSite,
                MisisPermission.getAdminPermissions());
    }

    @Override
    public WebsiteDetails getPagesDetails(final Long suiviId, final Long groupId) {
        final MisisUser currentConnectedUser = authenticatedUserService.getAndUpdateCurrentAuthenticatedUser();

        return websiteRepository.findPagesDetailsById(suiviId, currentConnectedUser)
                .stream().map(suiviDeSite -> websiteDetailsMapper.toPagesDetails(suiviDeSite, groupId))
                .findFirst()
                .orElseThrow(() -> new EntityNotFound(suiviId));
    }

    @Override
    public WebsiteDetails getResourcesDetails(final Long suiviId, final Long groupId, final RessourceType type) {
        final MisisUser currentConnectedUser = authenticatedUserService.getAndUpdateCurrentAuthenticatedUser();

        return websiteRepository.findResourcesDetailsById(suiviId, currentConnectedUser)
                .stream().map(suiviDeSite -> websiteDetailsMapper.toRessourcesDetails(suiviDeSite, groupId, type))
                .findFirst()
                .orElseThrow(() -> new EntityNotFound(suiviId));
    }

    @Override
    public WebsiteSummaryDto getWebsiteByIdVisibleToCurrentUser(final Long suiviId) {
        final MisisUser currentConnectedUser = authenticatedUserService.getAndUpdateCurrentAuthenticatedUser();

        return websiteRepository.findWebsiteByIdVisibleToCurrentUser(suiviId, currentConnectedUser)
                .stream()
                .map(suiviDeSite -> websiteSummaryMapper.toDto(suiviDeSite,
                        LocalDateTime.ofInstant(getNextFireTime(), ZoneId.systemDefault())))
                .findFirst()
                .orElseThrow(() -> new EntityNotFound(suiviId));

    }

    private Instant getNextFireTime() {
        Instant errorTime = Instant.parse("2000-01-01T00:00:00Z");
        try {
            final Trigger analysisSchedulerTrigger = scheduler.getScheduledJob("AnalysisScheduler");
            return Objects.isNull(analysisSchedulerTrigger)
                    ? errorTime
                    : analysisSchedulerTrigger.getNextFireTime();
        } catch (UnsupportedOperationException e) {
            return errorTime;
        }

    }
}
