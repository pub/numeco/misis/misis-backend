package fr.numeco.misis.suividesite.resource;

import java.util.List;

import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.enums.SchemaType;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.jboss.resteasy.reactive.ResponseStatus;

import fr.numeco.misis.security.Constants;
import fr.numeco.misis.suividesite.dto.WebsiteFormDto;
import fr.numeco.misis.suividesite.dto.WebsiteSummaryDto;
import fr.numeco.misis.suividesite.service.WebsiteService;
import jakarta.annotation.security.RolesAllowed;
import jakarta.validation.Valid;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import lombok.RequiredArgsConstructor;

@Path("/suivis-de-site")
@RequiredArgsConstructor
@RolesAllowed(Constants.SERVICE_PUBLIC_ROLE)
public class WebsiteResource {

    private final WebsiteService websiteService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Get all websites visible to current user", description = "Returns a list of websites")
    @APIResponse(responseCode = "200", description = "List of sites", content = @Content(mediaType = "application/json", schema = @Schema(type = SchemaType.ARRAY, implementation = WebsiteSummaryDto.class)))
    public List<WebsiteSummaryDto> getAllWebsitesVisibleToCurrentUser() {
        return websiteService.getAllWebsitesVisibleToCurrentUser();
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Get website by ID visible to current user", description = "Returns a website")
    @APIResponse(responseCode = "200", description = "Website summary", content = @Content(mediaType = "application/json", schema = @Schema(implementation = WebsiteSummaryDto.class)))
    public WebsiteSummaryDto getWebsite(
            @PathParam("id") final Long id
    ) {
        return websiteService.getWebsiteByIdVisibleToCurrentUser(id);
    }

    @POST
    @Operation(summary = "Create a new suivi de site", description = "Creates a new suivi de site in the system.")
    @APIResponse(responseCode = "201", description = "suivi de site created successfully")
    @APIResponse(responseCode = "400", description = "Invalid input")
    @ResponseStatus(201)
    public void create(
            @RequestBody(
                    description = "Suivi de site data to create",
                    required = true,
                    content = @Content(schema = @Schema(implementation = WebsiteFormDto.class))
            ) @Valid final WebsiteFormDto websiteFormDto) {
        websiteService.create(websiteFormDto);
    }
}
