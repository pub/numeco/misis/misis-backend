package fr.numeco.misis.suividesite.resource;

import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;

import fr.numeco.misis.enums.RessourceType;
import fr.numeco.misis.security.Constants;
import fr.numeco.misis.suividesite.dto.WebsitePagesDetailsDto;
import fr.numeco.misis.suividesite.dto.WebsiteResourcesDetailsDto;
import fr.numeco.misis.suividesite.dto.abst.WebsiteDetails;
import fr.numeco.misis.suividesite.service.WebsiteService;
import jakarta.annotation.security.RolesAllowed;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import lombok.RequiredArgsConstructor;

@Path("suivis-de-site/{suiviId}/groupe/{groupId}")
@RequiredArgsConstructor
@RolesAllowed(Constants.SERVICE_PUBLIC_ROLE)
public class WebsiteDetailsResource {

    private final WebsiteService websiteService;

    @GET
    @Path("/liste-des-pages")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Get a website's page list by ID", description = "Returns the list of pages based on the provided ID.")
    @APIResponse(responseCode = "200", description = "Page list details", content = @Content(mediaType = "application/json", schema = @Schema(implementation = WebsitePagesDetailsDto.class)))
    @APIResponse(responseCode = "404", description = "Site or Group not found")
    public WebsiteDetails getWebsitePagesDetails(
            @Parameter(description = "ID of the site to retrieve", required = true) @PathParam("suiviId") Long suiviId,
            @Parameter(description = "ID of the group to view", required = true) @PathParam("groupId") Long groupId) {

        return websiteService.getPagesDetails(suiviId, groupId);
    }

    @GET
    @Path("/liste-des-ressources")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Get a website's resource list by ID", description = "Returns the list of resources based on the provided ID.")
    @APIResponse(responseCode = "200", description = "Resources list details", content = @Content(mediaType = "application/json", schema = @Schema(implementation = WebsiteResourcesDetailsDto.class)))
    @APIResponse(responseCode = "404", description = "Site or Group not found")
    public WebsiteDetails getWebsiteResourcesDetails(
            @Parameter(description = "ID of the site to retrieve", required = true) @PathParam("suiviId") Long suiviId,
            @Parameter(description = "ID of the group to view", required = true) @PathParam("groupId") Long groupId,
            @Parameter(description = "Type of resources to view", required = true) @QueryParam("type") RessourceType type) {

        return websiteService.getResourcesDetails(suiviId, groupId, type);
    }

}
