package fr.numeco.misis.suividesite.resource;

import java.util.List;

import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.enums.SchemaType;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;

import fr.numeco.misis.security.Constants;
import fr.numeco.misis.suividesite.dto.GroupOfPagesFormDto;
import fr.numeco.misis.suividesite.dto.SettingsFormDto;
import fr.numeco.misis.suividesite.dto.WebsiteSettingsDto;
import fr.numeco.misis.suividesite.service.SuiviDeSiteService;
import jakarta.annotation.security.RolesAllowed;
import jakarta.validation.Valid;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import lombok.AllArgsConstructor;

@Path("/suivis-de-site/settings")
@AllArgsConstructor
@RolesAllowed(Constants.SERVICE_PUBLIC_ROLE)
public class WebsiteSettingsRessource {

    private final SuiviDeSiteService suiviDeSiteService;

    @GET
    @Path("/{suiviId}")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Get suivi de site settings", description = "Returns a list of users who have been shared")
    @APIResponse(responseCode = "200", description = "Paramètre du suivi de site", content = @Content(mediaType = "application/json", schema = @Schema(implementation = WebsiteSettingsDto.class)))
    public WebsiteSettingsDto getSettings(@PathParam("suiviId") final Long suiviId) {
        return new WebsiteSettingsDto(
                suiviDeSiteService.getUsersOfTrackingInReading(suiviId),
                suiviDeSiteService.getGroupOfPagesForms(suiviId)
        );
    }

    @POST
    @Operation(summary = "Define the suivi de site settings", description = "Erases the old settings with the new")
    @APIResponse(responseCode = "204", description = "updated successfully")
    @APIResponse(responseCode = "404", description = "Site not found")
    public void updateSettings(@RequestBody @Valid final SettingsFormDto settingsFormDto) {
        suiviDeSiteService.updateSharedUsersOfTracking(settingsFormDto.getSuivisId(), settingsFormDto.getUsersEmail());
    }

    @POST
    @Path("/{suiviId}")
    @Operation(summary = "Edit group of pages", description = "Edit group of pages on selected website")
    @APIResponse(responseCode = "204", description = "updated successfully")
    @APIResponse(responseCode = "404", description = "Site not found")
    public void editGroupeOfPages(@PathParam("suiviId") final Long suiviId,
                                  @RequestBody @Valid final List<GroupOfPagesFormDto> groupOfPagesFormDto) {
        suiviDeSiteService.editGroupOfPages(suiviId, groupOfPagesFormDto);
    }

    @DELETE
    @Path("/{suiviId}")
    @Operation(summary = "remove website")
    @APIResponse(responseCode = "204", description = "deleted successfully")
    @APIResponse(responseCode = "404", description = "Site not found", content = @Content(mediaType = "application/json", schema = @Schema(implementation = GroupOfPagesFormDto.class, type = SchemaType.ARRAY)))
    public void removeWebsite(@PathParam("suiviId") final Long suiviId) {
        suiviDeSiteService.remove(suiviId);
    }

}
