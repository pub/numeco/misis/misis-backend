package fr.numeco.misis.suividesite.repository;

import fr.numeco.misis.enums.MisisPermission;
import fr.numeco.misis.suividesite.domain.SuiviDeSite;
import fr.numeco.misis.user.domain.MisisUser;
import io.quarkus.hibernate.orm.panache.PanacheQuery;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import jakarta.enterprise.context.ApplicationScoped;
import lombok.AllArgsConstructor;

@ApplicationScoped
@AllArgsConstructor
public class WebsiteRepository implements PanacheRepository<SuiviDeSite> {
    private static final String ORDER_BY_OWNERSHIP = """
                ORDER BY
                CASE owner.email
                    WHEN ?1 THEN 0
                    ELSE 1 END
            """;

    private static final String QUERY_ALL_WEBSITE_SUMMARY_VISIBLE_BY_USER = """
            SELECT s
            FROM PermissionSurLeSuiviDeSite AS p
            JOIN p.domain AS s
            JOIN s.groupesDePages AS g
            JOIN g.pages AS gp
            JOIN p.allowed AS pt
            JOIN p.user AS u
            JOIN s.createdBy AS owner
            WHERE u.email = ?1 AND pt.misisPermission = ?2
            GROUP BY s.id, s.name, s.url, owner.email
            """ + ORDER_BY_OWNERSHIP;

    private static final String QUERY_WEBSITE_SUMMARY_BY_ID_VISIBLE_BY_USER = """
            SELECT s
            FROM PermissionSurLeSuiviDeSite AS p
            JOIN p.domain AS s
            JOIN s.groupesDePages AS g
            JOIN g.pages AS gp
            JOIN p.allowed AS pt
            JOIN p.user AS u
            JOIN s.createdBy AS owner
            WHERE u.email = ?1 AND s.id = ?2 AND pt.misisPermission = ?3
            GROUP BY s.id, owner.id
            ORDER BY
                CASE owner.email
                    WHEN ?1 THEN 0
                    ELSE 1 END
            """;

    private static final String QUERY_WEBSITE_BY_ID = """
            SELECT s
            FROM PermissionSurLeSuiviDeSite AS p
            JOIN p.domain AS s
            JOIN s.groupesDePages AS g
            JOIN g.pages AS gp
            JOIN p.allowed AS pt
            JOIN p.user AS u
            JOIN s.createdBy AS owner
            WHERE u.email = ?1 AND s.id = ?2 AND pt.misisPermission = ?3
            GROUP BY s.id, owner.email
            """ + ORDER_BY_OWNERSHIP;

    public PanacheQuery<SuiviDeSite> findAllVisibleByUser(final MisisUser misisUser) {
        return find(QUERY_ALL_WEBSITE_SUMMARY_VISIBLE_BY_USER, misisUser.getEmail(), MisisPermission.READ);
    }

    public PanacheQuery<SuiviDeSite> findWebsiteByIdVisibleToCurrentUser(final Long id, final MisisUser misisUser) {
        return find(QUERY_WEBSITE_SUMMARY_BY_ID_VISIBLE_BY_USER, misisUser.getEmail(), id, MisisPermission.READ);
    }

    public PanacheQuery<SuiviDeSite> findPagesDetailsById(final Long suiviId, final MisisUser misisUser) {
        return find(QUERY_WEBSITE_BY_ID, misisUser.getEmail(), suiviId, MisisPermission.READ);
    }

    public PanacheQuery<SuiviDeSite> findResourcesDetailsById(final Long suiviId, final MisisUser misisUser) {
        return find(QUERY_WEBSITE_BY_ID, misisUser.getEmail(), suiviId, MisisPermission.READ);

    }

}
