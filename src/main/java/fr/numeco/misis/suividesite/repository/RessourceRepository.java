package fr.numeco.misis.suividesite.repository;

import fr.numeco.misis.suividesite.domain.Ressource;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import jakarta.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class RessourceRepository implements PanacheRepository<Ressource> {
}
