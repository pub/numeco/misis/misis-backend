package fr.numeco.misis.suividesite.repository;

import fr.numeco.misis.suividesite.domain.Page;
import io.quarkus.hibernate.orm.panache.PanacheQuery;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import jakarta.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class PageRepository  implements PanacheRepository<Page> {
    private static final String QUERY_ANALYZABLE_PAGES = """
                SELECT p FROM Page p
                JOIN p.groupeDePages g
                WHERE p.analyseStatut = 'NOUVEAU' OR
                (
                    p.analyseStatut = 'TERMINEE' AND
                    (
                        (p.analysedAt < CURRENT_DATE AND g.periodiciteDuSuivi = 'QUOTIDIEN')
                        OR (g.periodiciteDuSuivi = 'HEBDOMADAIRE' AND DATEADD(week, -1, CURRENT_DATE) > p.analysedAt)
                        OR (g.periodiciteDuSuivi = 'MENSUEL' AND DATEADD(month, -1, CURRENT_DATE) > p.analysedAt)
                    )
                )
                ORDER BY p.modifiedAt ASC
            """;

    public PanacheQuery<Page> findAvailableByPeriodicity() {
        return find(QUERY_ANALYZABLE_PAGES);
    }

}
