package fr.numeco.misis.suividesite.repository;

import fr.numeco.misis.enums.MisisPermission;
import fr.numeco.misis.suividesite.domain.SuiviDeSite;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import jakarta.enterprise.context.ApplicationScoped;

import java.util.List;

@ApplicationScoped
public class SuiviDeSiteRepository implements PanacheRepository<SuiviDeSite> {

    private static final String QUERY_ALL_VISIBLE_BY_USER = """
            SELECT s
            FROM PermissionSurLeSuiviDeSite AS p
            JOIN p.domain AS s
            JOIN p.user AS u
            JOIN s.createdBy AS owner
            JOIN p.allowed AS pt
            WHERE owner.email = ?1 OR (u.email = ?1 AND pt.misisPermission = ?2)
            ORDER BY
                CASE owner.email
                    WHEN ?1 THEN 0
                    ELSE 1 END
            """;

    public boolean existsByName(String name) {
        return find("name", name).singleResultOptional().isPresent();
    }

    public boolean existsByUrl(String url) {
        return find("url", url).singleResultOptional().isPresent();
    }

    public List<SuiviDeSite> findAllByUserId(Long id) {
        return list("""
                SELECT s from SuiviDeSite s
                JOIN s.users u
                WHERE u.id = ?1
                """, id);
    }

    public List<SuiviDeSite> findAllVisibleByUser(String email) {
        return this.list(QUERY_ALL_VISIBLE_BY_USER, email, MisisPermission.READ);
    }

    public List<SuiviDeSite> findLatestAnalysis(List<Long> ids) {
        return find("""
                SELECT s FROM SuiviDeSite AS s
                JOIN s.groupesDePages AS g
                JOIN g.pages AS p
                WHERE p.id IN (?1)
                GROUP BY s
                """, ids).list();
    }
}
