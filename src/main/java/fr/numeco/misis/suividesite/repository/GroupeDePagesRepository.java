package fr.numeco.misis.suividesite.repository;

import fr.numeco.misis.suividesite.domain.GroupeDePages;
import fr.numeco.misis.suividesite.dto.GroupOfPagesFormDto;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import jakarta.enterprise.context.ApplicationScoped;

import java.util.List;

@ApplicationScoped
public class GroupeDePagesRepository implements PanacheRepository<GroupeDePages> {

    private static final String QUERY_GROUPS_ANALYZED_FOR_HISTORISATION = """
            SELECT g FROM GroupeDePages AS g
            LEFT JOIN (
                SELECT s.groupeDePages AS latest_group, MAX(s.timestamp) AS latest_date FROM Statistique AS s GROUP BY s.groupeDePages
            ) AS s ON s.latest_group = g
            WHERE s.latest_group IS NULL
                OR ( g.periodiciteDuSuivi = 'QUOTIDIEN' AND s.latest_date < CURRENT_DATE )
                OR ( g.periodiciteDuSuivi = 'HEBDOMADAIRE' AND s.latest_date < DATEADD(week, -1, CURRENT_DATE) )
                OR ( g.periodiciteDuSuivi = 'MENSUEL' AND s.latest_date < DATEADD(month, -1, CURRENT_DATE) )
            GROUP BY g
            """;
    public static final String FIND_GROUP_FOR_UPDATE = """
            SELECT g.id, g.name, g.periodiciteDuSuivi, g.methodeDeCreationDeGroupe, JSON_AGG(DISTINCT JSONB_BUILD_OBJECT(p.id, p.url))
            FROM GroupeDePages AS g
            JOIN g.pages AS p
            JOIN g.suiviDeSite AS s
            WHERE s.id = ?1
            GROUP BY g.id
            """;

    public List<GroupeDePages> findGroupsAnalysedForHistorization() {
        return list(QUERY_GROUPS_ANALYZED_FOR_HISTORISATION);
    }

    public List<GroupOfPagesFormDto> findGroupsForUpdate(Long suiviId) {
        return find(FIND_GROUP_FOR_UPDATE, suiviId)
                .project(GroupOfPagesFormDto.class)
                .list();
    }
}
