package fr.numeco.misis.notification.repository;

import fr.numeco.misis.notification.domain.Notification;
import fr.numeco.misis.notification.dto.NotificationDto;
import fr.numeco.misis.notification.mapper.NotificationMapper;
import fr.numeco.misis.user.domain.MisisUser;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import jakarta.enterprise.context.ApplicationScoped;
import lombok.AllArgsConstructor;

import java.util.List;

@ApplicationScoped
@AllArgsConstructor
public class NotificationRepository implements PanacheRepository<Notification> {

    private static final String QUERY_VISIBLE_NOTIFICATION = "SELECT n FROM Notification n WHERE n.visible=?1 AND n.user=?2 ORDER BY n.createdAt DESC";
    private static final String UPDATE_NOTIFICATIONS = "visible = false WHERE user=?1 AND visible=true AND id IN (?2)";

    private final NotificationMapper notificationMapper;

    public List<NotificationDto> findVisibleNotification(MisisUser misisUser) {
        return find(QUERY_VISIBLE_NOTIFICATION, true, misisUser)
                .stream().map(notificationMapper::toDto)
                .toList();
    }

    public void updateNotifications(MisisUser misisUser, List<Long> ids) {
        update(UPDATE_NOTIFICATIONS, misisUser, ids);
    }
}
