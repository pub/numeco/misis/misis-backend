package fr.numeco.misis.notification.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@AllArgsConstructor
@Getter
@Setter
public class NotificationDto {
    @NotNull
    private Long id;
    @NotBlank
    private String message;
    @NotNull
    private LocalDateTime createdAt;
    @NotNull
    private Boolean visible;
}
