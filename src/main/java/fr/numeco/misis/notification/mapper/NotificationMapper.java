package fr.numeco.misis.notification.mapper;

import fr.numeco.misis.notification.domain.Notification;
import fr.numeco.misis.notification.dto.NotificationDto;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

@Mapper(componentModel = MappingConstants.ComponentModel.JAKARTA)
public interface NotificationMapper {
    NotificationDto toDto(Notification notification);
}
