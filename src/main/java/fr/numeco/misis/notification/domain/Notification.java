package fr.numeco.misis.notification.domain;

import fr.numeco.misis.user.domain.MisisUser;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Notification {
    private static final String ENTITY_SEQ = "notification_SEQ";

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = ENTITY_SEQ)
    @SequenceGenerator(name = ENTITY_SEQ, sequenceName = ENTITY_SEQ, allocationSize = 1)
    @Column(nullable = false)
    private Long id;

    @Column(nullable = false)
    private String message;

    @Column(nullable = false)
    private boolean visible = Boolean.TRUE;

    @ManyToOne(fetch = FetchType.LAZY)
    private MisisUser user;

    @Column(nullable = false)
    private LocalDateTime createdAt = LocalDateTime.now();

    public static Notification create(final MisisUser user, final String message) {
        final Notification notification = new Notification();
        notification.setUser(user);
        notification.setMessage(message);
        return notification;
    }
}
