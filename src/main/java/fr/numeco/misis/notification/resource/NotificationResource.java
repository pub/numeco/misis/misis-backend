package fr.numeco.misis.notification.resource;

import java.util.List;

import fr.numeco.misis.notification.dto.NotificationDto;
import fr.numeco.misis.notification.service.NotificationService;
import fr.numeco.misis.security.Constants;
import jakarta.annotation.security.RolesAllowed;
import jakarta.validation.Valid;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.PATCH;
import jakarta.ws.rs.Path;
import lombok.AllArgsConstructor;

@Path("/notifications")
@RolesAllowed(Constants.SERVICE_PUBLIC_ROLE)
@AllArgsConstructor
public class NotificationResource {

    private final NotificationService notificationService;

    @GET
    public List<NotificationDto> getVisibleNotification() {
        return notificationService.getVisibleNotification();
    }

    @PATCH
    public void markAsRead(@Valid final List<NotificationDto> notificationDtos) {
        notificationService.markAsRead(notificationDtos);
    }
}
