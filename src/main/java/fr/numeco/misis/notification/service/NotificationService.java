package fr.numeco.misis.notification.service;

import fr.numeco.misis.notification.domain.Notification;
import fr.numeco.misis.notification.dto.NotificationDto;
import fr.numeco.misis.notification.repository.NotificationRepository;
import fr.numeco.misis.security.service.AuthenticatedUserService;
import fr.numeco.misis.suividesite.domain.SuiviDeSite;
import fr.numeco.misis.user.domain.MisisUser;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;

import java.util.List;

@ApplicationScoped
@AllArgsConstructor
@Transactional
public class NotificationService {

    private static final String ACCESS_MODIFIED_FORMAT = "Votre accès au suivi de site '%s' a été modifié";
    private static final String ANALYSIS_COMPLETED_FORMAT = "L'analyse du suivi de site '%s' est terminée";
    private static final String LIST_OF_USERS_MODIFIED_FORMAT = "La liste des utilisateurs ayant accès au suivi de site '%s' a été modifiée";
    private final AuthenticatedUserService misisAuthenticatedUserService;
    private final NotificationRepository notificationRepository;

    public List<NotificationDto> getVisibleNotification() {
        final MisisUser misisUser = misisAuthenticatedUserService.getAndUpdateCurrentAuthenticatedUser();
        return notificationRepository.findVisibleNotification(misisUser);
    }

    protected void persistNotification(final String message, final MisisUser misisUser) {
        notificationRepository.persist(Notification.create(misisUser, message));
    }

    public void markAsRead(final List<NotificationDto> notificationDtos) {
        final MisisUser misisUser = misisAuthenticatedUserService.getAndUpdateCurrentAuthenticatedUser();
        final List<Long> ids = notificationDtos.stream()
                .map(NotificationDto::getId)
                .toList();
        notificationRepository.updateNotifications(misisUser, ids);
    }

    public void createNotificationAccessModified(final SuiviDeSite suiviDeSite, final MisisUser misisUser) {
        persistNotification(String.format(ACCESS_MODIFIED_FORMAT, suiviDeSite.getName()), misisUser);
    }

    public void createNotificationAnalysisCompleted(final SuiviDeSite suiviDeSite) {
        persistNotification(String.format(ANALYSIS_COMPLETED_FORMAT, suiviDeSite.getName()), suiviDeSite.getCreatedBy());
    }

    public void createNotificationListOfUsersWithAccessHasBeenModified(final SuiviDeSite suiviDeSite) {
        persistNotification(String.format(LIST_OF_USERS_MODIFIED_FORMAT, suiviDeSite.getName()), suiviDeSite.getCreatedBy());
    }
}
