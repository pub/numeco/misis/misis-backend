package fr.numeco.misis.error;

import io.quarkus.logging.Log;
import jakarta.ws.rs.NotFoundException;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

@Provider
public class MisisExceptionHandler implements ExceptionMapper<Exception> {

    @Override
    public Response toResponse(Exception e) {
        if (e instanceof EntityNotFound || e instanceof NotFoundException) {
            ErrorResponse errorResponse = new ErrorResponse(
                    Response.Status.NOT_FOUND,
                    e.getMessage()
            );

            return Response
                    .status(Response.Status.NOT_FOUND)
                    .entity(errorResponse)
                    .type(MediaType.APPLICATION_JSON)
                    .build();
        } else if (e instanceof UniqueConstraintViolationException) {
            ErrorResponse errorResponse = new ErrorResponse(
                    Response.Status.BAD_REQUEST,
                    e.getMessage()
            );

            return Response
                    .status(Response.Status.BAD_REQUEST)
                    .entity(errorResponse)
                    .type(MediaType.APPLICATION_JSON)
                    .build();
        }

        Log.error("unhandled exception", e);
        ErrorResponse errorResponse = new ErrorResponse(
                Response.Status.INTERNAL_SERVER_ERROR,
                e != null ? e.getMessage() : "An error occurred"
        );

        return Response
                .status(Response.Status.INTERNAL_SERVER_ERROR)
                .entity(errorResponse)
                .type(MediaType.APPLICATION_JSON)
                .build();
    }
}
