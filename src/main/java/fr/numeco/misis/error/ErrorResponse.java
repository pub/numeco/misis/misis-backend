package fr.numeco.misis.error;

import jakarta.ws.rs.core.Response;
import lombok.Data;

import java.time.Instant;

@Data
public class ErrorResponse {
    private Instant timestamp;
    private Response.Status status;
    private String message;

    public ErrorResponse(Response.Status status, String message) {
        this.timestamp = Instant.now();
        this.status = status;
        this.message = message;
    }
}
