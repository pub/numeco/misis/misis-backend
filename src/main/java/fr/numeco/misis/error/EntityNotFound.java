package fr.numeco.misis.error;

public class EntityNotFound extends RuntimeException {

    public EntityNotFound(Long id) {
        super("Entity with id " + id + " not found");
    }

    public EntityNotFound(String message) {
        super(message);
    }
}
