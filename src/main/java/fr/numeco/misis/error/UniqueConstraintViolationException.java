package fr.numeco.misis.error;

public class UniqueConstraintViolationException extends RuntimeException {

        public UniqueConstraintViolationException(String field, String value) {
            super("An entity with the same " + field + " already exists: " + value);
        }
}
