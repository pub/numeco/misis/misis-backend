package fr.numeco.misis.error;

public class InvalidUserInfoException extends RuntimeException{
    public InvalidUserInfoException() {
    }

    public InvalidUserInfoException(Throwable cause) {
        super(cause);
    }

    public InvalidUserInfoException(String message) {
        super(message);
    }

    public InvalidUserInfoException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidUserInfoException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
