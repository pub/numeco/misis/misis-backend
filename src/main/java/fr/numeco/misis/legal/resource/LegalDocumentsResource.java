package fr.numeco.misis.legal.resource;

import fr.numeco.misis.user.service.MisisUserService;
import io.quarkus.oidc.AuthorizationCodeTokens;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import lombok.AllArgsConstructor;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;

@Path("/legalDocuments")
@AllArgsConstructor
public class LegalDocumentsResource {

    private final MisisUserService misisUserService;

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @APIResponse(responseCode = "200", description = "exchange successfully", content = @Content(mediaType = "application/json", schema = @Schema(implementation = AuthorizationCodeTokens.class)))
    @APIResponse(responseCode = "401", description = "Unauthorized")
    public void legalDocumentAcceptCgu() {
        misisUserService.acceptCgu();

    }
}
