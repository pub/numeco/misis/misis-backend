package fr.numeco.misis.legal.repository;

import fr.numeco.misis.legal.domain.LegalDocuments;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import jakarta.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class LegalDocumentsRepository implements PanacheRepository<LegalDocuments> {

    public LegalDocuments findLatest() {
        return find("order by effectiveDate desc").firstResult();
    }
}
