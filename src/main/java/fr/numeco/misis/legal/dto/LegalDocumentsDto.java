package fr.numeco.misis.legal.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class LegalDocumentsDto {
    private final String version;//NOSONAR
    private final String content;//NOSONAR
}
