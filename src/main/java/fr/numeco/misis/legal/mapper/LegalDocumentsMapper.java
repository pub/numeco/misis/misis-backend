package fr.numeco.misis.legal.mapper;

import fr.numeco.misis.legal.domain.LegalDocuments;
import fr.numeco.misis.legal.dto.LegalDocumentsDto;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

@Mapper(componentModel = MappingConstants.ComponentModel.JAKARTA)
public interface LegalDocumentsMapper {
    LegalDocumentsDto toDto(LegalDocuments legalDocuments);
}
