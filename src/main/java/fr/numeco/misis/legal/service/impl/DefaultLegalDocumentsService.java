package fr.numeco.misis.legal.service.impl;

import fr.numeco.misis.legal.domain.LegalDocuments;
import fr.numeco.misis.legal.dto.LegalDocumentsDto;
import fr.numeco.misis.legal.mapper.LegalDocumentsMapper;
import fr.numeco.misis.legal.repository.LegalDocumentsRepository;
import fr.numeco.misis.legal.service.LegalDocumentsService;
import fr.numeco.misis.security.service.AuthenticatedUserService;
import fr.numeco.misis.user.domain.MisisUser;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;

import java.util.Objects;

@ApplicationScoped
@AllArgsConstructor
@Transactional
public class DefaultLegalDocumentsService implements LegalDocumentsService {

    private final AuthenticatedUserService authenticatedUserService;
    private final LegalDocumentsRepository legalDocumentsRepository;
    private final LegalDocumentsMapper legalDocumentsMapper;

    @Override
    public LegalDocumentsDto getLatestRequiredToContinue(final String email) {
        final LegalDocuments legalDocuments = legalDocumentsRepository.findLatest();
        final MisisUser misisUser = authenticatedUserService.getAndUpdateCurrentAuthenticatedUser(email);

        if (Objects.isNull(legalDocuments) || Objects.isNull(misisUser) || StringUtils.equals(legalDocuments.getVersion(), misisUser.getAcceptedTermsVersion())) {
            return null;
        }

        return legalDocumentsMapper.toDto(legalDocuments);
    }

}
