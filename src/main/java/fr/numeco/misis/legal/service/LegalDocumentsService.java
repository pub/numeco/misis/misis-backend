package fr.numeco.misis.legal.service;

import fr.numeco.misis.legal.dto.LegalDocumentsDto;

public interface LegalDocumentsService {

    LegalDocumentsDto getLatestRequiredToContinue(final String email);

}
