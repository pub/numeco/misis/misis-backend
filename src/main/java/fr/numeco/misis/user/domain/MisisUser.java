package fr.numeco.misis.user.domain;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import fr.numeco.misis.suividesite.domain.SuiviDeSite;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Index;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(indexes = {
        @Index(name = "email_index", columnList = "email"),
})
public class MisisUser {

    private static final String ENTITY_SEQ = "misis_user_SEQ";

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = ENTITY_SEQ)
    @SequenceGenerator(name = ENTITY_SEQ, sequenceName = ENTITY_SEQ, allocationSize = 1)
    @Column(nullable = false)
    private Long id;

    @Column(nullable = false)
    private String username;

    @Column(nullable = false, unique = true)
    private String email;

    @ManyToMany
    private List<SuiviDeSite> suivisDeSite = new ArrayList<>();

    @Column(nullable = false, updatable = false)
    private LocalDate createdDate = LocalDate.now();
    private String acceptedTermsVersion;
}
