package fr.numeco.misis.user.dto;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class MisisUserBaseDto {
    @NotBlank
    private final String username; //NOSONAR
    @NotBlank
    @Email
    private final String email; //NOSONAR
}
