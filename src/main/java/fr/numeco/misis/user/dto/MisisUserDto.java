package fr.numeco.misis.user.dto;

import jakarta.validation.constraints.NotNull;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@EqualsAndHashCode(callSuper = true)
public class MisisUserDto extends MisisUserBaseDto {
    @NotNull
    private final Long id; //NOSONAR

    public MisisUserDto(String username, String email, Long id) {
        super(username, email);
        this.id = id;
    }
}
