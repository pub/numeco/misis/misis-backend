package fr.numeco.misis.user.service;

import fr.numeco.misis.error.EntityNotFound;
import fr.numeco.misis.error.UniqueConstraintViolationException;
import fr.numeco.misis.legal.domain.LegalDocuments;
import fr.numeco.misis.legal.repository.LegalDocumentsRepository;
import fr.numeco.misis.security.service.AuthenticatedUserService;
import fr.numeco.misis.user.domain.MisisUser;
import fr.numeco.misis.user.dto.MisisUserBaseDto;
import fr.numeco.misis.user.dto.MisisUserDto;
import fr.numeco.misis.user.mapper.MisisUserMapper;
import fr.numeco.misis.user.repository.MisisUserRepository;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;

import java.util.Collections;
import java.util.List;

@ApplicationScoped
@RequiredArgsConstructor
@Transactional
public class MisisUserService {

    private final MisisUserMapper userMapper;
    private final MisisUserRepository userRepository;
    private final AuthenticatedUserService authenticatedUserService;
    private final LegalDocumentsRepository legalDocumentsRepository;


    public MisisUser create(MisisUserBaseDto userDto) {

        controlUniqueConstraints(userDto);

        MisisUser misisUser = userMapper.toMisisUser(userDto);
        userRepository.persist(misisUser);
        return misisUser;
    }

    public List<MisisUserDto> getAll() {
        return userRepository.listAll().stream()
                .map(userMapper::toMisisUserDto)
                .toList();
    }

    public MisisUserDto getById(Long id) {
        MisisUser found = userRepository.findByIdOptional(id).orElseThrow(
                () -> new EntityNotFound(id)
        );
        return userMapper.toMisisUserDto(found);
    }

    public void deleteById(Long id) {
        MisisUser found = userRepository.findByIdOptional(id).orElseThrow(
                () -> new EntityNotFound(id)
        );
        userRepository.delete(found);
    }

    public MisisUser updateById(Long id, MisisUserDto userDto) {
        return userRepository.findByIdOptional(id).map(user -> {
            user.setEmail(userDto.getEmail());
            user.setUsername(userDto.getUsername());
            return user;
        }).orElseThrow(
                () -> new EntityNotFound(id)
        );
    }

    private void controlUniqueConstraints(MisisUserBaseDto userDto) {
        boolean uniqueUsername = userRepository.existsByUsername(userDto.getUsername());
        if (uniqueUsername) {
            throw new UniqueConstraintViolationException("username", userDto.getUsername());
        }

        boolean uniqueEmail = userRepository.existsByEmail(userDto.getEmail());
        if (uniqueEmail) {
            throw new UniqueConstraintViolationException("email", userDto.getEmail());
        }
    }

    public List<MisisUserDto> getUsersByEmailStartWith(final String email) {
        if (email == null || email.isBlank()) {
            return Collections.emptyList();
        }

        return userRepository.searchByEmailStartWith(email.trim().toLowerCase());
    }

    public void acceptCgu() {
        MisisUser misisUser = authenticatedUserService.getAndUpdateCurrentAuthenticatedUser();
        LegalDocuments legalDocuments = legalDocumentsRepository.findLatest();
        misisUser.setAcceptedTermsVersion(legalDocuments.getVersion());

        userRepository.persist(misisUser);
    }
}
