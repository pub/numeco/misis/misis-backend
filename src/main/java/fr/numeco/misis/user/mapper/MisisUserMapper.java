package fr.numeco.misis.user.mapper;

import fr.numeco.misis.user.domain.MisisUser;
import fr.numeco.misis.user.dto.MisisUserBaseDto;
import fr.numeco.misis.user.dto.MisisUserDto;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

@Mapper(componentModel = MappingConstants.ComponentModel.JAKARTA)
public interface MisisUserMapper {

    MisisUserDto toMisisUserDto(MisisUser user);
    MisisUser toMisisUser(MisisUserBaseDto userDto);
}
