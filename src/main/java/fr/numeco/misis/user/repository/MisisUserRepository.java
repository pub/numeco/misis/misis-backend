package fr.numeco.misis.user.repository;

import fr.numeco.misis.enums.MisisPermission;
import fr.numeco.misis.suividesite.domain.SuiviDeSite;
import fr.numeco.misis.suividesite.dto.WebsiteUserDto;
import fr.numeco.misis.suividesite.mapper.WebsiteUserMapper;
import fr.numeco.misis.user.domain.MisisUser;
import fr.numeco.misis.user.dto.MisisUserDto;
import fr.numeco.misis.user.mapper.MisisUserMapper;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import jakarta.enterprise.context.ApplicationScoped;
import lombok.AllArgsConstructor;

import java.util.List;

@ApplicationScoped
@AllArgsConstructor
public class MisisUserRepository implements PanacheRepository<MisisUser> {
    private static final String QUERY_EMAIL_START_WITH = """
            SELECT u.username, u.email, u.id
            FROM MisisUser AS u 
            WHERE u.email LIKE ?1
            """;
    private static final String QUERY_ALL_BY_EMAIL = """
            SELECT u 
            FROM MisisUser AS u
            WHERE u.email IN (?1) AND u != ?2
            """;
    private static final String QUERY_USERS_OF_TRACKING_IN_READING = """
            SELECT u
            FROM PermissionSurLeSuiviDeSite AS p
            JOIN p.domain AS s
            JOIN p.user AS u
            JOIN p.allowed AS pt
            WHERE s=?1 AND pt.misisPermission=?2
            GROUP BY u.id, s.createdBy.id
            """;

    private final WebsiteUserMapper websiteUserMapper;
    private final MisisUserMapper misisUserMapper;

    public boolean existsByEmail(String email) {
        return find("email", email).singleResultOptional().isPresent();
    }

    public boolean existsByUsername(String username) {
        return find("username", username).singleResultOptional().isPresent();
    }

    public List<MisisUserDto> searchByEmailStartWith(final String email) {
        return find(QUERY_EMAIL_START_WITH, email + "%")
                .stream().map(misisUserMapper::toMisisUserDto)
                .toList();
    }

    public List<MisisUser> findAllByEmailAndCurrentUser(final List<String> usersEmail, final MisisUser excludeUser) {
        return list(QUERY_ALL_BY_EMAIL, usersEmail, excludeUser);
    }

    public List<WebsiteUserDto> findUsersOfTrackingInReading(final SuiviDeSite suiviDeSite) {
        return find(QUERY_USERS_OF_TRACKING_IN_READING, suiviDeSite, MisisPermission.READ)
                .stream().map(misisUser -> websiteUserMapper.toDto(misisUser, suiviDeSite))
                .toList();
    }

}
