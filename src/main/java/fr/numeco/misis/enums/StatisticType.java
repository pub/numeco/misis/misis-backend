package fr.numeco.misis.enums;

public enum StatisticType {
    TRANSFERRED_DATA,
    REQUESTS,
    DOM_ELEMENTS,
    IMAGE,
    PDF,
    OTHERS,
    STREAMING
}
