package fr.numeco.misis.enums;

import lombok.Getter;

import java.util.Arrays;
import java.util.List;

@Getter
public enum MisisPermission {
    READ("Lecture"),
    WRITE("Écriture"),
    DELETE("Suppression");

    private final String defaultName;

    MisisPermission(String name) {
        this.defaultName = name;
    }

    public static List<MisisPermission> getAdminPermissions() {
        return Arrays.asList(
                MisisPermission.WRITE,
                MisisPermission.READ,
                MisisPermission.DELETE
        );
    }

    public static List<MisisPermission> getUserPermissions() {
        return Arrays.asList(
                MisisPermission.WRITE,
                MisisPermission.READ,
                MisisPermission.DELETE
        );
    }

}
