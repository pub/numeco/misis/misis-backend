package fr.numeco.misis.enums;

public enum RessourceType {
    IMAGE,
    PDF,
    OTHERS,
    PAGE,
    UNDEFINED,
    STREAMING
}