package fr.numeco.misis.enums;

public enum Alert {
    WARNING,
    ERROR,
}
