package fr.numeco.misis.enums;

public enum LoadingType {
    LANDING,
    SCROLLING,
    DOWNLOADING
}
