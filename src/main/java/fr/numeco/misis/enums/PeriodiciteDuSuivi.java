package fr.numeco.misis.enums;

public enum PeriodiciteDuSuivi {
    QUOTIDIEN,
    HEBDOMADAIRE,
    MENSUEL,
}
