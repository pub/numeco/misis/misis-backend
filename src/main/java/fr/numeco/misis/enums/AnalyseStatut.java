package fr.numeco.misis.enums;

public enum AnalyseStatut {
    NOUVEAU,
    EN_COURS,
    TERMINEE,
    INVALIDE,
    ERREUR_URL_VIDE,
    ERREUR_URL_MALFORMEE,
    ERREUR_URL_NON_RESOLUE,
    ERREUR_PAGE_302,
    ERREUR_PAGE_404,
    ERREUR_PAGE_500;

    public static AnalyseStatut getAnalyseStatut(long statut) {
        if (statut == 302L) {
            return AnalyseStatut.ERREUR_PAGE_302;
        } else if (statut == 404L) {
            return AnalyseStatut.ERREUR_PAGE_404;
        } else if (statut == 500L) {
            return AnalyseStatut.ERREUR_PAGE_500;
        } else {
            return AnalyseStatut.INVALIDE;
        }
    }
}
