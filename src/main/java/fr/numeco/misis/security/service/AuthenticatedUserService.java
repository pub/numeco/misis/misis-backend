package fr.numeco.misis.security.service;

import fr.numeco.misis.user.domain.MisisUser;

public interface AuthenticatedUserService {
    MisisUser getAndUpdateCurrentAuthenticatedUser();
    MisisUser getAndUpdateCurrentAuthenticatedUser(final String email);
    MisisUser createdCurrentAuthenticatedUser(String email);
}
