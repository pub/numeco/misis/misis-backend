package fr.numeco.misis.security.service.impl;

import java.util.Objects;
import java.util.Optional;

import fr.numeco.misis.error.InvalidUserInfoException;
import fr.numeco.misis.security.service.AuthenticatedUserService;
import fr.numeco.misis.user.domain.MisisUser;
import fr.numeco.misis.user.repository.MisisUserRepository;
import io.quarkus.oidc.UserInfo;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;

@ApplicationScoped
@Transactional
@RequiredArgsConstructor
public class DefaultAuthenticatedUserService implements AuthenticatedUserService {

    public static final String DATABASE_USERNAME_FORMAT = "%s %s";

    private final UserInfo userInfo;

    private final MisisUserRepository misisUserRepository;

    @Override
    public MisisUser getAndUpdateCurrentAuthenticatedUser() {
        final String email = userInfo.getEmail();
        if (Objects.isNull(email) || email.isBlank()) {
            throw new InvalidUserInfoException("User Email is empty.");
        }
        return getAndUpdateCurrentAuthenticatedUser(email.toLowerCase());
    }

    @Override
    public MisisUser getAndUpdateCurrentAuthenticatedUser(final String email) {
        final Optional<MisisUser> optionalCurrentConnectedUser = misisUserRepository.find("email", email)
                .singleResultOptional();

        MisisUser misisUser;
        if (optionalCurrentConnectedUser.isPresent()) {
            misisUser = optionalCurrentConnectedUser.get();
        } else {
            misisUser = createdCurrentAuthenticatedUser(email);
        }

        return misisUser;
    }

    @Override
    public MisisUser createdCurrentAuthenticatedUser(String email) {
        final MisisUser misisUser = new MisisUser();
        setUserInfo(email.toLowerCase(), misisUser);
        misisUserRepository.persistAndFlush(misisUser);
        return misisUser;
    }

    private void setUserInfo(final String email, final MisisUser misisUser) {
        final String username = String.format(DATABASE_USERNAME_FORMAT,
                userInfo.getString("given_name"),
                userInfo.getString("usual_name"));

        misisUser.setEmail(email);
        misisUser.setUsername(username);
    }
}
