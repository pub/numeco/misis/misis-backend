package fr.numeco.misis.security.service.impl;

import java.util.Objects;
import java.util.regex.Pattern;

import org.eclipse.microprofile.config.inject.ConfigProperty;

import fr.numeco.misis.security.Constants;
import io.quarkus.oidc.UserInfo;
import io.quarkus.security.identity.AuthenticationRequestContext;
import io.quarkus.security.identity.SecurityIdentity;
import io.quarkus.security.identity.SecurityIdentityAugmentor;
import io.quarkus.security.runtime.QuarkusSecurityIdentity;
import io.smallrye.mutiny.Uni;
import jakarta.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class EmailRoleAugmentor implements SecurityIdentityAugmentor {

    private static final String USER_INFO_ATTRIBUTE = "userinfo";
    @ConfigProperty(name = "gouv.email.pattern")
    protected Pattern gouvEmailPattern;

    @Override
    public Uni<SecurityIdentity> augment(SecurityIdentity identity, AuthenticationRequestContext context) {
        return Uni.createFrom().item(() -> {
            if (identity.isAnonymous()) {
                return identity;
            }

            final String email = getEmail(identity);
            if (email != null && gouvEmailPattern.matcher(email).matches()) {
                return QuarkusSecurityIdentity.builder(identity)
                        .addRole(Constants.SERVICE_PUBLIC_ROLE)
                        .build();
            }
            return identity;
        });

    }

    protected String getEmail(SecurityIdentity identity) {
        UserInfo userInfo = identity.getAttribute(USER_INFO_ATTRIBUTE);
        return Objects.nonNull(userInfo)
                ? userInfo.getEmail()
                : null;
    }

}
