package fr.numeco.misis.security.filter;

import io.quarkus.arc.Unremovable;
import io.quarkus.logging.Log;
import io.quarkus.oidc.common.OidcRequestFilter;
import jakarta.enterprise.context.ApplicationScoped;

@ApplicationScoped
@Unremovable
public class OidcClientRequestCustomFilter implements OidcRequestFilter {
    @Override
    public void filter(OidcRequestContext context) {
        Log.warn(context.request().method().name() + " - " + context.request().uri());
    }
}
