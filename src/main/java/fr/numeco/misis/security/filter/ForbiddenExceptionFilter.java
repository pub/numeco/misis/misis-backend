package fr.numeco.misis.security.filter;

import java.io.IOException;
import java.net.URI;

import org.eclipse.microprofile.config.inject.ConfigProperty;

import io.quarkus.oidc.OidcSession;
import jakarta.inject.Inject;
import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.container.ContainerResponseContext;
import jakarta.ws.rs.container.ContainerResponseFilter;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.Provider;

@Provider
public class ForbiddenExceptionFilter implements ContainerResponseFilter {

    @ConfigProperty(name = "quarkus.redirect.frontend.url")
    String redirectFrontendUrl;

    @Inject
    OidcSession oidcSession;

    @Override
    public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext) throws IOException {
        if (responseContext.getStatus() == Response.Status.FORBIDDEN.getStatusCode() && requestContext.getUriInfo().getPath().contains("callback")) {
            responseContext.setStatus(Response.Status.SEE_OTHER.getStatusCode());
            responseContext.getHeaders().add("Location", URI.create(redirectFrontendUrl).resolve("/forbidden"));
            oidcSession.logout().subscribe().with(unused -> {});
        }
    }
}
