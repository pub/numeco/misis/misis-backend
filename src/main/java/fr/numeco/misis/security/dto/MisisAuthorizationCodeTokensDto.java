package fr.numeco.misis.security.dto;

import fr.numeco.misis.legal.dto.LegalDocumentsDto;
import io.quarkus.oidc.AuthorizationCodeTokens;
import lombok.Getter;

@Getter
public class MisisAuthorizationCodeTokensDto extends AuthorizationCodeTokens {
    private final LegalDocumentsDto legalDocuments;//NOSONAR
    public MisisAuthorizationCodeTokensDto(String idToken, String accessToken, String refreshToken, Long accessTokenExpiresIn, LegalDocumentsDto legalDocumentsDto) {
        super(idToken, accessToken, refreshToken, accessTokenExpiresIn);
        this.legalDocuments = legalDocumentsDto;
    }

}
