package fr.numeco.misis.security.dto;

import lombok.Getter;

@Getter
public class UserInfosDto {
    private final String email;
    private final String username;

    public UserInfosDto(String email, String username) {
        this.email = email;
        this.username = username;
    }
}
