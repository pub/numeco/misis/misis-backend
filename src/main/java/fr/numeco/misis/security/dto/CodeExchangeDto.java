package fr.numeco.misis.security.dto;

import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class CodeExchangeDto {
    @NotBlank
    private String code; //NOSONAR
    @NotBlank
    private String nonce; //NOSONAR
}
