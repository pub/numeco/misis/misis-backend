package fr.numeco.misis.security.resource;

import java.net.URI;

import org.eclipse.microprofile.config.inject.ConfigProperty;

import fr.numeco.misis.security.Constants;
import fr.numeco.misis.security.dto.UserInfosDto;
import fr.numeco.misis.security.service.impl.DefaultAuthenticatedUserService;
import io.quarkus.oidc.UserInfo;
import jakarta.annotation.security.PermitAll;
import jakarta.annotation.security.RolesAllowed;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.SecurityContext;
import lombok.RequiredArgsConstructor;

@Path("/oauth2")
@RolesAllowed(Constants.SERVICE_PUBLIC_ROLE)
@RequiredArgsConstructor
public class AuthenticateResource {

    @ConfigProperty(name = "quarkus.redirect.frontend.url")
    String redirectFrontendUrl;

    private final UserInfo userInfo;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response login() {
        URI redirectUri = URI.create(redirectFrontendUrl);
        return Response.seeOther(redirectUri).build();
    }

    @GET
    @Path("/callback")
    public Response callback() {
        URI redirectUri = URI.create(redirectFrontendUrl);
        return Response.seeOther(redirectUri).build();
    }

    @GET
    @Path("/status")
    @PermitAll
    @Produces(MediaType.APPLICATION_JSON)
    public UserInfosDto getAuthStatus(@Context SecurityContext securityContext) {
        if (securityContext.getUserPrincipal() != null) {
            return new UserInfosDto(userInfo.getEmail(), String.format(DefaultAuthenticatedUserService.DATABASE_USERNAME_FORMAT, userInfo.getString("given_name"), userInfo.getString("usual_name")));
        } else {
            return null; // Non authentifié, HTTP 200 sans User
        }
    }

}
