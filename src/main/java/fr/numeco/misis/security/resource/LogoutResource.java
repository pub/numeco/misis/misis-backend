package fr.numeco.misis.security.resource;

import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;

import fr.numeco.misis.security.Constants;
import io.smallrye.mutiny.Uni;
import jakarta.annotation.security.RolesAllowed;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.HttpHeaders;

@Path("/logout")
@RolesAllowed(Constants.SERVICE_PUBLIC_ROLE)
public class LogoutResource {

    @GET
    @APIResponse(responseCode = "200", description = "logout successfully")
    @APIResponse(responseCode = "401", description = "Unauthorized")
    public Uni<Void> logout(HttpHeaders headers) {
        return Uni.createFrom().nullItem();
    }

}
