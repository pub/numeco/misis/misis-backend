package fr.numeco.analyser.service;

import fr.numeco.analyser.errors.MisisPageAnalyserException;
import fr.numeco.analyser.service.impl.FakeNetworkAnalyser;
import fr.numeco.misis.analyzer.dto.PageDto;
import fr.numeco.misis.enums.AnalyseStatut;
import io.quarkus.test.junit.QuarkusTest;
import io.smallrye.mutiny.helpers.test.AssertSubscriber;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.spy;

@QuarkusTest
class NetworkAnalyserTest {

    private static final Duration ANALYSIS_TIMEOUT = Duration.ofSeconds(30);
    private final FakeNetworkAnalyser networkAnalyser = spy(FakeNetworkAnalyser.class);

    @Test
    void shouldFailWhenPageWithoutUrlIsProvided() {
        // given
        List<PageDto> actual = new ArrayList<>();
        actual.add(new PageDto(null, null, null, null, null, null, null, null, AnalyseStatut.NOUVEAU));
        actual.add(new PageDto(null, "", null, null, null, null, null, null, AnalyseStatut.NOUVEAU));

        // when
        final AssertSubscriber<PageDto> subscriber = networkAnalyser.analyse(actual.stream())
                // then
                .subscribe().withSubscriber(AssertSubscriber.create(actual.size()));

        subscriber
                .awaitItems(0, ANALYSIS_TIMEOUT)
                .assertFailedWith(MisisPageAnalyserException.class)
        ;

        Mockito.verify(networkAnalyser, Mockito.never()).getRandomBytes();
        Mockito.verify(networkAnalyser, Mockito.never()).getRandomDomElementsNumber();
        Mockito.verify(networkAnalyser, Mockito.never()).getRandomHttpRequestsNumber();
    }

    @Test
    void shouldHaveDifferentAnalysedDate() {
        // given
        List<PageDto> actual = new ArrayList<>();
        actual.add(new PageDto(null, "www.sample.com", null, null, null, null, null, null, AnalyseStatut.NOUVEAU));
        actual.add(new PageDto(null, "www.sample.com", null, null, null, null, null, null, AnalyseStatut.NOUVEAU));
        // when

        final AssertSubscriber<PageDto> subscriber = networkAnalyser.analyse(actual.stream())
                // then
                .invoke(result -> assertThat(result.getAnalysedAt()).isNotNull())
                .invoke(result -> assertThat(result.getAnalysedAt()).isNotNull())
                .subscribe().withSubscriber(AssertSubscriber.create(actual.size()));

        subscriber
                .awaitItems(actual.size(), ANALYSIS_TIMEOUT)
                .assertCompleted()
                ;

        Mockito.verify(networkAnalyser, Mockito.times(4)).getRandomBytes();
        Mockito.verify(networkAnalyser, Mockito.times(2)).getRandomDomElementsNumber();
        Mockito.verify(networkAnalyser, Mockito.times(2)).getRandomHttpRequestsNumber();

    }

    @Test
    void shouldHavePosteriorAnalysedDate() {
        // given
        final List<PageDto> actual = new ArrayList<>();
        final LocalDateTime lastAnalyseDate = LocalDateTime.now().minusDays(7);
        final PageDto page = new PageDto(null, "www.sample.com", null, null, null, null, null, lastAnalyseDate, AnalyseStatut.NOUVEAU);

        actual.add(page);
        // when
        final AssertSubscriber<PageDto> subscriber = networkAnalyser.analyse(actual.stream())
                // then
                .invoke(result -> assertThat(result.getAnalysedAt()).isAfter(lastAnalyseDate))
                .subscribe().withSubscriber(AssertSubscriber.create(actual.size()));

        subscriber
                .awaitItems(actual.size(), ANALYSIS_TIMEOUT)
                .assertCompleted();

        Mockito.verify(networkAnalyser, Mockito.times(2)).getRandomBytes();
        Mockito.verify(networkAnalyser, Mockito.times(1)).getRandomDomElementsNumber();
        Mockito.verify(networkAnalyser, Mockito.times(1)).getRandomHttpRequestsNumber();

    }

    @Test
    void shouldNotHaveNullData() {
        // given
        List<PageDto> actual = new ArrayList<>();
        actual.add(new PageDto(null, "www.sample.com", null, null, null, null, null, null, AnalyseStatut.NOUVEAU));
        // when
        AssertSubscriber<PageDto> subscriber = networkAnalyser.analyse(actual.stream())
                .invoke(result -> assertThat(result.getDataTransferedInBytes()).isNotNull())
                .invoke(result -> assertThat(result.getDataDecodedInBytes()).isNotNull())
                .invoke(result -> assertThat(result.getDomElementsCount()).isNotNull())
                .invoke(result -> assertThat(result.getHttpRequestsCount()).isNotNull())
                .subscribe().withSubscriber(AssertSubscriber.create(actual.size()));

        subscriber
                .awaitItems(actual.size(), ANALYSIS_TIMEOUT)
                .assertCompleted();

        Mockito.verify(networkAnalyser, Mockito.times(2)).getRandomBytes();
        Mockito.verify(networkAnalyser, Mockito.times(1)).getRandomDomElementsNumber();
        Mockito.verify(networkAnalyser, Mockito.times(1)).getRandomHttpRequestsNumber();
    }
}
