package fr.numeco.analyser.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import fr.numeco.misis.enums.AnalyseStatut;
import jakarta.inject.Inject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import fr.numeco.analyser.AnalyzerConfig;
import fr.numeco.misis.analyzer.dto.PageDto;
import io.quarkus.test.InjectMock;
import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
class DynamicNetworkAnalyserTest {

    @InjectMock
    MisisNetworkAnalyser misisNetworkAnalyser;

    @InjectMock
    FakeNetworkAnalyser fakeNetworkAnalyser;

    @Inject
    AnalyzerConfig analyzerConfig;

    DynamicNetworkAnalyser dynamicNetworkAnalyser;

    @BeforeEach
    public void setup() {
        PageDto fakePageDto = new PageDto(null, "Fake", null, null, null, null, null, null, AnalyseStatut.NOUVEAU);
        PageDto misisPageDto = new PageDto(null, "Misis", null, null, null, null, null, null, AnalyseStatut.NOUVEAU);
        Mockito.when(fakeNetworkAnalyser.analyse(List.of())).thenReturn(Stream.of(fakePageDto));
        Mockito.when(misisNetworkAnalyser.analyse(List.of())).thenReturn(Stream.of(misisPageDto));
    }

    @Test
    void whenFakeConfigThenFakeAnalyserIsUsed() {
        Mockito.when(analyzerConfig.remoteUrl()).thenReturn(Optional.empty());
        // given
        dynamicNetworkAnalyser = new DynamicNetworkAnalyser(fakeNetworkAnalyser, misisNetworkAnalyser, analyzerConfig);

        // when
        PageDto page = dynamicNetworkAnalyser.analyse(List.of()).toList().get(0);

        // then
        assertEquals("Fake", page.getUrl());
        assertTrue(dynamicNetworkAnalyser.isFake());
    }

    @Test
    void whenNotFakeConfigThenMisisAnalyserIsUsed() {
        Mockito.when(analyzerConfig.remoteUrl()).thenReturn(Optional.of("fake-url"));

        // given
        dynamicNetworkAnalyser = new DynamicNetworkAnalyser(fakeNetworkAnalyser, misisNetworkAnalyser, analyzerConfig);

        // when
        PageDto page = dynamicNetworkAnalyser.analyse(List.of()).toList().get(0);

        // then
        assertEquals("Misis", page.getUrl());
        assertFalse(dynamicNetworkAnalyser.isFake());
    }

}
