package fr.numeco.analyser.service.impl;

import java.time.Duration;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.mockito.ArgumentMatchers.any;
import org.mockito.Mockito;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;
import org.openqa.selenium.remote.RemoteWebDriver;

import fr.numeco.analyser.AnalyzerConfig;
import fr.numeco.analyser.bean.ExtractedPageInfo;
import fr.numeco.analyser.errors.MisisPageAnalyserException;
import fr.numeco.misis.analyzer.dto.PageDto;
import io.quarkus.test.junit.QuarkusTest;
import io.smallrye.mutiny.helpers.test.AssertSubscriber;
import io.smallrye.mutiny.tuples.Tuple2;
import jakarta.inject.Inject;

@QuarkusTest
class MisisNetworkAnalyserTest {

    final Duration timeout = Duration.ofSeconds(20);
    @Inject
    AnalyzerConfig analyzerConfig;
    MisisWebDriverService driverService;
    MisisNetworkAnalyser analyser;

    @BeforeEach
    public void setUp() throws Exception {
        driverService = mock(MisisWebDriverService.class);
        analyser = new MisisNetworkAnalyser(driverService, analyzerConfig);

        PageDto pageDto = mock(PageDto.class);
        Mockito.when(pageDto.getUrl()).thenReturn("www.sample.fr");

        Tuple2<PageDto, ExtractedPageInfo> mockedTuple = mock(Tuple2.class);
        when(mockedTuple.getItem1()).thenReturn(pageDto);
        when(mockedTuple.getItem2()).thenReturn(mock(ExtractedPageInfo.class));

        when(driverService.extractInformation(any())).thenReturn(mockedTuple);
    }

    @Test
    void shouldReturnEmptyResponseWhenNullIsProvided() {
        final AssertSubscriber<PageDto> subscriber = analyser.analyse((Stream<PageDto>) null)
                .subscribe().withSubscriber(AssertSubscriber.create());

        subscriber
                .awaitItems(0, timeout)
                .assertCompleted();

        Mockito.verify(driverService, never()).createNewTuple(any(PageDto.class));
        Mockito.verify(driverService, never()).extractInformation(any());
    }

    @Test
    void shouldReturnAnalysedPageWhenListOfDtoIsProvided() {
        final List<PageDto> data = Arrays.asList(
                createPageMockedWithUrl("www.sample-dont-exist.fr"),
                createPageMockedWithUrl("www.sample-dont-really-exist.fr"),
                createPageMockedWithUrl("www.an-other-sample.fr")
        );
        final int expected = data.size();

        final AssertSubscriber<PageDto> subscriber = analyser.analyse(data.stream())
                .subscribe().withSubscriber(AssertSubscriber.create(data.size()));

        subscriber
                .awaitItems(expected, timeout)
                .assertCompleted();

        Mockito.verify(driverService, times(data.size())).createNewTuple(any(PageDto.class));
        Mockito.verify(driverService, times(data.size())).extractInformation(any());

    }

    @Test
    void shouldIgnorePageWhenEmptyUrlIsProvided() {
        final List<PageDto> expected = Arrays.asList(
                createPageMockedWithUrl("www.sample-dont-exist.fr"),
                createPageMockedWithUrl("www.sample-dont-really-exist.fr")
        );

        final List<PageDto> data = Arrays.asList(
                createPageMockedWithUrl(null),
                createPageMockedWithUrl(" "),
                createPageMockedWithUrl("")
        );

        data.forEach(pageDto -> {
            when(driverService.extractInformation(Tuple2.of(mock(RemoteWebDriver.class), pageDto))).thenThrow(MisisPageAnalyserException.class);
        });


        final AssertSubscriber<PageDto> subscriberExpect = analyser.analyse(Stream.concat(expected.stream(), data.stream()))
                .subscribe().withSubscriber(AssertSubscriber.create(data.size()+expected.size()));

        subscriberExpect
                .awaitItems(expected.size(), timeout)
                .assertCompleted();

        Mockito.verify(driverService, times(expected.size() + data.size())).createNewTuple(any(PageDto.class));
        Mockito.verify(driverService, times(expected.size() + data.size())).extractInformation(any());

    }

    @Test
    void shouldRequestPageWhenUrlIsProvided() {
        final List<PageDto> data = Arrays.asList(
                createPageMockedWithUrl("www.sample-dont-exist.fr"),
                createPageMockedWithUrl("www.sample-dont-really-exist.fr")
        );

        final AssertSubscriber<PageDto> subscriberExpect = analyser.analyse(data.stream())
                .subscribe().withSubscriber(AssertSubscriber.create(data.size()));

        subscriberExpect
                .awaitItems(data.size(), timeout)
                .assertCompleted();

        Mockito.verify(driverService, times(data.size())).createNewTuple(any(PageDto.class));
        Mockito.verify(driverService, times(data.size())).extractInformation(any());
    }


    private PageDto createPageMockedWithUrl(String url) {
        PageDto pageDto = mock(PageDto.class);
        Mockito.when(pageDto.getUrl()).thenReturn(url);
        return pageDto;
    }
}