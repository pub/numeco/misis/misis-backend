package fr.numeco.analyser;

import io.quarkus.test.Mock;
import io.smallrye.config.SmallRyeConfig;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.inject.Produces;
import jakarta.inject.Inject;
import org.eclipse.microprofile.config.Config;

import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

public class AnalyzerConfigProducer {
    @Inject
    Config config;

    @Produces
    @ApplicationScoped
    @Mock
    AnalyzerConfig analyzerConfig() {
        AnalyzerConfig analyzerConfig = config.unwrap(SmallRyeConfig.class).getConfigMapping(AnalyzerConfig.class);
        AnalyzerConfig analyzerConfigSpy = spy(analyzerConfig);
        AnalyzerConfig.Trace traceSpy = spy(analyzerConfig.trace());
        when(analyzerConfigSpy.trace()).thenReturn(traceSpy);

        return analyzerConfigSpy;
    }
}
