package fr.numeco.misis;

import fr.numeco.misis.security.service.impl.EmailRoleAugmentor;
import io.quarkus.security.identity.SecurityIdentity;
import io.quarkus.security.runtime.QuarkusPrincipal;
import jakarta.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class TestEmailRoleAugmentor extends EmailRoleAugmentor {

    @Override
    protected String getEmail(SecurityIdentity identity) {
        return identity.getPrincipal() instanceof QuarkusPrincipal principal
                ? principal.getName()
                : null;
    }

}
