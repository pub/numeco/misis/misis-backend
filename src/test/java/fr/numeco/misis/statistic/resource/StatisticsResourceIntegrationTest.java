package fr.numeco.misis.statistic.resource;

import java.util.Map;
import java.util.regex.Matcher;

import static org.hamcrest.CoreMatchers.is;
import org.junit.jupiter.api.Test;

import fr.numeco.misis.TestEmailRoleAugmentor;
import fr.numeco.misis.enums.StatisticType;
import io.quarkus.test.common.http.TestHTTPEndpoint;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import static io.restassured.RestAssured.given;

@QuarkusTest
@TestHTTPEndpoint(StatisticsResource.class)
class StatisticsResourceIntegrationTest {

    protected final String getStatisticsType = "/{statType}";

    @Test
    @TestSecurity(user = "admin@test.fr", augmentors=TestEmailRoleAugmentor.class)
    void get_shouldReturnDomElementsStatisticsWhenSuiviIdGroupIdAndStatTypeIsProvided() {
        given()
                .pathParams(buildSuiviGroupIdsAndTypePathParam(1L, 1L, StatisticType.DOM_ELEMENTS))
                .get(getStatisticsType)
                .then()
                .statusCode(200)
                .body(is(Matcher.quoteReplacement(
                        "{\"labels\":[\"2019-06-01\",\"2020-07-02\",\"2021-08-03\",\"2022-09-04\",\"2023-10-05\"],\"datasets\":[{\"label\":\"Maximum\",\"data\":[95000,100000,95000,95000,100000]},{\"label\":\"Minimum\",\"data\":[60000,55000,50000,55000,60000]},{\"label\":\"Médiane\",\"data\":[75000,80000,70000,75000,80000]}]}")));
    }

    @Test
    @TestSecurity(user = "admin@test.fr", augmentors=TestEmailRoleAugmentor.class)
    void get_shouldReturnNotFoundWhenNotExistingSuiviIdIsProvided() {
        given()
                .pathParams(buildSuiviGroupIdsAndTypePathParam(-1L, 1L, StatisticType.DOM_ELEMENTS))
                .get(getStatisticsType)
                .then()
                .statusCode(404)
                .body("status", is("NOT_FOUND"))
                .body("message", is("Entity with id -1 not found"));
    }

    @Test
    @TestSecurity(user = "admin@test.fr", augmentors=TestEmailRoleAugmentor.class)
    void get_shouldReturnEmptyResponseWhenNotExistingGroupIdIsProvided() {
        given()
                .pathParams(buildSuiviGroupIdsAndTypePathParam(1L, -1L, StatisticType.DOM_ELEMENTS))
                .get(getStatisticsType)
                .then()
                .statusCode(200)
                .body(is(Matcher.quoteReplacement("{\"labels\":[],\"datasets\":[]}")));
    }

    @Test
    @TestSecurity(user = "admin@test.fr", augmentors=TestEmailRoleAugmentor.class)
    void get_shouldReturnNotFoundResponseWhenNotExistingStatTypeIsProvided() {
        given()
                .pathParams(buildSuiviGroupIdsAndTypePathParam(1L, 1L, "NOT_EXIST"))
                .get(getStatisticsType)
                .then()
                .statusCode(404)
                .body("status", is("NOT_FOUND"))
                .body("message", is("HTTP 404 Not Found"));
    }

    private Map<String, ?> buildSuiviGroupIdsAndTypePathParam(final Long suiviId, final Long groupId,
            final StatisticType type) {
        return buildSuiviGroupIdsAndTypePathParam(suiviId, groupId, type.name());
    }

    private Map<String, ?> buildSuiviGroupIdsAndTypePathParam(final Long suiviId, final Long groupId,
            final String statisticType) {
        return Map.of(
                "suiviId", suiviId,
                "groupId", groupId,
                "statType", statisticType);
    }

}