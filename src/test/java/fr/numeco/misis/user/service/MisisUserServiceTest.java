package fr.numeco.misis.user.service;

import fr.numeco.misis.user.domain.MisisUser;
import fr.numeco.misis.user.repository.MisisUserRepository;
import fr.numeco.misis.error.EntityNotFound;
import fr.numeco.misis.user.dto.MisisUserDto;
import io.quarkus.test.InjectMock;
import io.quarkus.test.junit.QuarkusTest;
import jakarta.inject.Inject;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.*;

@QuarkusTest
class MisisUserServiceTest {

    @Inject
    MisisUserService misisUserService;

    @InjectMock
    MisisUserRepository misisUserRepository;

    @Test
    void getUsers() {
        // given
        List<MisisUser> users = List.of(new MisisUser(), new MisisUser());
        when(misisUserRepository.listAll()).thenReturn(users);

        // when
        List<MisisUserDto> result = misisUserService.getAll();

        // then
        verify(misisUserRepository).listAll();
        assertThat(result).hasSize(2);
    }

    @Test
    void shouldThrowEntityNotFoundWhenUserNotFound() {
        // given
        Long id = 1L;
        when(misisUserRepository.findByIdOptional(id)).thenReturn(Optional.empty());

        // when
        // then
        assertThatThrownBy(() -> misisUserService.getById(id))
                .isInstanceOf(EntityNotFound.class)
                .hasMessage("Entity with id " + id + " not found");
    }

    @Test
    void getUsersByEmailStartWith_shouldReturnEmptyWhenNullIsProvided() {
        final String given = null;
        final List<MisisUserDto> excepted = Collections.emptyList();
        final List<MisisUserDto> actual = misisUserService.getUsersByEmailStartWith(given);

        assertThat(actual).hasSize(excepted.size());
    }

    @Test
    void getUsersByEmailStartWith_shouldReturnEmptyWhenBlankIsProvided() {
        final String given = "                ";
        final List<MisisUserDto> excepted = Collections.emptyList();
        final List<MisisUserDto> actual = misisUserService.getUsersByEmailStartWith(given);

        assertThat(actual).hasSize(excepted.size());
    }

    @Test
    void getUsersByEmailStartWith_shouldSearchEmailWhenThreeCharactersIsProvided() {
        final String given = "lov";
        misisUserService.getUsersByEmailStartWith(given);

        verify(misisUserRepository, times(1)).searchByEmailStartWith(given);
    }
}
