package fr.numeco.misis.suividesite.resource;

import java.util.regex.Matcher;

import static org.hamcrest.CoreMatchers.is;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import fr.numeco.misis.TestEmailRoleAugmentor;
import io.quarkus.oidc.UserInfo;
import io.quarkus.test.InjectMock;
import io.quarkus.test.common.http.TestHTTPEndpoint;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import static io.restassured.RestAssured.given;

@QuarkusTest
@TestHTTPEndpoint(WebsiteSettingsRessource.class)
class WebsiteSettingsDtoRessourceIntegrationTest {

    private final String suiviId = "/{suiviId}";

    @InjectMock
    UserInfo userInfo;

    @Test
    @TestSecurity(user = "admin@test.fr", augmentors=TestEmailRoleAugmentor.class)
    void getSettings_shouldReturnSettingsWhenSuiviIdIsProvided() {
        Mockito.when(userInfo.getEmail()).thenReturn("admin@test.fr");
        Mockito.when(userInfo.getString("given_name")).thenReturn("given_name");
        Mockito.when(userInfo.getString("usual_name")).thenReturn("usual_name");

        given()
                .pathParams("suiviId", 1L)
                .get(suiviId)
                .then()
                .statusCode(200)
                .body(is(Matcher.quoteReplacement("{\"users\":[{\"id\":1,\"name\":\"admin\",\"email\":\"admin@test.fr\",\"admin\":true}],\"groupOfPagesFormDtos\":[{\"id\":1,\"name\":\"Groupe n°1\",\"periodiciteDuSuivi\":\"QUOTIDIEN\",\"methodeDeCreationDeGroupe\":\"MANUELLE\",\"pages\":[{\"id\":1,\"url\":\"https://www.site-web-d-exemple-inexistant.fr/page1_1\"},{\"id\":2,\"url\":\"https://www.site-web-d-exemple-inexistant.fr/page2_1\"},{\"id\":3,\"url\":\"https://www.site-web-d-exemple-inexistant.fr/page3_1\"},{\"id\":4,\"url\":\"https://www.site-web-d-exemple-inexistant.fr/page4_1\"},{\"id\":5,\"url\":\"https://www.site-web-d-exemple-inexistant.fr/page5_1\"},{\"id\":6,\"url\":\"https://www.site-web-d-exemple-inexistant.fr/page6_1\"},{\"id\":7,\"url\":\"https://www.site-web-d-exemple-inexistant.fr/page7_1\"},{\"id\":8,\"url\":\"https://www.site-web-d-exemple-inexistant.fr/page8_1\"},{\"id\":9,\"url\":\"https://www.site-web-d-exemple-inexistant.fr/page9_1\"},{\"id\":10,\"url\":\"https://www.site-web-d-exemple-inexistant.fr/page10_1\"},{\"id\":11,\"url\":\"https://www.site-web-d-exemple-inexistant.fr/page11_1\"},{\"id\":12,\"url\":\"https://www.site-web-d-exemple-inexistant.fr/page12_1\"},{\"id\":13,\"url\":\"https://www.site-web-d-exemple-inexistant.fr/page13_1\"},{\"id\":14,\"url\":\"https://www.site-web-d-exemple-inexistant.fr/page14_1\"},{\"id\":15,\"url\":\"https://www.site-web-d-exemple-inexistant.fr/page15_1\"},{\"id\":16,\"url\":\"https://www.site-web-d-exemple-inexistant.fr/page16_1\"},{\"id\":17,\"url\":\"https://www.site-web-d-exemple-inexistant.fr/page17_1\"},{\"id\":18,\"url\":\"https://www.site-web-d-exemple-inexistant.fr/page18_1\"},{\"id\":19,\"url\":\"https://www.site-web-d-exemple-inexistant.fr/page19_1\"},{\"id\":20,\"url\":\"https://www.site-web-d-exemple-inexistant.fr/page20_1\"}]},{\"id\":51,\"name\":\"Groupe n°2\",\"periodiciteDuSuivi\":\"QUOTIDIEN\",\"methodeDeCreationDeGroupe\":\"MANUELLE\",\"pages\":[{\"id\":21,\"url\":\"https://www.site-web-d-exemple-inexistant.fr/page1_2\"},{\"id\":22,\"url\":\"https://www.site-web-d-exemple-inexistant.fr/page2_2\"},{\"id\":23,\"url\":\"https://www.site-web-d-exemple-inexistant.fr/page3_2\"},{\"id\":24,\"url\":\"https://www.site-web-d-exemple-inexistant.fr/page4_2\"},{\"id\":25,\"url\":\"https://www.site-web-d-exemple-inexistant.fr/page5_2\"},{\"id\":26,\"url\":\"https://www.site-web-d-exemple-inexistant.fr/page6_2\"},{\"id\":27,\"url\":\"https://www.site-web-d-exemple-inexistant.fr/page7_2\"},{\"id\":28,\"url\":\"https://www.site-web-d-exemple-inexistant.fr/page8_2\"},{\"id\":29,\"url\":\"https://www.site-web-d-exemple-inexistant.fr/page9_2\"},{\"id\":30,\"url\":\"https://www.site-web-d-exemple-inexistant.fr/page10_2\"},{\"id\":31,\"url\":\"https://www.site-web-d-exemple-inexistant.fr/page11_2\"},{\"id\":32,\"url\":\"https://www.site-web-d-exemple-inexistant.fr/page12_2\"},{\"id\":33,\"url\":\"https://www.site-web-d-exemple-inexistant.fr/page13_2\"},{\"id\":34,\"url\":\"https://www.site-web-d-exemple-inexistant.fr/page14_2\"},{\"id\":35,\"url\":\"https://www.site-web-d-exemple-inexistant.fr/page15_2\"},{\"id\":36,\"url\":\"https://www.site-web-d-exemple-inexistant.fr/page16_2\"},{\"id\":37,\"url\":\"https://www.site-web-d-exemple-inexistant.fr/page17_2\"},{\"id\":38,\"url\":\"https://www.site-web-d-exemple-inexistant.fr/page18_2\"},{\"id\":39,\"url\":\"https://www.site-web-d-exemple-inexistant.fr/page19_2\"},{\"id\":40,\"url\":\"https://www.site-web-d-exemple-inexistant.fr/page20_2\"}]},{\"id\":101,\"name\":\"Groupe n°3\",\"periodiciteDuSuivi\":\"QUOTIDIEN\",\"methodeDeCreationDeGroupe\":\"MANUELLE\",\"pages\":[{\"id\":41,\"url\":\"https://www.site-web-d-exemple-inexistant.fr/page1_3\"},{\"id\":42,\"url\":\"https://www.site-web-d-exemple-inexistant.fr/page2_3\"},{\"id\":43,\"url\":\"https://www.site-web-d-exemple-inexistant.fr/page3_3\"},{\"id\":44,\"url\":\"https://www.site-web-d-exemple-inexistant.fr/page4_3\"},{\"id\":45,\"url\":\"https://www.site-web-d-exemple-inexistant.fr/page5_3\"},{\"id\":46,\"url\":\"https://www.site-web-d-exemple-inexistant.fr/page6_3\"},{\"id\":47,\"url\":\"https://www.site-web-d-exemple-inexistant.fr/page7_3\"},{\"id\":48,\"url\":\"https://www.site-web-d-exemple-inexistant.fr/page8_3\"},{\"id\":49,\"url\":\"https://www.site-web-d-exemple-inexistant.fr/page9_3\"},{\"id\":50,\"url\":\"https://www.site-web-d-exemple-inexistant.fr/page10_3\"},{\"id\":51,\"url\":\"https://www.site-web-d-exemple-inexistant.fr/page11_3\"},{\"id\":52,\"url\":\"https://www.site-web-d-exemple-inexistant.fr/page12_3\"},{\"id\":53,\"url\":\"https://www.site-web-d-exemple-inexistant.fr/page13_3\"},{\"id\":54,\"url\":\"https://www.site-web-d-exemple-inexistant.fr/page14_3\"},{\"id\":55,\"url\":\"https://www.site-web-d-exemple-inexistant.fr/page15_3\"},{\"id\":56,\"url\":\"https://www.site-web-d-exemple-inexistant.fr/page16_3\"},{\"id\":57,\"url\":\"https://www.site-web-d-exemple-inexistant.fr/page17_3\"},{\"id\":58,\"url\":\"https://www.site-web-d-exemple-inexistant.fr/page18_3\"},{\"id\":59,\"url\":\"https://www.site-web-d-exemple-inexistant.fr/page19_3\"},{\"id\":60,\"url\":\"https://www.site-web-d-exemple-inexistant.fr/page20_3\"}]}]}")));
    }

    @Test
    @TestSecurity(user = "admin@test.fr", augmentors=TestEmailRoleAugmentor.class)
    void post_shouldSaveSharingUsersWhenFormIsProvided() {
        Mockito.when(userInfo.getEmail()).thenReturn("admin@test.fr");
        Mockito.when(userInfo.getString("given_name")).thenReturn("given_name");
        Mockito.when(userInfo.getString("usual_name")).thenReturn("usual_name");

        given()
                .header("Content-Type", "application/json")
                .body(Matcher.quoteReplacement("{\"suivisId\":[1],\"usersEmail\":[\"user2@test.fr\"]}"))
                .post()
                .then()
                .statusCode(204)
                .body(is(""));
    }

    @Test
    @TestSecurity(user = "admin@test.fr", augmentors=TestEmailRoleAugmentor.class)
    void post_shouldUpdateSuiviWhenFormIsProvided() {
        Mockito.when(userInfo.getEmail()).thenReturn("admin@test.fr");
        Mockito.when(userInfo.getString("given_name")).thenReturn("given_name");
        Mockito.when(userInfo.getString("usual_name")).thenReturn("usual_name");

        given()
                .pathParams("suiviId", 1L)
                .header("Content-Type", "application/json")
                .body("""
                        [
                          {
                            "id": 1,
                            "name": "EDITED FRONT API",
                            "periodiciteDuSuivi": "QUOTIDIEN",
                            "methodeDeCreationDeGroupe": "MANUELLE",
                            "pages": [
                              {
                                "id": 1,
                                "url": "https://accenture.fr/edited"
                              }
                            ]
                          }
                        ]
                        """)
                .post(suiviId)
                .then()
                .statusCode(204)
                .body(is(""));
    }

    @Test
    @TestSecurity(user = "admin@test.fr", augmentors=TestEmailRoleAugmentor.class)
    void delete_shouldDeleteSuiviDeSiteWhenIdIsProvided() {
        Mockito.when(userInfo.getEmail()).thenReturn("admin@test.fr");
        Mockito.when(userInfo.getString("given_name")).thenReturn("given_name");
        Mockito.when(userInfo.getString("usual_name")).thenReturn("usual_name");

        given()
                .pathParams("suiviId", 2L)
                .delete(suiviId)
                .then()
                .statusCode(204)
                .body(is(""));
    }

}
