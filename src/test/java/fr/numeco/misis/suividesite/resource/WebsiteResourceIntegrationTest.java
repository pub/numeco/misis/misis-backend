package fr.numeco.misis.suividesite.resource;

import java.util.regex.Matcher;

import static org.hamcrest.CoreMatchers.is;
import static org.jboss.resteasy.reactive.RestResponse.StatusCode.CREATED;
import org.junit.jupiter.api.Test;

import fr.numeco.misis.TestEmailRoleAugmentor;
import io.quarkus.test.common.http.TestHTTPEndpoint;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;

@QuarkusTest
@TestHTTPEndpoint(WebsiteResource.class)
class WebsiteResourceIntegrationTest {

    protected final String existingWebsite = "/{suiviId}";

    @Test
    @TestSecurity(user = "admin@test.fr", augmentors = TestEmailRoleAugmentor.class)
    void get_shouldReturnArrayOfWebsiteWhenUserIsAuthenticated() {
        when().get()
                .then()
                .statusCode(200)
                .body(is(Matcher.quoteReplacement(
                        "[{\"id\":1,\"name\":\"Site d'exemple\",\"url\":\"https://www.site-web-d-exemple-inexistant.fr/\",\"lastAnalysis\":\"2000-01-02T00:00:00\",\"nextAnalysis\":\"2000-01-01T01:00:00\",\"groups\":{\"1\":{\"name\":\"Groupe n°1\",\"availableRessourceTypes\":[\"IMAGE\",\"PDF\"],\"alert\":null},\"51\":{\"name\":\"Groupe n°2\",\"availableRessourceTypes\":[],\"alert\":null},\"101\":{\"name\":\"Groupe n°3\",\"availableRessourceTypes\":[],\"alert\":null}},\"pageCount\":60,\"alert\":null},{\"id\":101,\"name\":\"Gitlab Page\",\"url\":\"https://misis-test-website-pub-numeco-misis-319bda392a27d3f07057d9125aa.gitlab-pages.din.developpement-durable.gouv.fr/\",\"lastAnalysis\":null,\"nextAnalysis\":\"2000-01-01T01:00:00\",\"groups\":{\"201\":{\"name\":\"Mes Gitlab Pages\",\"availableRessourceTypes\":[],\"alert\":null}},\"pageCount\":4,\"alert\":null},{\"id\":151,\"name\":\"Test API\",\"url\":\"https://accenture.fr\",\"lastAnalysis\":null,\"nextAnalysis\":\"2000-01-01T01:00:00\",\"groups\":{\"251\":{\"name\":\"Groupe API 1\",\"availableRessourceTypes\":[],\"alert\":null}},\"pageCount\":1,\"alert\":null},{\"id\":51,\"name\":\"Site partagé par John Doe (un@test.fr)\",\"url\":\"https://www.john-doe-site-web.fr/\",\"lastAnalysis\":\"2000-01-02T00:00:00\",\"nextAnalysis\":\"2000-01-01T01:00:00\",\"groups\":{\"151\":{\"name\":\"Default\",\"availableRessourceTypes\":[],\"alert\":null}},\"pageCount\":1,\"alert\":null}]")));
    }

    @Test
    @TestSecurity(user = "created@test.fr", augmentors=TestEmailRoleAugmentor.class)
    void 
    post_shouldCreateWebsiteWhenCompleteFormIsProvided() {
        final String requestBodyWithForm = """
        {
          "name": "Test API",
          "url": "https://accenture.fr",
          "groupOfPages": [
            {
              "name": "Groupe API 1",
              "periodiciteDuSuivi": "QUOTIDIEN",
              "methodeDeCreationDeGroupe": "MANUELLE",
              "pages": [
                {
                  "url": "https://accenture.com"
                }
              ]
            }
          ]
        }""";

        given()
                .header("Content-Type", "application/json")
                .body(requestBodyWithForm)
                .when().post()
                .then()
                .statusCode(CREATED)
                .body(is(""));
    }

    @Test
    @TestSecurity(user = "admin@test.fr", augmentors = TestEmailRoleAugmentor.class)
    void get_shouldReturnWebsiteWhenExistingIdIsProvided() {
        given()
                .pathParams("suiviId", 1)
                .get(existingWebsite)
                .then()
                .statusCode(200)
                .body(is(Matcher.quoteReplacement(
                        "{\"id\":1,\"name\":\"Site d'exemple\",\"url\":\"https://www.site-web-d-exemple-inexistant.fr/\",\"lastAnalysis\":\"2000-01-02T00:00:00\",\"nextAnalysis\":\"2000-01-01T01:00:00\",\"groups\":{\"1\":{\"name\":\"Groupe n°1\",\"availableRessourceTypes\":[\"IMAGE\",\"PDF\"],\"alert\":null},\"51\":{\"name\":\"Groupe n°2\",\"availableRessourceTypes\":[],\"alert\":null},\"101\":{\"name\":\"Groupe n°3\",\"availableRessourceTypes\":[],\"alert\":null}},\"pageCount\":60,\"alert\":null}")));
    }

    @Test
    @TestSecurity(user = "admin@test.fr", augmentors = TestEmailRoleAugmentor.class)
    void get_shouldReturnNotFoundWhenNotExistingIdIsProvided() {
        given()
                .pathParams("suiviId", -1)
                .get(existingWebsite)
                .then()
                .statusCode(404)
                .body("status", is("NOT_FOUND"))
                .body("message", is("Entity with id -1 not found"));
    }

}
