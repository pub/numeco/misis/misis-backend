package fr.numeco.misis.suividesite.resource;

import java.util.Map;
import java.util.regex.Matcher;

import static org.hamcrest.CoreMatchers.is;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import fr.numeco.misis.TestEmailRoleAugmentor;
import io.quarkus.oidc.UserInfo;
import io.quarkus.test.InjectMock;
import io.quarkus.test.common.http.TestHTTPEndpoint;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import static io.restassured.RestAssured.given;

@QuarkusTest
@TestHTTPEndpoint(WebsiteDetailsResource.class)
class WebsiteDetailsResourceIntegrationTest {

    protected final String getWebsitePagesDetails = "/liste-des-pages";
    protected final String getWebsiteResourcesDetails = "/liste-des-ressources";
    @InjectMock
    private UserInfo userInfo;

    @Test
    @TestSecurity(user = "admin@test.fr", augmentors=TestEmailRoleAugmentor.class)
    void getPages_shouldReturnWebsitePagesDetailsWhenSuiviIdAndGroupIdIsProvided() {
        Mockito.when(userInfo.getEmail()).thenReturn("admin@test.fr");
        Mockito.when(userInfo.getString("given_name")).thenReturn("given_name");
        Mockito.when(userInfo.getString("usual_name")).thenReturn("usual_name");

        given()
                .pathParams(buildSuiviAndGroupIdsPathParam(1L, 1L))
                .get(getWebsitePagesDetails)
                .then()
                .statusCode(200)
                .body(is(Matcher.quoteReplacement(
                        "{\"websiteName\":\"Site d'exemple\",\"pageCount\":60,\"groups\":{\"1\":{\"name\":\"Groupe n°1\",\"availableRessourceTypes\":[\"IMAGE\",\"PDF\"],\"alert\":null},\"51\":{\"name\":\"Groupe n°2\",\"availableRessourceTypes\":[],\"alert\":null},\"101\":{\"name\":\"Groupe n°3\",\"availableRessourceTypes\":[],\"alert\":null}},\"pageDetailDtos\":[{\"url\":\"https://www.site-web-d-exemple-inexistant.fr/page1_1\",\"title\":null,\"dataTransferedInBytes\":135500030,\"dataDecodedInBytes\":90500020,\"httpRequestsCount\":245,\"domElementsCount\":9110,\"analyseStatut\":\"TERMINEE\"},{\"url\":\"https://www.site-web-d-exemple-inexistant.fr/page2_1\",\"title\":null,\"dataTransferedInBytes\":135500030,\"dataDecodedInBytes\":90500020,\"httpRequestsCount\":245,\"domElementsCount\":9110,\"analyseStatut\":\"TERMINEE\"},{\"url\":\"https://www.site-web-d-exemple-inexistant.fr/page3_1\",\"title\":null,\"dataTransferedInBytes\":135500030,\"dataDecodedInBytes\":90500020,\"httpRequestsCount\":245,\"domElementsCount\":9110,\"analyseStatut\":\"TERMINEE\"},{\"url\":\"https://www.site-web-d-exemple-inexistant.fr/page4_1\",\"title\":null,\"dataTransferedInBytes\":135500030,\"dataDecodedInBytes\":90500020,\"httpRequestsCount\":245,\"domElementsCount\":9110,\"analyseStatut\":\"TERMINEE\"},{\"url\":\"https://www.site-web-d-exemple-inexistant.fr/page5_1\",\"title\":null,\"dataTransferedInBytes\":135500030,\"dataDecodedInBytes\":90500020,\"httpRequestsCount\":245,\"domElementsCount\":9110,\"analyseStatut\":\"TERMINEE\"},{\"url\":\"https://www.site-web-d-exemple-inexistant.fr/page6_1\",\"title\":null,\"dataTransferedInBytes\":135500030,\"dataDecodedInBytes\":90500020,\"httpRequestsCount\":245,\"domElementsCount\":9110,\"analyseStatut\":\"TERMINEE\"},{\"url\":\"https://www.site-web-d-exemple-inexistant.fr/page7_1\",\"title\":null,\"dataTransferedInBytes\":135500030,\"dataDecodedInBytes\":90500020,\"httpRequestsCount\":245,\"domElementsCount\":9110,\"analyseStatut\":\"TERMINEE\"},{\"url\":\"https://www.site-web-d-exemple-inexistant.fr/page8_1\",\"title\":null,\"dataTransferedInBytes\":135500030,\"dataDecodedInBytes\":90500020,\"httpRequestsCount\":245,\"domElementsCount\":9110,\"analyseStatut\":\"TERMINEE\"},{\"url\":\"https://www.site-web-d-exemple-inexistant.fr/page9_1\",\"title\":null,\"dataTransferedInBytes\":135500030,\"dataDecodedInBytes\":90500020,\"httpRequestsCount\":245,\"domElementsCount\":9110,\"analyseStatut\":\"TERMINEE\"},{\"url\":\"https://www.site-web-d-exemple-inexistant.fr/page10_1\",\"title\":null,\"dataTransferedInBytes\":135500030,\"dataDecodedInBytes\":90500020,\"httpRequestsCount\":245,\"domElementsCount\":9110,\"analyseStatut\":\"TERMINEE\"},{\"url\":\"https://www.site-web-d-exemple-inexistant.fr/page11_1\",\"title\":null,\"dataTransferedInBytes\":135500030,\"dataDecodedInBytes\":90500020,\"httpRequestsCount\":245,\"domElementsCount\":9110,\"analyseStatut\":\"TERMINEE\"},{\"url\":\"https://www.site-web-d-exemple-inexistant.fr/page12_1\",\"title\":null,\"dataTransferedInBytes\":135500030,\"dataDecodedInBytes\":90500020,\"httpRequestsCount\":245,\"domElementsCount\":9110,\"analyseStatut\":\"TERMINEE\"},{\"url\":\"https://www.site-web-d-exemple-inexistant.fr/page13_1\",\"title\":null,\"dataTransferedInBytes\":135500030,\"dataDecodedInBytes\":90500020,\"httpRequestsCount\":245,\"domElementsCount\":9110,\"analyseStatut\":\"TERMINEE\"},{\"url\":\"https://www.site-web-d-exemple-inexistant.fr/page14_1\",\"title\":null,\"dataTransferedInBytes\":135500030,\"dataDecodedInBytes\":90500020,\"httpRequestsCount\":245,\"domElementsCount\":9110,\"analyseStatut\":\"TERMINEE\"},{\"url\":\"https://www.site-web-d-exemple-inexistant.fr/page15_1\",\"title\":null,\"dataTransferedInBytes\":135500030,\"dataDecodedInBytes\":90500020,\"httpRequestsCount\":245,\"domElementsCount\":9110,\"analyseStatut\":\"TERMINEE\"},{\"url\":\"https://www.site-web-d-exemple-inexistant.fr/page16_1\",\"title\":null,\"dataTransferedInBytes\":135500030,\"dataDecodedInBytes\":90500020,\"httpRequestsCount\":245,\"domElementsCount\":9110,\"analyseStatut\":\"TERMINEE\"},{\"url\":\"https://www.site-web-d-exemple-inexistant.fr/page17_1\",\"title\":null,\"dataTransferedInBytes\":135500030,\"dataDecodedInBytes\":90500020,\"httpRequestsCount\":245,\"domElementsCount\":9110,\"analyseStatut\":\"TERMINEE\"},{\"url\":\"https://www.site-web-d-exemple-inexistant.fr/page18_1\",\"title\":null,\"dataTransferedInBytes\":135500030,\"dataDecodedInBytes\":90500020,\"httpRequestsCount\":245,\"domElementsCount\":9110,\"analyseStatut\":\"TERMINEE\"},{\"url\":\"https://www.site-web-d-exemple-inexistant.fr/page19_1\",\"title\":null,\"dataTransferedInBytes\":135500030,\"dataDecodedInBytes\":90500020,\"httpRequestsCount\":245,\"domElementsCount\":9110,\"analyseStatut\":\"TERMINEE\"},{\"url\":\"https://www.site-web-d-exemple-inexistant.fr/page20_1\",\"title\":null,\"dataTransferedInBytes\":135500030,\"dataDecodedInBytes\":90500020,\"httpRequestsCount\":245,\"domElementsCount\":9110,\"analyseStatut\":\"TERMINEE\"}]}")));
    }

    @Test
    @TestSecurity(user = "admin@test.fr", augmentors=TestEmailRoleAugmentor.class)
    void getPages_shouldReturnNotFoundWhenNotExistingSuiviIdIsProvided() {
        Mockito.when(userInfo.getEmail()).thenReturn("admin@test.fr");
        Mockito.when(userInfo.getString("given_name")).thenReturn("given_name");
        Mockito.when(userInfo.getString("usual_name")).thenReturn("usual_name");

        given()
                .pathParams(buildSuiviAndGroupIdsPathParam(-1L, 1L))
                .get(getWebsitePagesDetails)
                .then()
                .statusCode(404)
                .body("status", is("NOT_FOUND"))
                .body("message", is("Entity with id -1 not found"));
    }

    @Test
    @TestSecurity(user = "admin@test.fr", augmentors=TestEmailRoleAugmentor.class)
    void getPages_shouldReturnEmptyResponseWhenNotExistingGroupIdIsProvided() {
        Mockito.when(userInfo.getEmail()).thenReturn("admin@test.fr");
        Mockito.when(userInfo.getString("given_name")).thenReturn("given_name");
        Mockito.when(userInfo.getString("usual_name")).thenReturn("usual_name");

        given()
                .pathParams(buildSuiviAndGroupIdsPathParam(1L, -1L))
                .get(getWebsitePagesDetails)
                .then()
                .statusCode(200)
                .body(is(Matcher.quoteReplacement(
                        "{\"websiteName\":\"Site d'exemple\",\"pageCount\":60,\"groups\":{\"1\":{\"name\":\"Groupe n°1\",\"availableRessourceTypes\":[\"IMAGE\",\"PDF\"],\"alert\":null},\"51\":{\"name\":\"Groupe n°2\",\"availableRessourceTypes\":[],\"alert\":null},\"101\":{\"name\":\"Groupe n°3\",\"availableRessourceTypes\":[],\"alert\":null}},\"pageDetailDtos\":[]}")));
    }

    @Test
    @TestSecurity(user = "admin@test.fr", augmentors=TestEmailRoleAugmentor.class)
    void getResources_shouldReturnWebsitePagesDetailsWhenSuiviIdAndGroupIdIsProvided() {
        Mockito.when(userInfo.getEmail()).thenReturn("admin@test.fr");
        Mockito.when(userInfo.getString("given_name")).thenReturn("given_name");
        Mockito.when(userInfo.getString("usual_name")).thenReturn("usual_name");

        given()
                .pathParams(buildSuiviAndGroupIdsPathParam(1L, 1L))
                .get(getWebsiteResourcesDetails)
                .then()
                .statusCode(200)
                .body(is(Matcher.quoteReplacement(
                        "{\"websiteName\":\"Site d'exemple\",\"pageCount\":60,\"groups\":{\"1\":{\"name\":\"Groupe n°1\",\"availableRessourceTypes\":[\"IMAGE\",\"PDF\"],\"alert\":null},\"51\":{\"name\":\"Groupe n°2\",\"availableRessourceTypes\":[],\"alert\":null},\"101\":{\"name\":\"Groupe n°3\",\"availableRessourceTypes\":[],\"alert\":null}},\"resourceDetailDtos\":[]}")));

    }

    @Test
    @TestSecurity(user = "admin@test.fr", augmentors=TestEmailRoleAugmentor.class)
    void getResources_shouldReturnNotFoundWhenNotExistingSuiviIdIsProvided() {
        Mockito.when(userInfo.getEmail()).thenReturn("admin@test.fr");
        Mockito.when(userInfo.getString("given_name")).thenReturn("given_name");
        Mockito.when(userInfo.getString("usual_name")).thenReturn("usual_name");

        given()
                .pathParams(buildSuiviAndGroupIdsPathParam(-1L, 1L))
                .get(getWebsiteResourcesDetails)
                .then()
                .statusCode(404)
                .body("status", is("NOT_FOUND"))
                .body("message", is("Entity with id -1 not found"));
    }

    @Test
    @TestSecurity(user = "admin@test.fr", augmentors=TestEmailRoleAugmentor.class)
    void getResources_shouldReturnEmptyResponseWhenNotExistingGroupIdIsProvided() {
        Mockito.when(userInfo.getEmail()).thenReturn("admin@test.fr");
        Mockito.when(userInfo.getString("given_name")).thenReturn("given_name");
        Mockito.when(userInfo.getString("usual_name")).thenReturn("usual_name");

        given()
                .pathParams(buildSuiviAndGroupIdsPathParam(1L, -1L))
                .get(getWebsiteResourcesDetails)
                .then()
                .statusCode(200)
                .body(is(Matcher.quoteReplacement(
                        "{\"websiteName\":\"Site d'exemple\",\"pageCount\":60,\"groups\":{\"1\":{\"name\":\"Groupe n°1\",\"availableRessourceTypes\":[\"IMAGE\",\"PDF\"],\"alert\":null},\"51\":{\"name\":\"Groupe n°2\",\"availableRessourceTypes\":[],\"alert\":null},\"101\":{\"name\":\"Groupe n°3\",\"availableRessourceTypes\":[],\"alert\":null}},\"resourceDetailDtos\":[]}")));
    }

    private Map<String, ?> buildSuiviAndGroupIdsPathParam(final Long suiviId, final Long groupId) {
        return Map.of(
                "suiviId", suiviId,
                "groupId", groupId);
    }

}
