package fr.numeco;

import io.quarkus.oidc.UserInfo;
import io.quarkus.test.Mock;
import jakarta.enterprise.context.ApplicationScoped;

@Mock
@ApplicationScoped
public class MockUserInfo extends UserInfo {

    @Override
    public String getEmail() {
        return "admin@test.fr";
    }
    
}
