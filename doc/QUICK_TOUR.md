# Présentation rapide
## Le projet
Ce projet est composé de deux grandes fonctionnalités :
* l'API RestFull pour le frontend
    * Les Controllers, Services, Repositories, Domaines et DTOs sont regroupé par thème dans le dossier [misis](src/main/java/fr/numeco/misis)
* l'Analyzer pour effectuer le scan de site web
    * Les Schedulers, Services, DTOs sont regroupé dans le dossier [analyser](src/main/java/fr/numeco/analyser)

## l'Analyzer
L'Analyzer est conçu avec le framework Selenium.
* Le déclenchement des scénarios est planifiées depuis [AnalysisScheduler.java](src/main/java/fr/numeco/analyser/service/AnalysisScheduler.java)
* Le(s) scénario(s) sont dans [MisisNetworkAnalyser.java](src/main/java/fr/numeco/analyser/service/impl/MisisNetworkAnalyser.java)
* Les interactions avec le client de Selenium (WebDriver) sont dans [MisisWebDriverService.java](src/main/java/fr/numeco/analyser/service/impl/MisisWebDriverService.java)

### Le WebDriver
Choisi est Chrome dans une version supérieur à 126

#### Pour l'environnement LOCAL
Nous utilisons directement le `ChromeDriver` pour garder une simplicité dans le lancement du projet.

#### Pour l'environnement DEV, TESTS, PROD
Nous utilisons le `RemoteWebDriver` qui permet de désolidariser le serveur Selenium du projet.
Le serveur Selenium quant à lui, il est présent dans l'image Docker `selenium/standalone-chrome` ou toutes autres pour une meilleure scalabilité

## Prochaine étape
[La configuration](CONFIGURATION.md)