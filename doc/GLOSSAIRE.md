# Glossaire des termes techniques
## API RestFull
Une API (Application Programming Interface) qui utilise les principes REST (Representational State Transfer) pour permettre la communication entre le frontend et le backend.
## Controllers
Composants qui gèrent les requêtes HTTP entrantes, les traitent et renvoient les réponses appropriées.
## Services
Composants qui contiennent la logique métier de l'application.
## Repositories
Composants qui gèrent l'accès aux données, généralement en interagissant avec une base de données.
## Domaines
Objets représentant les entités principales du modèle de données de l'application.
## DTOs (Data Transfer Objects)
Objets utilisés pour transférer des données entre différentes couches de l'application.
## Analyzer
Composant conçu pour effectuer le scan de sites web, utilisant le framework Selenium.
## Schedulers
Composants qui planifient et déclenchent des tâches à des moments spécifiques.
## Selenium
Framework pour automatiser les navigateurs web, utilisé pour tester des applications web.
## WebDriver
Interface de Selenium permettant de contrôler un navigateur web.
## ChromeDriver
Implémentation de WebDriver pour le navigateur Google Chrome.
## RemoteWebDriver
Implémentation de WebDriver permettant de contrôler un navigateur web à distance, souvent utilisé avec des serveurs Selenium.
## Docker
Plateforme permettant de créer, déployer et exécuter des applications dans des conteneurs.
## selenium/standalone-chrome
Image Docker contenant un serveur Selenium avec un navigateur Chrome intégré, utilisée pour exécuter des tests de manière scalable.
