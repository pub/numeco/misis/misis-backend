# La configuration
## L'Analyzer

| Clé                                                 | Description                                                                       | Exemple         |
|-----------------------------------------------------|-----------------------------------------------------------------------------------|-----------------|
| analyzer.pagination-size                            | Nombre de pages qui sera prit lors de la prochaine analyse                        | 70              |
| analyzer.scrolling.height-in-pixel                  | Nombre de pixel lors de la simulation du défilement de page                       | 100             |
| analyzer.scrolling.pause-in-milliseconds            | Temps d'attente entre deux defilement                                             | 400             |
| analyzer.documents.css-selector-attribute.PDF       | Le nom de l'attribut contenant l'url pour la recherche via le sélecteur CSS       | href            |
| analyzer.documents.css-selector-attribute.OTHERS    | * (idem)                                                                          | href            |
| analyzer.documents.css-selector-attribute.STREAMING | * (idem)                                                                          | src             |
| analyzer.documents.extensions.PDF ¹                 | La liste des extensions prise en compte dans la catégorie 'PDF'                   | .pdf            |
| analyzer.documents.extensions.OTHERS ¹              | * (idem pour la catégorie OTHERS)                                                 | .xls,.xlsx,.ods |
| analyzer.documents.extensions.STREAMING ¹           | * (idem pour la catégorie STREAMING)                                              | .mp4,.avi,.mkv  |
| analyser.refresh.schedule.cron.expression           | Expression CRON de l'intervalle de déclenchement de l'analyse                     | 0 0/30 0 * * ?  |
| stats.refresh.schedule.cron.expression              | Expression CRON de l'intervalle de déclenchement de la création des statistiques  | 0 0/35 * * * ?  |


¹ : le séparateur de la liste est une virgule