# L'Analyse et les Statistiques
Voici comment fonctionne le projet afin d'avoir des statistiques pour un suivi de site.
## 1. La création du suivi de site
À la création du suivi, plusieurs champs sont à renseigner :

| Nom du champ                  | Description                                                                                                                      |
|-------------------------------|----------------------------------------------------------------------------------------------------------------------------------|
| Nom du suivi                  | Le Nom du suivi est pour vous repérer plus facilement votre suivi                                                                |
| Nom de domaine                | C'est le nom renseigner dans au niveau du DNS de votre site                                                                      |
| Nom du groupe                 | Ici, nous commençons à structuré les statistiques.<br/>Toutes les pages renseigné dans ce groupe permet de les comparé entre eux |
| Périodicité du suivi          | Défini l'intervalle de création des statistiques                                                                                 |
| Méthode de création de groupe | Choix sur la manières de renseigner les pages (décrit plus bas)                                                                  |

À noter les les URLs sont découpé de la sorte, je vais prendre pour exemple : 
- `https://misis.developpement-durable.gouv.fr/suivi-de-site`

**Protocole** = `https://`<br/>**Nom de domaine** = `misis.developpement-durable.gouv.fr`<br/>**Chemin de la page** = `/suivi-de-site` ou `/` pour l'accueil

### Méthode de création de groupe
#### Manuel
Dans ce mode de saisi, tous les chemins de page devront être saisi dans le tableau qui apparaitra à la suite du formulaire de création de suivi de site.
#### Lien à partir d'une page parente (WIP)
**/!\\** *Travail en cours* **/!\\**
#### Analyse du sitemap (WIP)
**/!\\** *Travail en cours* **/!\\**
#### Par API (WIP)
**/!\\** *Travail en cours* **/!\\**

## 2. Après la création
Votre suivi de site est enregistré mais, en attente du prochain déclenchement de l'analyse.
Le temps d'attente est d'environ 30 minutes.

> TODO : 
> Des améliorations ou modification comme un déclenchement manuel ou à la création impacte cette documentation !

## 3. Après la première analyse
Votre suivi de site possède de nouvelles informations mais, aucune statistiques !
- Sur la *liste des pages*
  - Données transférées
  - Données décodées
  - Le nombre de Requêtes
  - Nombre d'éléments du DOM
- Sur les *images*, *PDF* et *Autres documents*
  - Poids

## 4. Les statistiques
Le temps d'attente maximum est de 35 minutes pour un suivi de site nouvellement créé.
Les graphiques, ils apparaitront avec un seul point sans courbe. Le premier trait sera visible après en fonction de votre `Périodicité du suivi`
