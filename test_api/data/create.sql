
    create sequence groupe_de_pages_seq start with 1 increment by 1;

    create sequence misis_user_seq start with 1 increment by 1;

    create sequence page_seq start with 1 increment by 1;

    create sequence ressource_seq start with 1 increment by 1;

    create sequence suivi_de_site_seq start with 1 increment by 1;

    create table groupe_de_pages (
        id bigint not null,
        suivi_de_site_id bigint,
        methode_de_creation_de_groupe varchar(255) not null check (methode_de_creation_de_groupe in ('MANUELLE')),
        name varchar(255) not null,
        periodicite_du_suivi varchar(255) not null check (periodicite_du_suivi in ('QUOTIDIEN','HEBDOMADAIRE','MENSUEL')),
        primary key (id)
    );

    create table misis_user (
        created_date date not null,
        id bigint not null,
        email varchar(255) not null unique,
        username varchar(255) not null unique,
        primary key (id)
    );

    create table misis_user_suivi_de_site (
        suivis_de_site_id bigint not null,
        users_id bigint not null
    );

    create table page (
        dom_elements_count integer,
        http_requests_count integer,
        data_decoded_in_bytes bigint,
        data_transfered_in_bytes bigint,
        groupe_de_pages_id bigint,
        id bigint not null,
        url varchar(255) not null,
        primary key (id),
        constraint unique_url_groupe_de_pages_id unique (groupe_de_pages_id, url)
    );

    create table ressource (
        id bigint not null,
        page_id bigint,
        size_in_bytes bigint,
        filename varchar(255),
        type varchar(255) check (type in ('IMAGE','PDF')),
        primary key (id)
    );

    create table suivi_de_site (
        created_date date not null,
        date_de_derniere_analyse date,
        modified_date date not null,
        created_by bigint not null,
        id bigint not null,
        modified_by bigint not null,
        name varchar(255) not null,
        url varchar(255) not null,
        primary key (id)
    );

    create index type_index 
       on ressource (type);

    alter table if exists groupe_de_pages 
       add constraint FKrsc6whcqvnc6sfotm37f81lto 
       foreign key (suivi_de_site_id) 
       references suivi_de_site;

    alter table if exists misis_user_suivi_de_site 
       add constraint FK4texldr7gp2yl50p0s29h4pff 
       foreign key (suivis_de_site_id) 
       references suivi_de_site;

    alter table if exists misis_user_suivi_de_site 
       add constraint FKjo874h6dgk5d5bwvm3klqm4f1 
       foreign key (users_id) 
       references misis_user;

    alter table if exists page 
       add constraint FKjxwpysanb4xqycbkrg4j6hw8v 
       foreign key (groupe_de_pages_id) 
       references groupe_de_pages;

    alter table if exists ressource 
       add constraint FKmsfbbkm717tlnghv53u7ub1e3 
       foreign key (page_id) 
       references page;

    alter table if exists suivi_de_site 
       add constraint FKamdsjd16bvx9666vxq6q86cx9 
       foreign key (created_by) 
       references misis_user;

    alter table if exists suivi_de_site 
       add constraint FKaehsc88r8pwk4re9ba401dyus 
       foreign key (modified_by) 
       references misis_user;
INSERT INTO misis_user(id,username,email,created_date) VALUES (nextval('misis_user_SEQ'), 'admin', 'admin@test.fr', now());
INSERT INTO misis_user(id,username,email,created_date) VALUES (nextval('misis_user_SEQ'), 'John Doe', 'one@test.fr', now());
INSERT INTO misis_user(id,username,email,created_date) VALUES (nextval('misis_user_SEQ'), 'Jane Doe', 'two@test.fr', now());
INSERT INTO misis_user(id,username,email,created_date) VALUES (nextval('misis_user_SEQ'), 'Third User', 'three@test.fr', now());
INSERT INTO suivi_de_site (id, name, url, created_date, modified_date, date_de_derniere_analyse, created_by, modified_by) VALUES     (nextval('suivi_de_site_seq'), 'Sample Site', 'www.sample.com', now(), now(), now(), 1, 1),     (nextval('suivi_de_site_seq'), 'Another Site', 'www.another.com', now(), now(), NULL, 1, 1);
INSERT INTO misis_user_suivi_de_site (users_id, suivis_de_site_id) VALUES     (1, 1),     (1, 2);
INSERT INTO groupe_de_pages(id, name, periodicite_du_suivi, methode_de_creation_de_groupe, suivi_de_site_id) VALUES     (nextval('groupe_de_pages_seq'), 'Sample Group 1', 'QUOTIDIEN', 'MANUELLE', 1),     (nextval('groupe_de_pages_seq'), 'Sample Group 2', 'QUOTIDIEN', 'MANUELLE', 1),     (nextval('groupe_de_pages_seq'), 'Sample Group 3', 'QUOTIDIEN', 'MANUELLE', 1),     (nextval('groupe_de_pages_seq'), 'Another Group 1', 'HEBDOMADAIRE', 'MANUELLE', 2),     (nextval('groupe_de_pages_seq'), 'Another Group 2', 'HEBDOMADAIRE', 'MANUELLE', 2),     (nextval('groupe_de_pages_seq'), 'Another Group 3', 'HEBDOMADAIRE', 'MANUELLE', 2),     (nextval('groupe_de_pages_seq'), 'Another Group 4', 'HEBDOMADAIRE', 'MANUELLE', 2);
