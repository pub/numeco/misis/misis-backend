*** Settings ***
Library  RequestsLibrary
Library  Browser

*** Variables ***
${API_BASE_URL}             http://%{APP_HOST}:8080
${KEYCLOAK_TOKEN_URL}       http://%{KEYCLOAK_HOST}:8088/realms/quarkus/protocol/openid-connect/token
${USERNAME}                 admin
${PASSWORD}                 %{KEYCLOAK_ADMIN_PASSWORD}
${CLIENT_ID}                backend-service
${CLIENT_SECRET}            secret

*** Test Cases ***
Requête GET sans Authentification
    ${response}=  GET  ${API_BASE_URL}/suivis-de-site  expected_status=200  
    ${content}=  Convert To String    ${response.content}
    Should Start With  ${content}  <!DOCTYPE html>  strip_spaces=True
    Should Start With    ${response.url}    http://%{KEYCLOAK_HOST}:8088/realms/quarkus/protocol/openid-connect/auth

*** Test Cases ***
Requête GET avec Authentification
    Authenticate as Admin
    ${headers}=  Create Dictionary  accept=application/json  Cookie=q_session\=${access_token}
    ${response}=  GET  ${API_BASE_URL}/suivis-de-site  headers=${headers}  expected_status=200
    Log  ${response}

*** Keywords ***
Authenticate as Admin
    New Page  ${API_BASE_URL}/suivis-de-site
    Type Text  input#username  ${USERNAME}
    Type Text  input#password  ${PASSWORD}
    Click  button#kc-login
    ${cookie}=  Get Cookie  q_session
    ${access_token}    Set Variable    ${cookie.value}
    Set Suite Variable    ${access_token}