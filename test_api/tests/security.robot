*** Settings ***
Library  RequestsLibrary
Library  Browser

*** Variables ***
${API_BASE_URL}             http://%{APP_HOST}:8080
${KEYCLOAK_TOKEN_URL}       http://%{KEYCLOAK_HOST}:8088/realms/quarkus/protocol/openid-connect/token
${USERNAME}                 user1
${PASSWORD}                 %{USER_PASSWORD}
${CLIENT_ID}                backend-service
${CLIENT_SECRET}            secret

#[GET] /suivis-de-site/{suiviId}
*** Test Cases ***
En tant que 'user1', je ne dois pas avoir voir le suivi de site id '1'
    [Documentation]  [GET] /suivis-de-site/{suiviId}
    [Tags]  Suivi du site
    Set Suite Variable  ${suiviId}  1
    Authenticate as User
    ${headers}=  Create Dictionary  Content-Type=application/json  Cookie=q_session\=${access_token}
    ${response}=  GET  ${API_BASE_URL}/suivis-de-site/{suiviId}  headers=${headers}  expected_status=404

#[GET] /suivis-de-site/settings/{suiviId}
*** Test Cases ***
En tant que 'user1', je ne dois pas avoir accès aux paramètres du suivi de site id '1'
    [Documentation]  [GET] /suivis-de-site/settings/{suiviId}
    [Tags]  Paramètres du suivi de site
    Set Suite Variable  ${suiviId}  1
    Authenticate as User
    ${headers}=  Create Dictionary  Content-Type=application/json  Cookie=q_session\=${access_token}
    ${response}=  GET  ${API_BASE_URL}/suivis-de-site/settings/${suiviId}  headers=${headers}  expected_status=404

#[POST] /suivis-de-site/settings/{suiviId}
*** Test Cases ***
En tant que 'user1', je ne dois pas pouvoir modifier les paramètres du suivi de site id '1'
    [Documentation]  [POST] /suivis-de-site/settings/{suiviId}
    [Tags]  Paramètres du suivi de site
    Set Suite Variable  ${suiviId}  1
    Set Suite Variable  ${body}  [{"id": 1,"name": "Nouveau nom du Suivi","periodiciteDuSuivi": "QUOTIDIEN","methodeDeCreationDeGroupe": "MANUELLE","pages": [{"id": null,"url": "https://misis.developpement-durable.gouv.fr/"}]}]
    Authenticate as User
    ${headers}=  Create Dictionary  Content-Type=application/json  Cookie=q_session\=${access_token}
    ${response}=  POST  ${API_BASE_URL}/suivis-de-site/settings/${suiviId}  data=${body}  headers=${headers}  expected_status=404

#[DELETE] /suivis-de-site/settings/{suiviId}
*** Test Cases ***
En tant que 'user1', je ne dois pas pouvoir supprimer le suivi de site id '1'
    [Documentation]  [DELETE] /suivis-de-site/settings/{suiviId}
    [Tags]  Paramètres du suivi de site
    Set Suite Variable  ${suiviId}  1
    Authenticate as User
    ${headers}=  Create Dictionary  Content-Type=application/json  Cookie=q_session\=${access_token}
    ${response}=  DELETE  ${API_BASE_URL}/suivis-de-site/settings/${suiviId}  headers=${headers}  expected_status=404

#[GET] /suivis-de-site/{suiviId}/groupe/{groupId}/liste-des-pages
*** Test Cases ***
En tant que 'user1', je ne dois pas voir la liste de page du suivi de site id '1'
    [Documentation]  [GET] /suivis-de-site/{suiviId}/groupe/{groupId}/liste-des-pages
    [Tags]  Ressources du suivi de site
    Set Suite Variable  ${suiviId}  1
    Set Suite Variable  ${groupId}  1
    Authenticate as User
    ${headers}=  Create Dictionary  Content-Type=application/json  Cookie=q_session\=${access_token}
    ${response}=  GET  ${API_BASE_URL}/suivis-de-site/${suiviId}/groupe/${groupId}/liste-des-pages  headers=${headers}  expected_status=404

#[GET] /suivis-de-site/{suiviId}/groupe/{groupId}/liste-des-ressources
*** Test Cases ***
En tant que 'user1', je ne dois pas voir les ressources du suivi de site id '1'
    [Documentation]  [GET] /suivis-de-site/{suiviId}/groupe/{groupId}/liste-des-ressources
    [Tags]  Ressources du suivi de site
    Set Suite Variable  ${suiviId}  1
    Set Suite Variable  ${groupId}  1
    Authenticate as User
    ${headers}=  Create Dictionary  Content-Type=application/json  Cookie=q_session\=${access_token}
    ${response}=  GET  ${API_BASE_URL}/suivis-de-site/${suiviId}/groupe/${groupId}/liste-des-ressources  headers=${headers}  expected_status=404

#[GET] /suivis-de-site/{suiviId}/groupe/{groupId}/statistiques/{statType}
*** Test Cases ***
En tant que 'user1', je ne dois pas voir les statistiques du suivi de site id '1'
    [Documentation]  [GET] /suivis-de-site/{suiviId}/groupe/{groupId}/statistiques/{statType}
    [Tags]  Ressources du suivi de site
    Set Suite Variable  ${suiviId}  1
    Set Suite Variable  ${groupId}  1
    Set Suite Variable  ${statType}  'IMAGES'
    Authenticate as User
    ${headers}=  Create Dictionary  Content-Type=application/json  Cookie=q_session\=${access_token}
    ${response}=  GET  ${API_BASE_URL}/suivis-de-site/${suiviId}/groupe/${groupId}/statistiques/${statType}  headers=${headers}  expected_status=404

*** Keywords ***
Authenticate as User
    New Page  ${API_BASE_URL}/suivis-de-site
    Type Text  input#username  ${USERNAME}
    Type Text  input#password  ${PASSWORD}
    Click  button#kc-login
    ${cookie}=  Get Cookie  q_session
    ${access_token}    Set Variable    ${cookie.value}
    Set Suite Variable    ${access_token}